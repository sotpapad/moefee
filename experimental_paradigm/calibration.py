﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2022.2.5),
    on Δευ 22 Ιουλ 2024 02:32:05 μμ 
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

# --- Import packages ---
from psychopy import locale_setup
from psychopy import prefs
from psychopy import sound, gui, visual, core, data, event, logging, clock, colors, layout, parallel
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle, choice as randchoice
import os  # handy system and path functions
import sys  # to get file system encoding

import psychopy.iohub as io
from psychopy.hardware import keyboard

# Run 'Before Experiment' code from load_data
import pickle
import time
from psychopy import prefs
from os.path import join

#-----#
debug_mode = True

#-----#
# Parallel port adress.
ppadr = "0x3FE8" # 0x3FF8

#-----#
# Does the target appear at the same time as the cursor?
cursor_target_sync = True  # True, False

#-----#
# During ADJUST trials, should the cursors or
# the actual hand be brought back to the
# neutral position?
bring_to_neutral_pos = "hand"  # "cursor", "hand"

#-----#
# Is this session gamified, so as to keep the
# subjects motivated?
gamification = False    # True, False

#-----#
# Loading of all data for randomization.
system = "linux"    # "linux", "windows"

if system == "linux":
    datapath = "/home/sotpapad/Codes/experiment/"
    #screen_num = int(float("1.0"))
elif system == "windows":
    datapath = "C:\Python_users\Hifi\\"
    #screen_num = int(float("1.0"))

# Is the cursor being reset to the neutral
# position between trials?
mode = "zero"   # "zero", "continue"

# What type of session is this?
session_type_str =  "_calibration"
if session_type_str == "_training":
    session_type_msg = "Training"
elif session_type_str == "_calibration":
    session_type_msg = "Calibration"
if session_type_str == "":
    session_type_msg = "Experiment"
 
datafile = "{}exp_design_{}{}.pickle".format(datapath, mode, session_type_str)

with open(datafile, "rb") as file:
    subjects_data = pickle.load(file)

#-----#
# Which units are being used?
units = prefs.general["units"]
if units != "height":
    raise ValueError("This epxeriment has been designed only to run using 'height' as units.")

#-----#
# Print all exoerimental design choices.
print("Working on {} system.".format(system))
print("Parallel port address used: {}.".format(ppadr))
print("Are debugging messages enabled? {}".format(debug_mode))
print("Session type: {}.".format(session_type_msg))
print("Trial mode (positioning of cursor at trial end): {}".format(mode))
print("Do ACTION cursor and target appear synchronously? {}".format(cursor_target_sync))
print("Do ADJUST trials ask for 'cursor' or 'hand' re-positioning? {}".format(bring_to_neutral_pos))
print("Is a score metric presented to the participants at the end of each block? {}".format(gamification))
print("\n")

#-----#
# Handy function definition for tranforming
# the mouse coordinates system.
def transform_mouse(
    xlist,
    ylist,
    unit_coords=[-1.0, 1.0],
    screen_coords=[-0.89, 0.89],
    rad=0.375,
    norm_units=False,
):
    
    # Tranform to array.
    if isinstance(xlist, float):
        xlist = [xlist]
    if isinstance(ylist, float):
        ylist = [ylist]
    
    # Define screen-to-arm scaling.
    #x_adj = np.linspace(2.0, 2.5, 101)
    x_adj = np.linspace(2.0, 2.61, 101)
    y_adj = np.linspace(1.0, 1.2, 101)

    x_poss = np.arange(0.0, 1.01, 0.01)
    y_poss = np.arange(0.0, 1.01, 0.01)

    # Use scaling.
    xlist = np.array(xlist) / x_adj[np.abs(xlist[-1]) <= x_poss][0]
    ylist = np.array(ylist) / y_adj[np.abs(ylist[-1]) <= y_poss][0]

    if norm_units == True:
        # Scale values from screen [min, max] to [-1, 1]
        # to have coordinates corresponding to the unit circle.
        xlist_scaled = unit_coords[0] + (xlist - screen_coords[0]) *\
            (unit_coords[1] - unit_coords[0]) / (screen_coords[1] - screen_coords[0])
        
        # Scale unit circle to custom radius.
        xlist_transformed = xlist_scaled * rad

        # Compute angle.
        xlist_angle = 90 - np.arccos(xlist_scaled) * 180 / np.pi

        # y is defined by x on the unit circle, then scaled
        # to custom radius.
        ylist_transformed = np.sin(np.arccos(xlist_scaled)) * rad

    elif norm_units == False:
        # Restrict values to [-rad, rad].
        if xlist.shape[0] == 1:
            if xlist > rad:
                xlist = rad
            elif xlist < -rad:
                xlist = -rad
        elif xlist.shape[0] > 1:
            over = xlist > rad
            under = xlist < -rad
            xlist[over] = rad
            xlist[under] = -rad
        
        # Restrict values to [0, rad]
        if ylist.shape[0] == 1:
            if ylist > rad:
                ylist = rad
            elif ylist < 0.0:
                ylist = 0.0
        elif ylist.shape[0] > 1:
            over = ylist > rad
            under = ylist < 0.0
            ylist[over] = rad
            ylist[under] = 0.0
        
        # Rename for consistency.
        xlist_transformed = xlist
        
        # Compute angle.
        xlist_true_angle = np.arccos(xlist / rad)
        xlist_angle = 90 - np.arccos(xlist / rad) * 180 / np.pi

        # y is defined on circle with custom radius.
        ylist_scaled = np.sin(xlist_true_angle) * rad 
        ylist_transformed = ylist_scaled - (rad - ylist)

        # Restrict to [ylist_scaled, rad].
        if isinstance(ylist_transformed, float):
            ylist_transformed = [ylist_transformed]
        
        ylist_transformed = np.array(ylist_transformed)

        if ylist_transformed.shape[0] == 1:
            if ylist_transformed > rad:
                ylist_transformed = rad
            elif ylist_transformed < ylist_scaled:
                ylist_transformed = ylist_scaled
        elif ylist_transformed.shape[0] > 1:
            over = ylist_transformed > rad
            under = ylist_transformed < ylist_scaled
            ylist_transformed[over] = rad
            ylist_transformed[under] = ylist_scaled[under]
        
    return xlist_transformed, ylist_transformed, xlist_angle



# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)
# Store info about the experiment session
psychopyVersion = '2022.2.5'
expName = 'calibration_session'  # from the Builder filename that created this script
expInfo = {
    'initials': 'XX',
}
# --- Show participant info dialog --
dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'calibration/Sub_%s_%s_%s' %(expInfo['initials'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='/home/sotpapad/git_stuff/moefee/experimental_paradigm/calibration.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp
frameTolerance = 0.001  # how close to onset before 'same' frame

# Start Code - component code to be run after the window creation

# --- Setup the Window ---
win = visual.Window(
    size=[1920, 1080], fullscr=True, screen=1, 
    winType='pyglet', allowStencil=False,
    monitor='Laptop_monitor', color=[0,0,0], colorSpace='rgb',
    blendMode='avg', useFBO=True)
win.mouseVisible = False
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess
# --- Setup input devices ---
ioConfig = {}

# Setup iohub keyboard
ioConfig['Keyboard'] = dict(use_keymap='psychopy')

ioSession = '1'
if 'session' in expInfo:
    ioSession = str(expInfo['session'])
ioServer = io.launchHubServer(window=win, **ioConfig)
eyetracker = None

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard(backend='iohub')

# --- Initialize components for Routine "LoadData" ---
# Run 'Begin Experiment' code from load_data
#-----#
# Relative coordinates of targets positions on screen.
# [-60, -30, 0, 30, 60]
if units == "norm":
    targets_coords = {
            "-60": (-0.63, 0.4),
            "-30": (-0.35, 0.75),
            "0": (0.001, 0.87),
            "30": (0.35, 0.75),
            "60": (0.63, 0.4)
            }
elif units == "height":
    targets_coords = {
            "-60": (-0.3247, 0.1875),
            "-45": (-0.2652, 0.2652),
            "-30": (-0.1875, 0.3247),
            "-20": (-0.1283, 0.3524),
            "0": (0.0, 0.375),
            "20": (0.1283, 0.3524),
            "30": (0.1875, 0.3247),
            "45": (0.2652, 0.2652),
            "60": (0.3247, 0.1875)
            }

# Confidence area for cursor and target overlap check.
mouse_conf = 0.001
mouse_adj_conf = 0.001

#-----#
# Initial pseudo-position for target and intitial cursor position (0).
target_coords = targets_coords["0"]
cursor_coords = targets_coords["0"]

# Initial pseudo-state for online cursor adjustment.
mouse_is_moving = False
mouse_x = 0
mouse_y = 0

# Variables that saves the state of trial with
# unacceptable reaction time or target reach.
too_slow = False
too_fast = False
is_valid = True
 
#-----#
# Targets, cursor, arc and fixation cross properties.
target_size = (0.10)
target_fill_col = (-1.0000, -1.0000, 1.0000)
target_border_col = (-1.0000, -1.0000, 1.0000)
target_fill_col_go = (1.0000, -1.0000, 1.0000)
target_border_col_go = (1.0000, -1.0000, 1.0000)

cursor_size = (0.06)
cursor_fill_col = (1.0000, -1.0000, -1.0000)
cursor_border_col = (1.0000, -1.0000, -1.0000)
cursor_adj_fill_col = (1.0000, 0.2941, -1.0000)
cursor_adj_border_col = (1.0000, 0.2941, -1.0000)

arc_size = (0.75, 0.4)
arc_position = (0.0, 0.18)
arc_image = "images/arc_new.png"
arc_catch_image = "images/arc_catch_new.png"
arc_fg_col = [1,1,1]

fix_size = (0.04)
fix_position = (0.0, 0.0)
fix_col = "white"

screen_lims = (0.888, 0.5)

#-----#
# Text properties.
text_size = 0.03
text_pos = (0.0, 0.25)
text_pos_upper = (0.0, 0.45)
text_pos_slow_fast = (0.0, 0.0)
text_wrap = 1.5

#-----#
# Random number generator initialization.
rng = np.random.default_rng()

# Uniform distribution of ITPs from 2 to 4 seconds.
list_of_durs = np.arange(2.0, 4.05, 0.1)

# Time offset in order to avoid flickering.
avoid_flicker = 0.05

#-----#
# Trigger values.
tr_begin_block = 11.0
tr_end_block = 12.0

tr_begin_trial = 1.0
tr_end_trial = 2.0

tr_adj_trial = 6.0
tr_catch_trial = 5.0

#-----#
# Duration (s) for pausing a loop when the reaction
# time is too slow or the cursor needs to be
# adjusted to the target.
inform_too_slow_dur = 1.0
pause_dur = 0.2

# Duration of mouse not moving to prematurelly end
# an action trial.
not_moving_dur = 5.0
screen_refresh_period = win.monitorFramePeriod
not_moved_id = int(not_moving_dur / screen_refresh_period)

# Duration of inter-block period.
ib_period = 1.0

# --- Initialize components for Routine "BeginScreen" ---
begin_session = visual.TextStim(win=win, name='begin_session',
    text='The calibration session begins.\n\n(Press SPACE to continue)',
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
# Run 'Begin Experiment' code from hide_mouse_begin_screen
#-----#
# Hide mouse.
win.mouseVisible = False
key_resp = keyboard.Keyboard()

# --- Initialize components for Routine "CheckMonitor" ---
mouse = event.Mouse(win=win)
x, y = [None, None]
mouse.mouseClock = core.Clock()
text = visual.TextStim(win=win, name='text',
    text='Test screen coordinates.',
    font='Open Sans',
    pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=-2.0);

# --- Initialize components for Routine "InitBlock" ---
block_info = visual.TextStim(win=win, name='block_info',
    text=None,
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);

# --- Initialize components for Routine "BeginTrial" ---
fixation_cross_itp = visual.ShapeStim(
    win=win, name='fixation_cross_itp', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=-1.0, interpolate=True)
arc_itp = visual.ImageStim(
    win=win,
    name='arc_itp', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-2.0)

# --- Initialize components for Routine "ActionTrial" ---
fixation_cross_action_trial = visual.ShapeStim(
    win=win, name='fixation_cross_action_trial', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=0.0, interpolate=True)
arc_action_trial = visual.ImageStim(
    win=win,
    name='arc_action_trial', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-1.0)
mouse_action_trial = event.Mouse(win=win)
x, y = [None, None]
mouse_action_trial.mouseClock = core.Clock()
cursor_action_trial = visual.ShapeStim(
    win=win, name='cursor_action_trial',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_border_col, fillColor=cursor_fill_col,
    opacity=None, depth=-3.0, interpolate=True)
target_action_trial = visual.ShapeStim(
    win=win, name='target_action_trial',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-4.0, interpolate=True)
target_action_trial_sync = visual.ShapeStim(
    win=win, name='target_action_trial_sync',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-5.0, interpolate=True)
target_action_trial_go = visual.ShapeStim(
    win=win, name='target_action_trial_go',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col_go, fillColor=target_fill_col_go,
    opacity=None, depth=-6.0, interpolate=True)
cursor_action_trial_online = visual.ShapeStim(
    win=win, name='cursor_action_trial_online',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_border_col, fillColor=cursor_fill_col,
    opacity=None, depth=-7.0, interpolate=True)
key_resp_2 = keyboard.Keyboard()
degrees_info = visual.TextStim(win=win, name='degrees_info',
    text=None,
    font='Open Sans',
    pos=text_pos_upper, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=-10.0);

# --- Initialize components for Routine "ActionTrialAdj" ---
slow_trial_info = visual.TextStim(win=win, name='slow_trial_info',
    text='TOO SLOW',
    font='Open Sans',
    pos=text_pos_slow_fast, height=text_size, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
fast_trial_info = visual.TextStim(win=win, name='fast_trial_info',
    text='TOO FAST',
    font='Open Sans',
    pos=text_pos_slow_fast, height=text_size, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=-1.0);
text_adjust = visual.TextStim(win=win, name='text_adjust',
    text='Bring your hand to neutral position and press any button to continue.',
    font='Open Sans',
    pos=text_pos_upper, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=-2.0);
fixation_cross_adj_trial = visual.ShapeStim(
    win=win, name='fixation_cross_adj_trial', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=-3.0, interpolate=True)
arc_adj_trial = visual.ImageStim(
    win=win,
    name='arc_adj_trial', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-4.0)
mouse_adj_trial = event.Mouse(win=win)
x, y = [None, None]
mouse_adj_trial.mouseClock = core.Clock()
target_adj_trial = visual.ShapeStim(
    win=win, name='target_adj_trial',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-6.0, interpolate=True)
cursor_adj_trial = visual.ShapeStim(
    win=win, name='cursor_adj_trial',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=None, depth=-7.0, interpolate=True)
key_resp_3 = keyboard.Keyboard()

# --- Initialize components for Routine "EndTrial" ---
fixation_cross_end = visual.ShapeStim(
    win=win, name='fixation_cross_end', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=0.0, interpolate=True)
arc_itp_end = visual.ImageStim(
    win=win,
    name='arc_itp_end', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-1.0)

# --- Initialize components for Routine "CalibrationDataSave" ---

# --- Initialize components for Routine "ByeByeScreen" ---
bye_bye = visual.TextStim(win=win, name='bye_bye',
    text='End of calibration session.',
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.Clock()  # to track time remaining of each (possibly non-slip) routine 

# --- Prepare to start Routine "LoadData" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
# keep track of which components have finished
LoadDataComponents = []
for thisComponent in LoadDataComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "LoadData" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in LoadDataComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "LoadData" ---
for thisComponent in LoadDataComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# Run 'End Routine' code from load_data
#-----#
# Specific subject data.
subject_id = "subject_{}".format(1)
this_sub_data_rec_order = subjects_data[subject_id]["rec_order"]
this_sub_data_hand_order = subjects_data[subject_id]["hand_order"]
this_sub_data_cond_order = subjects_data[subject_id]["condition_order"]

# Remove keys not encoding block structure.
this_sub_data_block_order = subjects_data[subject_id].keys()
this_sub_data_block_order = list(this_sub_data_block_order)
this_sub_data_block_order.remove("rec_order")
this_sub_data_block_order.remove("hand_order")
this_sub_data_block_order.remove("condition_order")

#-----#
# Print out information.
rec_num = 1
ses_num = 1
print("Recording {}".format(rec_num))
print("Session {}".format(ses_num))

print("Subject's 1st recording modality: {}.".format(this_sub_data_rec_order[0]))
print("This session starts with: {}.".format(this_sub_data_hand_order[rec_num-1][ses_num-1]))
print("This session starts with: {}.".format(this_sub_data_cond_order[rec_num-1][ses_num-1]))
 
#-----#
# Only use variables of current recording.
sub_block_strct = this_sub_data_block_order[:2]
 
#-----#
# Variables that keep track of the loop ids.
n_blocks = 1
block_id = 0
session_id = 0
# the Routine "LoadData" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "BeginScreen" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp.keys = []
key_resp.rt = []
_key_resp_allKeys = []
# keep track of which components have finished
BeginScreenComponents = [begin_session, key_resp]
for thisComponent in BeginScreenComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "BeginScreen" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *begin_session* updates
    if begin_session.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        begin_session.frameNStart = frameN  # exact frame index
        begin_session.tStart = t  # local t and not account for scr refresh
        begin_session.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(begin_session, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'begin_session.started')
        begin_session.setAutoDraw(True)
    
    # *key_resp* updates
    waitOnFlip = False
    if key_resp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp.frameNStart = frameN  # exact frame index
        key_resp.tStart = t  # local t and not account for scr refresh
        key_resp.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp.started')
        key_resp.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp.status == STARTED and not waitOnFlip:
        theseKeys = key_resp.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
        _key_resp_allKeys.extend(theseKeys)
        if len(_key_resp_allKeys):
            key_resp.keys = _key_resp_allKeys[-1].name  # just the last key pressed
            key_resp.rt = _key_resp_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in BeginScreenComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "BeginScreen" ---
for thisComponent in BeginScreenComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp.keys in ['', [], None]:  # No response was made
    key_resp.keys = None
thisExp.addData('key_resp.keys',key_resp.keys)
if key_resp.keys != None:  # we had a response
    thisExp.addData('key_resp.rt', key_resp.rt)
thisExp.nextEntry()
# the Routine "BeginScreen" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
monitor_test = data.TrialHandler(nReps=0.0, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=[None],
    seed=None, name='monitor_test')
thisExp.addLoop(monitor_test)  # add the loop to the experiment
thisMonitor_test = monitor_test.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisMonitor_test.rgb)
if thisMonitor_test != None:
    for paramName in thisMonitor_test:
        exec('{} = thisMonitor_test[paramName]'.format(paramName))

for thisMonitor_test in monitor_test:
    currentLoop = monitor_test
    # abbreviate parameter names if possible (e.g. rgb = thisMonitor_test.rgb)
    if thisMonitor_test != None:
        for paramName in thisMonitor_test:
            exec('{} = thisMonitor_test[paramName]'.format(paramName))
    
    # --- Prepare to start Routine "CheckMonitor" ---
    continueRoutine = True
    routineForceEnded = False
    # update component parameters for each repeat
    # setup some python lists for storing info about the mouse
    mouse.x = []
    mouse.y = []
    mouse.leftButton = []
    mouse.midButton = []
    mouse.rightButton = []
    mouse.time = []
    gotValidClick = False  # until a click is received
    # Run 'Begin Routine' code from code_2
    win.mouseVisible = True
    # keep track of which components have finished
    CheckMonitorComponents = [mouse, text]
    for thisComponent in CheckMonitorComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "CheckMonitor" ---
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        # *mouse* updates
        if mouse.status == NOT_STARTED and t >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            mouse.frameNStart = frameN  # exact frame index
            mouse.tStart = t  # local t and not account for scr refresh
            mouse.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(mouse, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.addData('mouse.started', t)
            mouse.status = STARTED
            mouse.mouseClock.reset()
            prevButtonState = [0, 0, 0]  # if now button is down we will treat as 'new' click
        if mouse.status == STARTED:  # only update if started and not finished!
            buttons = mouse.getPressed()
            if buttons != prevButtonState:  # button state changed?
                prevButtonState = buttons
                if sum(buttons) > 0:  # state changed to a new click
                    x, y = mouse.getPos()
                    mouse.x.append(x)
                    mouse.y.append(y)
                    buttons = mouse.getPressed()
                    mouse.leftButton.append(buttons[0])
                    mouse.midButton.append(buttons[1])
                    mouse.rightButton.append(buttons[2])
                    mouse.time.append(mouse.mouseClock.getTime())
        # Run 'Each Frame' code from code_2
        # check monitor coordinates
        buttons = mouse.getPressed()
        
        if buttons == [1, 0, 0]:
            
            mouse_x, mouse_y = mouse.getPos()
            
            cursor_x_tr, cursor_y_tr, cursor_x_angle = transform_mouse(mouse_x, mouse_y)
        
            print(mouse_x, mouse_y)
            print(cursor_x_tr, cursor_y_tr, cursor_x_angle)
        
        # *text* updates
        if text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            text.frameNStart = frameN  # exact frame index
            text.tStart = t  # local t and not account for scr refresh
            text.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'text.started')
            text.setAutoDraw(True)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in CheckMonitorComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "CheckMonitor" ---
    for thisComponent in CheckMonitorComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # store data for monitor_test (TrialHandler)
    monitor_test.addData('mouse.x', mouse.x)
    monitor_test.addData('mouse.y', mouse.y)
    monitor_test.addData('mouse.leftButton', mouse.leftButton)
    monitor_test.addData('mouse.midButton', mouse.midButton)
    monitor_test.addData('mouse.rightButton', mouse.rightButton)
    monitor_test.addData('mouse.time', mouse.time)
    # the Routine "CheckMonitor" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    thisExp.nextEntry()
    
# completed 0.0 repeats of 'monitor_test'


# --- Prepare to start Routine "InitBlock" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
# Run 'Begin Routine' code from itp_n_block
#-----#
# Set mouse visibility.
if debug_mode == True:
    win.mouseVisible = True
elif debug_mode == False:
    win.mouseVisible = False

#-----#
# Load correct block of trials.
block_name = "block_{}".format(block_id + 1)
this_block = subjects_data[subject_id][sub_block_strct[session_id]][block_name]

print("Block: {}, {}".format((block_id + 1),  this_block["condition"]))

# Unpack block variables.
this_block_cursor_seq = this_block["cursor_seq"]
this_block_target_seq = this_block["target_seq"]
#this_block_ampl_seq = this_block["ampl_seq"]
this_block_catch_seq = this_block["catch_seq"]

if debug_mode == True:
    print("Catch sequence: {}".format(this_block_catch_seq))

n_trials = len(this_block_target_seq)

# Inform type of block.
this_block_type = this_block["condition"]
block_info.text = this_block_type.upper()

#-----#
# Recording durations based on recording modality.
concentration_period = 2.0

if cursor_target_sync == False:
    reaction_period = concentration_period + 1.0
elif cursor_target_sync == True:
    #concentration_period += 1.0
    go_period = concentration_period  + 1.0
    reaction_period = go_period + 3.0

task_period = reaction_period + 3.0
rebound_period = 2.0

too_slow_period = 1.0
too_fast_period = 1.0

#-----#
# Trial count.
trial_id = 0

#-----#
# Set initial target and cursor position.
target_coords = targets_coords[str(this_block_target_seq[trial_id])]

cursor_action_trial.setPos(cursor_coords)
cursor_adj_trial.setPos(cursor_coords)

if cursor_target_sync == False:
    target_action_trial.setPos(target_coords)
elif cursor_target_sync == True:
    cursor_action_trial_online.setPos(cursor_coords)
    target_action_trial_sync.setPos(target_coords)
    target_action_trial_go.setPos(target_coords)
target_adj_trial.setPos(cursor_coords)

# Set relative mouse position to target zero
# when a block starts.
mouse_action_trial.setPos(cursor_coords)
mouse_adj_trial.setPos(cursor_coords)

# Reset mouse position for adjustment period
# when a block starts.
mouse_x = 0
mouse_y = 0
# keep track of which components have finished
InitBlockComponents = [block_info]
for thisComponent in InitBlockComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "InitBlock" ---
while continueRoutine and routineTimer.getTime() < 2.0:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *block_info* updates
    if block_info.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        block_info.frameNStart = frameN  # exact frame index
        block_info.tStart = t  # local t and not account for scr refresh
        block_info.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(block_info, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'block_info.started')
        block_info.setAutoDraw(True)
    if block_info.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > block_info.tStartRefresh + 2.0-frameTolerance:
            # keep track of stop time/frame for later
            block_info.tStop = t  # not accounting for scr refresh
            block_info.frameNStop = frameN  # exact frame index
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'block_info.stopped')
            block_info.setAutoDraw(False)
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in InitBlockComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "InitBlock" ---
for thisComponent in InitBlockComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# Run 'End Routine' code from itp_n_block
# Set up a variable to store x and y coordinates
# of mouse during all valid ACTION trials. 
# (meaning the reaction time was not too
# slow or too fast)
valid_trials = []

mouse_x_pos = []
mouse_y_pos = []

mouse_ix_pos = []
mouse_iy_pos = []
# using non-slip timing so subtract the expected duration of this Routine (unless ended on request)
if routineForceEnded:
    routineTimer.reset()
else:
    routineTimer.addTime(-2.000000)

# set up handler to look after randomisation of conditions etc
TRIAL = data.TrialHandler(nReps=n_trials, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=[None],
    seed=None, name='TRIAL')
thisExp.addLoop(TRIAL)  # add the loop to the experiment
thisTRIAL = TRIAL.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisTRIAL.rgb)
if thisTRIAL != None:
    for paramName in thisTRIAL:
        exec('{} = thisTRIAL[paramName]'.format(paramName))

for thisTRIAL in TRIAL:
    currentLoop = TRIAL
    # abbreviate parameter names if possible (e.g. rgb = thisTRIAL.rgb)
    if thisTRIAL != None:
        for paramName in thisTRIAL:
            exec('{} = thisTRIAL[paramName]'.format(paramName))
    
    # --- Prepare to start Routine "BeginTrial" ---
    continueRoutine = True
    routineForceEnded = False
    # update component parameters for each repeat
    # Run 'Begin Routine' code from initialize_trial
    #-----#
    # Uniform random sample of ITP.
    itp_duration = rng.choice(list_of_durs, 1)
    
    #-----#
    # Execute an "ACTION" trial.
    n_action_trial = 1
    n_imag_trial = 0
    n_catch_trial = 0
    # keep track of which components have finished
    BeginTrialComponents = [fixation_cross_itp, arc_itp]
    for thisComponent in BeginTrialComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "BeginTrial" ---
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *fixation_cross_itp* updates
        if fixation_cross_itp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            fixation_cross_itp.frameNStart = frameN  # exact frame index
            fixation_cross_itp.tStart = t  # local t and not account for scr refresh
            fixation_cross_itp.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(fixation_cross_itp, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'fixation_cross_itp.started')
            fixation_cross_itp.setAutoDraw(True)
        if fixation_cross_itp.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > fixation_cross_itp.tStartRefresh + itp_duration + 0.05-frameTolerance:
                # keep track of stop time/frame for later
                fixation_cross_itp.tStop = t  # not accounting for scr refresh
                fixation_cross_itp.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'fixation_cross_itp.stopped')
                fixation_cross_itp.setAutoDraw(False)
        
        # *arc_itp* updates
        if arc_itp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            arc_itp.frameNStart = frameN  # exact frame index
            arc_itp.tStart = t  # local t and not account for scr refresh
            arc_itp.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(arc_itp, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'arc_itp.started')
            arc_itp.setAutoDraw(True)
        if arc_itp.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > arc_itp.tStartRefresh + itp_duration + 0.05-frameTolerance:
                # keep track of stop time/frame for later
                arc_itp.tStop = t  # not accounting for scr refresh
                arc_itp.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'arc_itp.stopped')
                arc_itp.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in BeginTrialComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "BeginTrial" ---
    for thisComponent in BeginTrialComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # the Routine "BeginTrial" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    ACTIONTRIAL = data.TrialHandler(nReps=n_action_trial, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=[None],
        seed=None, name='ACTIONTRIAL')
    thisExp.addLoop(ACTIONTRIAL)  # add the loop to the experiment
    thisACTIONTRIAL = ACTIONTRIAL.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisACTIONTRIAL.rgb)
    if thisACTIONTRIAL != None:
        for paramName in thisACTIONTRIAL:
            exec('{} = thisACTIONTRIAL[paramName]'.format(paramName))
    
    for thisACTIONTRIAL in ACTIONTRIAL:
        currentLoop = ACTIONTRIAL
        # abbreviate parameter names if possible (e.g. rgb = thisACTIONTRIAL.rgb)
        if thisACTIONTRIAL != None:
            for paramName in thisACTIONTRIAL:
                exec('{} = thisACTIONTRIAL[paramName]'.format(paramName))
        
        # --- Prepare to start Routine "ActionTrial" ---
        continueRoutine = True
        routineForceEnded = False
        # update component parameters for each repeat
        # setup some python lists for storing info about the mouse_action_trial
        mouse_action_trial.x = []
        mouse_action_trial.y = []
        mouse_action_trial.leftButton = []
        mouse_action_trial.midButton = []
        mouse_action_trial.rightButton = []
        mouse_action_trial.time = []
        gotValidClick = False  # until a click is received
        mouse_action_trial.mouseClock.reset()
        # Run 'Begin Routine' code from track_mouse
        #-----#
        # Only show mouse in debug mode.
        if debug_mode != True:
            win.mouseVisible = False
        
        #-----#
        # Retrieve cursor position of current trial.
        init_pos = str(this_block_cursor_seq[trial_id])
        init_pos_x, init_pos_y = targets_coords[init_pos]
        mouse_before = [init_pos_x, init_pos_y]
        
        # Ignore movements made between trials.
        mouse_action_trial.setPos(targets_coords[init_pos])
        
        # Retrieve target position for current trial.
        target_pos = str(this_block_target_seq[trial_id])
        target_x, target_y = targets_coords[target_pos]
        
        if int(target_pos) > 0:
            pre_tp = "+"
        else:
            pre_tp = ""
        degrees_info.text = "Press any button when you reach the target at {}{}°".format(pre_tp, target_pos)
        
        if debug_mode == True:
            print("Trial's start position: {}, {}".format(init_pos_x, init_pos_y))
            print("Trial's target position: {}, {}".format(target_x, target_y))
            print("Trial timings (c/r/t/rb): {}, {}, {}, {}".format(concentration_period, reaction_period, task_period, rebound_period))
        
        #-----#
        # Reset variable that indicates whether a trial
        # ended with reaching the target or not.
        is_valid = True
        
        #-----#
        # Reset variable that saves the state of trial with
        # unacceptable reaction time.
        too_slow = False
        too_fast = False
        
        # Monitor mouse status in the trial, but only check
        # it following the concentration period.
        mouse_is_not_moving_t = np.array([])
        mouse_is_not_moving_count_t = 1.0
        
        mouse_xs = np.array([])
        mouse_ys = np.array([])
        key_resp_2.keys = []
        key_resp_2.rt = []
        _key_resp_2_allKeys = []
        # keep track of which components have finished
        ActionTrialComponents = [fixation_cross_action_trial, arc_action_trial, mouse_action_trial, cursor_action_trial, target_action_trial, target_action_trial_sync, target_action_trial_go, cursor_action_trial_online, key_resp_2, degrees_info]
        for thisComponent in ActionTrialComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "ActionTrial" ---
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *fixation_cross_action_trial* updates
            if fixation_cross_action_trial.status == NOT_STARTED and n_action_trial == 1.0:
                # keep track of start time/frame for later
                fixation_cross_action_trial.frameNStart = frameN  # exact frame index
                fixation_cross_action_trial.tStart = t  # local t and not account for scr refresh
                fixation_cross_action_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(fixation_cross_action_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'fixation_cross_action_trial.started')
                fixation_cross_action_trial.setAutoDraw(True)
            
            # *arc_action_trial* updates
            if arc_action_trial.status == NOT_STARTED and n_action_trial == 1.0:
                # keep track of start time/frame for later
                arc_action_trial.frameNStart = frameN  # exact frame index
                arc_action_trial.tStart = t  # local t and not account for scr refresh
                arc_action_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(arc_action_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'arc_action_trial.started')
                arc_action_trial.setAutoDraw(True)
            # *mouse_action_trial* updates
            if mouse_action_trial.status == NOT_STARTED and n_action_trial == 1.0:
                # keep track of start time/frame for later
                mouse_action_trial.frameNStart = frameN  # exact frame index
                mouse_action_trial.tStart = t  # local t and not account for scr refresh
                mouse_action_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(mouse_action_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'mouse_action_trial.started')
                mouse_action_trial.status = STARTED
                prevButtonState = mouse_action_trial.getPressed()  # if button is down already this ISN'T a new click
            if mouse_action_trial.status == STARTED:  # only update if started and not finished!
                x, y = mouse_action_trial.getPos()
                mouse_action_trial.x.append(x)
                mouse_action_trial.y.append(y)
                buttons = mouse_action_trial.getPressed()
                mouse_action_trial.leftButton.append(buttons[0])
                mouse_action_trial.midButton.append(buttons[1])
                mouse_action_trial.rightButton.append(buttons[2])
                mouse_action_trial.time.append(mouse_action_trial.mouseClock.getTime())
            
            # *cursor_action_trial* updates
            if cursor_action_trial.status == NOT_STARTED and n_action_trial == 1.0:
                # keep track of start time/frame for later
                cursor_action_trial.frameNStart = frameN  # exact frame index
                cursor_action_trial.tStart = t  # local t and not account for scr refresh
                cursor_action_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(cursor_action_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'cursor_action_trial.started')
                cursor_action_trial.setAutoDraw(True)
            if cursor_action_trial.status == STARTED:
                if bool((t > concentration_period)):
                    # keep track of stop time/frame for later
                    cursor_action_trial.tStop = t  # not accounting for scr refresh
                    cursor_action_trial.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'cursor_action_trial.stopped')
                    cursor_action_trial.setAutoDraw(False)
            
            # *target_action_trial* updates
            if target_action_trial.status == NOT_STARTED and (n_action_trial == 1.0) and (cursor_target_sync == False) and (t > concentration_period):
                # keep track of start time/frame for later
                target_action_trial.frameNStart = frameN  # exact frame index
                target_action_trial.tStart = t  # local t and not account for scr refresh
                target_action_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(target_action_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'target_action_trial.started')
                target_action_trial.setAutoDraw(True)
            
            # *target_action_trial_sync* updates
            if target_action_trial_sync.status == NOT_STARTED and (n_action_trial == 1.0) and (cursor_target_sync == True):
                # keep track of start time/frame for later
                target_action_trial_sync.frameNStart = frameN  # exact frame index
                target_action_trial_sync.tStart = t  # local t and not account for scr refresh
                target_action_trial_sync.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(target_action_trial_sync, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'target_action_trial_sync.started')
                target_action_trial_sync.setAutoDraw(True)
            if target_action_trial_sync.status == STARTED:
                if bool((t >= go_period)):
                    # keep track of stop time/frame for later
                    target_action_trial_sync.tStop = t  # not accounting for scr refresh
                    target_action_trial_sync.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_action_trial_sync.stopped')
                    target_action_trial_sync.setAutoDraw(False)
            
            # *target_action_trial_go* updates
            if target_action_trial_go.status == NOT_STARTED and (n_action_trial == 1.0) and (cursor_target_sync == True) and (t >= concentration_period):
                # keep track of start time/frame for later
                target_action_trial_go.frameNStart = frameN  # exact frame index
                target_action_trial_go.tStart = t  # local t and not account for scr refresh
                target_action_trial_go.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(target_action_trial_go, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'target_action_trial_go.started')
                target_action_trial_go.setAutoDraw(True)
            
            # *cursor_action_trial_online* updates
            if cursor_action_trial_online.status == NOT_STARTED and (t > concentration_period):
                # keep track of start time/frame for later
                cursor_action_trial_online.frameNStart = frameN  # exact frame index
                cursor_action_trial_online.tStart = t  # local t and not account for scr refresh
                cursor_action_trial_online.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(cursor_action_trial_online, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'cursor_action_trial_online.started')
                cursor_action_trial_online.setAutoDraw(True)
            # Run 'Each Frame' code from track_mouse
            #-----#
            # Update cursor position.
            # Mouse control has been passed to the ACTION TRIAL ADJ routine !!!
            # (because mouse_adj_trial is spawned 2nd ???)
            # Yet the mouseMoved() check has to be made for the current window !!!
            # (using mouse_action_trial)
            mouse_x, mouse_y = mouse_adj_trial.getPos()
            
            #-----#
            # Apply necessary tranformations in the recorded coordinates.
            #cursor_x_sc, cursor_x_tr, cursor_x_angle = transform_x(mouse_x)
            #cursor_y_tr = transform_y(cursor_x_sc)
            cursor_x_tr, cursor_y_tr, cursor_x_angle = transform_mouse(mouse_x, mouse_y)
            
            #-----#
            # Forcefully end the trial if the reaction time
            # is very fasrt, very slow, if the cursor has
            # reached the target before the task
            # duration limit has exprired, or if it has
            # stopped moving for a while.
            
            # Check for cases of early trial termination.
            if t < concentration_period:
            
                # The trial is aborted if the mouse is moved
                # before the target appears on screen.
                #if (mouse_x < init_pos_x - mouse_conf) or\
                #    (mouse_x > init_pos_x + mouse_conf) or\
                #    (mouse_y < init_pos_y - mouse_conf) or\
                #    (mouse_y > init_pos_y + mouse_conf):
            
                # The mouse is forced not to move from the
                # neutral position.
                mouse_action_trial.setPos(targets_coords[init_pos])
                    
                    # Trigger "TOO FAST" message in "ActionTrialAdj" Routine.
                    #too_fast = True
                    #continueRoutine = False
                    #if debug_mode == True:
                    #    print("Stop reason: TOO FAST")
            
            elif t >= concentration_period:
            
                # Provide online feedback of cursor position.
                cursor_action_trial_online.setPos([cursor_x_tr[-1], cursor_y_tr[-1]])
                cursor_action_trial_online.setOri(cursor_x_angle[-1])
            
                print(cursor_x_tr[-1], cursor_y_tr[-1])
                
                # Monitor consecutive frames of mouse inactivity.
                mmv_t = str(mouse_action_trial.mouseMoved())
                if mmv_t == "True":
                    mouse_is_not_moving_t = np.append(mouse_is_not_moving_t, 0)
                elif mmv_t == "False":
                   mouse_is_not_moving_t = np.append(mouse_is_not_moving_t, 1)
                
                mouse_is_not_moving_count_t = np.sum(np.flip(mouse_is_not_moving_t)[:not_moved_id])
                
                # Store last mouse state.
                mouse_xs = np.append(mouse_xs, mouse_x)
                mouse_ys = np.append(mouse_ys, mouse_y)
            
                # End trial if the mouse hasn't moved within
                # the given reaction period.
                if (t >= reaction_period) and\
                    (mouse_x < init_pos_x + mouse_conf) and\
                    (mouse_x > init_pos_x - mouse_conf) and\
                    (mouse_y < init_pos_y + mouse_conf) and\
                    (mouse_y > init_pos_y - mouse_conf):
                    
                    # Trigger "TOO SLOW" message in "ActionTrialAdj" Routine.
                    too_slow = True
                    is_valid = False
                    continueRoutine = False
                    if debug_mode == True:
                        print("Stop reason: TOO SLOW")
            
                # End trial if the mouse has reached the target
                # before the trial max duration.
                #elif (cursor_x_tr < target_x + mouse_conf) and\
                #    (cursor_x_tr > target_x - mouse_conf) and\
                #   (cursor_y_tr < target_y + mouse_conf) and\
                #   (cursor_y_tr > target_y - mouse_conf) and\
                #    (mouse_is_not_moving_count_t >= not_moved_id):
                    
                #    continueRoutine = False
                #    if debug_mode == True:
                #       print("Stop reason: TARGET REACHED")
            
                # End trial if the mouse has stopped moving
                # for a certain amount of time before reaching
                # the trial max duration.
                elif (mouse_is_not_moving_count_t >= not_moved_id):
                
                    too_slow = True
                    is_valid = False
                    continueRoutine = False
                    if debug_mode == True:
                        print("Stop reason: MOUSE NOT MOVING FOR {}s".format(not_moving_dur))
            
            """
            # End trial if mouse keeps moving.
            elif t >= task_period:
                    
                continueRoutine = False
                if debug_mode == True:
                    print("Stop reason: REACHED MAX TRIAL DURATION")
            """
            
            # *key_resp_2* updates
            waitOnFlip = False
            if key_resp_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                key_resp_2.frameNStart = frameN  # exact frame index
                key_resp_2.tStart = t  # local t and not account for scr refresh
                key_resp_2.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(key_resp_2, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'key_resp_2.started')
                key_resp_2.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(key_resp_2.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(key_resp_2.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if key_resp_2.status == STARTED and not waitOnFlip:
                theseKeys = key_resp_2.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
                _key_resp_2_allKeys.extend(theseKeys)
                if len(_key_resp_2_allKeys):
                    key_resp_2.keys = _key_resp_2_allKeys[-1].name  # just the last key pressed
                    key_resp_2.rt = _key_resp_2_allKeys[-1].rt
                    # a response ends the routine
                    continueRoutine = False
            
            # *degrees_info* updates
            if degrees_info.status == NOT_STARTED and n_action_trial == 1.0:
                # keep track of start time/frame for later
                degrees_info.frameNStart = frameN  # exact frame index
                degrees_info.tStart = t  # local t and not account for scr refresh
                degrees_info.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(degrees_info, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'degrees_info.started')
                degrees_info.setAutoDraw(True)
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in ActionTrialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "ActionTrial" ---
        for thisComponent in ActionTrialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        # store data for ACTIONTRIAL (TrialHandler)
        ACTIONTRIAL.addData('mouse_action_trial.x', mouse_action_trial.x)
        ACTIONTRIAL.addData('mouse_action_trial.y', mouse_action_trial.y)
        ACTIONTRIAL.addData('mouse_action_trial.leftButton', mouse_action_trial.leftButton)
        ACTIONTRIAL.addData('mouse_action_trial.midButton', mouse_action_trial.midButton)
        ACTIONTRIAL.addData('mouse_action_trial.rightButton', mouse_action_trial.rightButton)
        ACTIONTRIAL.addData('mouse_action_trial.time', mouse_action_trial.time)
        # Run 'End Routine' code from track_mouse
        #-----#
        # Debug message.
        if debug_mode == True:
            print("Frames' threshold for mouse inactivity: {}".format(not_moved_id))
            print("Trial duration: {}s".format(t))
        
        #-----#
        # Store the time needed to complete each trial.
        if too_slow == False and too_fast == False and is_valid == True:
            
            valid_trials.append(target_pos)
        
            mouse_x_pos.append(mouse_xs)
            mouse_y_pos.append(mouse_ys)
        
            if debug_mode == True:
                print("Valid {} trial duration: {}s".format(target_pos, t))
        # check responses
        if key_resp_2.keys in ['', [], None]:  # No response was made
            key_resp_2.keys = None
        ACTIONTRIAL.addData('key_resp_2.keys',key_resp_2.keys)
        if key_resp_2.keys != None:  # we had a response
            ACTIONTRIAL.addData('key_resp_2.rt', key_resp_2.rt)
        # the Routine "ActionTrial" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        
        # --- Prepare to start Routine "ActionTrialAdj" ---
        continueRoutine = True
        routineForceEnded = False
        # update component parameters for each repeat
        # setup some python lists for storing info about the mouse_adj_trial
        mouse_adj_trial.x = []
        mouse_adj_trial.y = []
        mouse_adj_trial.leftButton = []
        mouse_adj_trial.midButton = []
        mouse_adj_trial.rightButton = []
        mouse_adj_trial.time = []
        gotValidClick = False  # until a click is received
        mouse_adj_trial.mouseClock.reset()
        # Run 'Begin Routine' code from adj_cursor_position
        #-----#
        # Only show mouse in debug mode.
        if debug_mode != True:
            win.mouseVisible = False
        
        #-----#
        # Target position for trial. We just make sure
        # to bring the mouse to the neutral position.
        target_adj_x, target_adj_y = targets_coords["0"]
        
        if debug_mode == True:
            print("Last mouse state: {}".format([mouse_x, mouse_y]))
            print("Was the trial 'slow'? : {}".format(too_slow))
            print("Was the trial 'fast'? : {}".format(too_fast))
        
        #-----#
        # Save mouse coordinates when going back
        # to neutral position.
        mouse_ixs = np.array([])
        mouse_iys = np.array([])
        key_resp_3.keys = []
        key_resp_3.rt = []
        _key_resp_3_allKeys = []
        # keep track of which components have finished
        ActionTrialAdjComponents = [slow_trial_info, fast_trial_info, text_adjust, fixation_cross_adj_trial, arc_adj_trial, mouse_adj_trial, target_adj_trial, cursor_adj_trial, key_resp_3]
        for thisComponent in ActionTrialAdjComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "ActionTrialAdj" ---
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *slow_trial_info* updates
            if slow_trial_info.status == NOT_STARTED and too_slow == True:
                # keep track of start time/frame for later
                slow_trial_info.frameNStart = frameN  # exact frame index
                slow_trial_info.tStart = t  # local t and not account for scr refresh
                slow_trial_info.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(slow_trial_info, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'slow_trial_info.started')
                slow_trial_info.setAutoDraw(True)
            if slow_trial_info.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > slow_trial_info.tStartRefresh + too_slow_period-frameTolerance:
                    # keep track of stop time/frame for later
                    slow_trial_info.tStop = t  # not accounting for scr refresh
                    slow_trial_info.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'slow_trial_info.stopped')
                    slow_trial_info.setAutoDraw(False)
            
            # *fast_trial_info* updates
            if fast_trial_info.status == NOT_STARTED and too_fast == True:
                # keep track of start time/frame for later
                fast_trial_info.frameNStart = frameN  # exact frame index
                fast_trial_info.tStart = t  # local t and not account for scr refresh
                fast_trial_info.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(fast_trial_info, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'fast_trial_info.started')
                fast_trial_info.setAutoDraw(True)
            if fast_trial_info.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > fast_trial_info.tStartRefresh + too_fast_period-frameTolerance:
                    # keep track of stop time/frame for later
                    fast_trial_info.tStop = t  # not accounting for scr refresh
                    fast_trial_info.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'fast_trial_info.stopped')
                    fast_trial_info.setAutoDraw(False)
            
            # *text_adjust* updates
            if text_adjust.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                text_adjust.frameNStart = frameN  # exact frame index
                text_adjust.tStart = t  # local t and not account for scr refresh
                text_adjust.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(text_adjust, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'text_adjust.started')
                text_adjust.setAutoDraw(True)
            
            # *fixation_cross_adj_trial* updates
            if fixation_cross_adj_trial.status == NOT_STARTED and (too_slow == False and too_fast == False) or (too_slow == True and t > too_slow_period) or (too_fast == True and t > too_slow_period):
                # keep track of start time/frame for later
                fixation_cross_adj_trial.frameNStart = frameN  # exact frame index
                fixation_cross_adj_trial.tStart = t  # local t and not account for scr refresh
                fixation_cross_adj_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(fixation_cross_adj_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'fixation_cross_adj_trial.started')
                fixation_cross_adj_trial.setAutoDraw(True)
            
            # *arc_adj_trial* updates
            if arc_adj_trial.status == NOT_STARTED and (too_slow == False and too_fast == False) or (too_slow == True and t > too_slow_period) or (too_fast == True and t > too_slow_period):
                # keep track of start time/frame for later
                arc_adj_trial.frameNStart = frameN  # exact frame index
                arc_adj_trial.tStart = t  # local t and not account for scr refresh
                arc_adj_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(arc_adj_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'arc_adj_trial.started')
                arc_adj_trial.setAutoDraw(True)
            # *mouse_adj_trial* updates
            if mouse_adj_trial.status == NOT_STARTED and (too_slow == False and too_fast == False) or (too_slow == True and t > too_slow_period) or (too_fast == True and t > too_slow_period):
                # keep track of start time/frame for later
                mouse_adj_trial.frameNStart = frameN  # exact frame index
                mouse_adj_trial.tStart = t  # local t and not account for scr refresh
                mouse_adj_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(mouse_adj_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'mouse_adj_trial.started')
                mouse_adj_trial.status = STARTED
                prevButtonState = mouse_adj_trial.getPressed()  # if button is down already this ISN'T a new click
            if mouse_adj_trial.status == STARTED:  # only update if started and not finished!
                x, y = mouse_adj_trial.getPos()
                mouse_adj_trial.x.append(x)
                mouse_adj_trial.y.append(y)
                buttons = mouse_adj_trial.getPressed()
                mouse_adj_trial.leftButton.append(buttons[0])
                mouse_adj_trial.midButton.append(buttons[1])
                mouse_adj_trial.rightButton.append(buttons[2])
                mouse_adj_trial.time.append(mouse_adj_trial.mouseClock.getTime())
            
            # *target_adj_trial* updates
            if target_adj_trial.status == NOT_STARTED and (too_slow == False and too_fast == False) or (too_slow == True and t > too_slow_period) or (too_fast == True and t > too_slow_period):
                # keep track of start time/frame for later
                target_adj_trial.frameNStart = frameN  # exact frame index
                target_adj_trial.tStart = t  # local t and not account for scr refresh
                target_adj_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(target_adj_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'target_adj_trial.started')
                target_adj_trial.setAutoDraw(True)
            
            # *cursor_adj_trial* updates
            if cursor_adj_trial.status == NOT_STARTED and (too_slow == False and too_fast == False) or (too_slow == True and t > too_slow_period) or (too_fast == True and t > too_slow_period):
                # keep track of start time/frame for later
                cursor_adj_trial.frameNStart = frameN  # exact frame index
                cursor_adj_trial.tStart = t  # local t and not account for scr refresh
                cursor_adj_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(cursor_adj_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'cursor_adj_trial.started')
                cursor_adj_trial.setAutoDraw(True)
            # Run 'Each Frame' code from adj_cursor_position
            #-----#
            # Update cursor position.
            mouse_x, mouse_y = mouse_adj_trial.getPos()
            
            #-----#
            # Apply necessary tranformations in the recorded coordinates.
            #cursor_x_sc, cursor_x_tr, cursor_x_angle = transform_x(mouse_x)
            #cursor_y_tr = transform_y(cursor_x_sc)
            cursor_x_tr, cursor_y_tr, cursor_x_angle = transform_mouse(mouse_x, mouse_y)
            
            cursor_adj_trial.setPos([cursor_x_tr[-1], cursor_y_tr[-1]])
            cursor_adj_trial.setOri(cursor_x_angle[-1])
            
            #-----#
            # End adjustment when the cursor has moved
            # to the target.
            
            # Store last mouse state.
            mouse_ixs = np.append(mouse_ixs, mouse_x)
            mouse_iys = np.append(mouse_iys, mouse_y)
            
            # *key_resp_3* updates
            waitOnFlip = False
            if key_resp_3.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                # keep track of start time/frame for later
                key_resp_3.frameNStart = frameN  # exact frame index
                key_resp_3.tStart = t  # local t and not account for scr refresh
                key_resp_3.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(key_resp_3, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'key_resp_3.started')
                key_resp_3.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(key_resp_3.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(key_resp_3.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if key_resp_3.status == STARTED and not waitOnFlip:
                theseKeys = key_resp_3.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
                _key_resp_3_allKeys.extend(theseKeys)
                if len(_key_resp_3_allKeys):
                    key_resp_3.keys = _key_resp_3_allKeys[-1].name  # just the last key pressed
                    key_resp_3.rt = _key_resp_3_allKeys[-1].rt
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in ActionTrialAdjComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "ActionTrialAdj" ---
        for thisComponent in ActionTrialAdjComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        # store data for ACTIONTRIAL (TrialHandler)
        ACTIONTRIAL.addData('mouse_adj_trial.x', mouse_adj_trial.x)
        ACTIONTRIAL.addData('mouse_adj_trial.y', mouse_adj_trial.y)
        ACTIONTRIAL.addData('mouse_adj_trial.leftButton', mouse_adj_trial.leftButton)
        ACTIONTRIAL.addData('mouse_adj_trial.midButton', mouse_adj_trial.midButton)
        ACTIONTRIAL.addData('mouse_adj_trial.rightButton', mouse_adj_trial.rightButton)
        ACTIONTRIAL.addData('mouse_adj_trial.time', mouse_adj_trial.time)
        # Run 'End Routine' code from adj_cursor_position
        #-----#
        # Store adjustment trial's mouse coordinates.
        if too_slow == False and too_fast == False and is_valid == True:
            mouse_ix_pos.append(mouse_ixs)
            mouse_iy_pos.append(mouse_iys)
        # check responses
        if key_resp_3.keys in ['', [], None]:  # No response was made
            key_resp_3.keys = None
        ACTIONTRIAL.addData('key_resp_3.keys',key_resp_3.keys)
        if key_resp_3.keys != None:  # we had a response
            ACTIONTRIAL.addData('key_resp_3.rt', key_resp_3.rt)
        # the Routine "ActionTrialAdj" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        thisExp.nextEntry()
        
    # completed n_action_trial repeats of 'ACTIONTRIAL'
    
    
    # --- Prepare to start Routine "EndTrial" ---
    continueRoutine = True
    routineForceEnded = False
    # update component parameters for each repeat
    # Run 'Begin Routine' code from finalize_trial
    #-----#   
    # Debug message.
    if debug_mode == True:
        print("End of trial {}.\n".format(trial_id+1))
    
    #-----#
    # Starting position of next trial corresponds to
    # zero. 
    cursor_position = "0"
    
    cursor_action_trial.setPos(targets_coords[cursor_position])
    
    # Update count of trials.
    if trial_id < (n_trials - 1):
    
        trial_id += 1
        target_position =  str(this_block_target_seq[trial_id])
    
        # Update target position.
        if cursor_target_sync == False:
            target_action_trial.setPos(targets_coords[target_position])
        elif cursor_target_sync == True:
            target_action_trial_sync.setPos(targets_coords[target_position])
            target_action_trial_go.setPos(targets_coords[target_position])
    
    #-----#
    # Terminate without allowing the arc and the fixation cross to appear
    # following a CATCH trial, or between blocks.
    if (trial_id == n_trials - 1):
        
        continueRoutine = False
    # keep track of which components have finished
    EndTrialComponents = [fixation_cross_end, arc_itp_end]
    for thisComponent in EndTrialComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "EndTrial" ---
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *fixation_cross_end* updates
        if fixation_cross_end.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
            # keep track of start time/frame for later
            fixation_cross_end.frameNStart = frameN  # exact frame index
            fixation_cross_end.tStart = t  # local t and not account for scr refresh
            fixation_cross_end.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(fixation_cross_end, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'fixation_cross_end.started')
            fixation_cross_end.setAutoDraw(True)
        if fixation_cross_end.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > fixation_cross_end.tStartRefresh + 0.1 + avoid_flicker-frameTolerance:
                # keep track of stop time/frame for later
                fixation_cross_end.tStop = t  # not accounting for scr refresh
                fixation_cross_end.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'fixation_cross_end.stopped')
                fixation_cross_end.setAutoDraw(False)
        
        # *arc_itp_end* updates
        if arc_itp_end.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
            # keep track of start time/frame for later
            arc_itp_end.frameNStart = frameN  # exact frame index
            arc_itp_end.tStart = t  # local t and not account for scr refresh
            arc_itp_end.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(arc_itp_end, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'arc_itp_end.started')
            arc_itp_end.setAutoDraw(True)
        if arc_itp_end.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > arc_itp_end.tStartRefresh + 0.1 + avoid_flicker-frameTolerance:
                # keep track of stop time/frame for later
                arc_itp_end.tStop = t  # not accounting for scr refresh
                arc_itp_end.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'arc_itp_end.stopped')
                arc_itp_end.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in EndTrialComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "EndTrial" ---
    for thisComponent in EndTrialComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # the Routine "EndTrial" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    thisExp.nextEntry()
    
# completed n_trials repeats of 'TRIAL'


# --- Prepare to start Routine "CalibrationDataSave" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
# keep track of which components have finished
CalibrationDataSaveComponents = []
for thisComponent in CalibrationDataSaveComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "CalibrationDataSave" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in CalibrationDataSaveComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "CalibrationDataSave" ---
for thisComponent in CalibrationDataSaveComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "CalibrationDataSave" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "ByeByeScreen" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
# keep track of which components have finished
ByeByeScreenComponents = [bye_bye]
for thisComponent in ByeByeScreenComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "ByeByeScreen" ---
while continueRoutine and routineTimer.getTime() < 5.0:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *bye_bye* updates
    if bye_bye.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        bye_bye.frameNStart = frameN  # exact frame index
        bye_bye.tStart = t  # local t and not account for scr refresh
        bye_bye.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(bye_bye, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'bye_bye.started')
        bye_bye.setAutoDraw(True)
    if bye_bye.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > bye_bye.tStartRefresh + 5.0-frameTolerance:
            # keep track of stop time/frame for later
            bye_bye.tStop = t  # not accounting for scr refresh
            bye_bye.frameNStop = frameN  # exact frame index
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'bye_bye.stopped')
            bye_bye.setAutoDraw(False)
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in ByeByeScreenComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "ByeByeScreen" ---
for thisComponent in ByeByeScreenComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# using non-slip timing so subtract the expected duration of this Routine (unless ended on request)
if routineForceEnded:
    routineTimer.reset()
else:
    routineTimer.addTime(-5.000000)
# Run 'End Experiment' code from save_cal_data
#-----#
# Save calibration data.

cal_path = join(datapath, "linux")

np.save(cal_path + "valid_calibration_trials", valid_trials)

np.save(cal_path + "mouse_x_pos", mouse_x_pos)
np.save(cal_path + "mouse_y_pos", mouse_y_pos)

np.save(cal_path + "mouse_ix_pos", mouse_ix_pos)
np.save(cal_path + "mouse_iy_pos", mouse_iy_pos)

# --- End experiment ---
# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv', delim='auto')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
if eyetracker:
    eyetracker.setConnectionState(False)
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
