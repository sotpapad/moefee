#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2022.2.5),
    on Τρι 16 Απρ 2024 01:24:38 μμ 
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

# --- Import packages ---
from psychopy import locale_setup
from psychopy import prefs
from psychopy import sound, gui, visual, core, data, event, logging, clock, colors, layout
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle, choice as randchoice
import os  # handy system and path functions
import sys  # to get file system encoding

import psychopy.iohub as io
from psychopy.hardware import keyboard

# Run 'Before Experiment' code from load_data
import pickle
import time
from psychopy import prefs

#-----#
debug_mode = True

#-----#
# Which units are being used?
units = prefs.general["units"]
if debug_mode == True:
    print("Units used in the experiment: '{}'".format(units))

#-----#
#cmd = 'xrandr --output {} --{}-of {}'.format(name, way, self.primary)


# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)
# Store info about the experiment session
psychopyVersion = '2022.2.5'
expName = 'instructions_session'  # from the Builder filename that created this script
expInfo = {
    '': '',
}
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/Sub_%s_%s' % (expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='/home/sotpapad/git_stuff/moefee/experimental_paradigm/instructions.py',
    savePickle=True, saveWideText=False,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp
frameTolerance = 0.001  # how close to onset before 'same' frame

# Start Code - component code to be run after the window creation

# --- Setup the Window ---
win = visual.Window(
    size=[1920, 1080], fullscr=True, screen=0, 
    winType='pyglet', allowStencil=False,
    monitor='Laptop_monitor', color=[0,0,0], colorSpace='rgb',
    blendMode='avg', useFBO=True)
win.mouseVisible = False
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess
# --- Setup input devices ---
ioConfig = {}

# Setup iohub keyboard
ioConfig['Keyboard'] = dict(use_keymap='psychopy')

ioSession = '1'
if 'session' in expInfo:
    ioSession = str(expInfo['session'])
ioServer = io.launchHubServer(window=win, **ioConfig)
eyetracker = None

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard(backend='iohub')

# --- Initialize components for Routine "LoadData" ---
# Run 'Begin Experiment' code from load_data
#-----#
# Relative coordinates of targets positions on screen.
# [-60, -30, 0, 30, 60]
targets_coords = {
        "-60": (-0.3247, 0.1875),
        "-30": (-0.1875, 0.3247),
        "0": (0.0, 0.375),
        "30": (0.1875, 0.3247),
        "60": (0.3247, 0.1875)
        }

targets_catch_coords = {
        "-75": (-0.362,0.097),
        "-60": (-0.3247, 0.1875),
        "-45": (-0.2652, 0.2652),
        "-30": (-0.1875, 0.3247),
        "-20": (-0.1283, 0.3524),
        "0": (0.0, 0.375),
        "20": (0.1283, 0.3524),
        "30": (0.1875, 0.3247),
        "45": (0.2652, 0.2652),
        "60": (0.3247, 0.1875),
        "75": (0.362,0.097),
        }

targets_catch_degrees = {
        "-75": (-75.0),
        "-60": (-60.0),
        "-45": (-45.0),
        "-30": (-30.0),
        "-20": (-20.0),
        "0": (0.0),
        "20": (20.0),
        "30": (30.0),
        "45": (45.0),
        "60": (60.0),
        "75": (75.0),
        }

#-----#
# Cursor and target positions.
cursor_pos = [(0.0, 0.375),
                        (0.0, 0.375)]
target_pos = [(-0.3247, 0.1875),
                        (0.1875, 0.3247)]
target_degrees = [(-60.0),
                        (30.0)]

#-----#
# Show cursor and target controlling the opacity.
show_cursor = 0.0
show_target = 0.0
show_go = 0.0
show_cursor_adj = 0.0

# Positions.
id_pos = 0

#-----#
# Targets, cursor, arc and fixation cross properties.
target_size = (0.10)
target_fill_col = (-1.0000, -1.0000, 1.0000)
target_border_col = (-1.0000, -1.0000, 1.0000)
target_fill_col_go = (1.0000, -1.0000, 1.0000)
target_border_col_go = (1.0000, -1.0000, 1.0000)

cursor_size = (0.06)
cursor_fill_col = (1.0000, -1.0000, -1.0000)
cursor_border_col = (1.0000, -1.0000, -1.0000)
cursor_adj_fill_col = (1.0000, 0.2941, -1.0000)
cursor_adj_border_col = (1.0000, 0.2941, -1.0000)

arc_size = (0.75, 0.4)
arc_position = (0.0, 0.18)
arc_image = "images/arc_new.png"
arc_catch_image = "images/arc_catch_new.png"
arc_fg_col = [1,1,1]

fix_size = (0.04)
fix_position = (0.0, 0.0)
fix_col = "white"

#-----#
# Text properties.
text_size = 0.03
text_wrap = 1.5

# --- Initialize components for Routine "WelcomeScreen" ---
welcome_message = visual.TextStim(win=win, name='welcome_message',
    text='Welcome to the training session.\n\nPress any button to move to the next screen.',
    font='Open Sans',
    pos=(0.0, 0.0), height=text_size, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp = keyboard.Keyboard()

# --- Initialize components for Routine "InstructionsInit" ---
instr_init_msg_1 = visual.TextStim(win=win, name='instr_init_msg_1',
    text='During this experiment you will be required to perform repeated real or imagined movements of your hands.',
    font='Open Sans',
    pos=(0.0, 0.0), height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp_2 = keyboard.Keyboard()

# --- Initialize components for Routine "InstructionsInit_2" ---
instr_init_msg = visual.TextStim(win=win, name='instr_init_msg',
    text='It is important that throughout the duration of the experiment you try to maintain your gaze at the fixation cross that will appear at the center of the screen.',
    font='Open Sans',
    pos=(0.0, -0.2), height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
instr_init_fix_cross_2 = visual.ShapeStim(
    win=win, name='instr_init_fix_cross_2', vertices='cross',units='norm', 
    size=(0.04, 0.06),
    ori=0.0, pos=(0, 0), anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor='white', fillColor='white',
    opacity=None, depth=-1.0, interpolate=True)
key_resp_4 = keyboard.Keyboard()

# --- Initialize components for Routine "InstructionsInit_3" ---
instr_init_msg_3 = visual.TextStim(win=win, name='instr_init_msg_3',
    text='The experiment is organized in a small number of blocks, each consisting of multiple trials.\n\nBefore each block of trials, you will be instructed as to whether you will be required to move your hand (ACTION) or imagine the experience of moving it (IMAGERY).',
    font='Open Sans',
    pos=(0.0, 0.0), height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp_3 = keyboard.Keyboard()

# --- Initialize components for Routine "InstructionsMain" ---
instr_main_msg_1 = visual.TextStim(win=win, name='instr_main_msg_1',
    text="During each trial, 1 out of 4 possible targets will appear on an arc path on the screen.\n\nTargets are represented by blue circles.\n\nThe target's position will vary between trials.",
    font='Open Sans',
    pos=(0.0, -0.2), height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
arc_instructions_main = visual.ImageStim(
    win=win,
    name='arc_instructions_main', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-1.0)
fixation_cross_instructions_main = visual.ShapeStim(
    win=win, name='fixation_cross_instructions_main', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=-2.0, interpolate=True)
instr_main_target_p_30 = visual.ShapeStim(
    win=win, name='instr_main_target_p_30',
    size=target_size, vertices='circle',
    ori=0.0, pos=(0.1875, 0.3247), anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-3.0, interpolate=True)
instr_main_target_m_30 = visual.ShapeStim(
    win=win, name='instr_main_target_m_30',
    size=target_size, vertices='circle',
    ori=0.0, pos=(-0.1875, 0.3247), anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-4.0, interpolate=True)
instr_main_target_p_60 = visual.ShapeStim(
    win=win, name='instr_main_target_p_60',
    size=target_size, vertices='circle',
    ori=0.0, pos=(0.3247, 0.1875), anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-5.0, interpolate=True)
instr_main_target_m_60 = visual.ShapeStim(
    win=win, name='instr_main_target_m_60',
    size=target_size, vertices='circle',
    ori=0.0, pos=(-0.3247, 0.1875), anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-6.0, interpolate=True)
key_resp_5 = keyboard.Keyboard()

# --- Initialize components for Routine "InstructionsMain_2" ---
instr_main_msg = visual.TextStim(win=win, name='instr_main_msg',
    text='At the begining of each trial a red cursor indicating your current hand position will also appear on the screen.\n\nWhen the target color changes from blue to magenta you should move the cursor to the target either by moving your hand, or by imagining doing so.\n',
    font='Open Sans',
    pos=(0.0, -0.2), height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
arc_instructions_main_3 = visual.ImageStim(
    win=win,
    name='arc_instructions_main_3', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-1.0)
fixation_cross_instructions_main_3 = visual.ShapeStim(
    win=win, name='fixation_cross_instructions_main_3', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=-2.0, interpolate=True)
cursor_instructions_main = visual.ShapeStim(
    win=win, name='cursor_instructions_main',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=(0.0, 0.375), anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_border_col, fillColor=cursor_fill_col,
    opacity=show_cursor, depth=-3.0, interpolate=True)
instr_main_target_m_60_3 = visual.ShapeStim(
    win=win, name='instr_main_target_m_60_3',
    size=target_size, vertices='circle',
    ori=0.0, pos=(-0.3247, 0.1875), anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=show_target, depth=-4.0, interpolate=True)
inst_main_target_m_60_go = visual.ShapeStim(
    win=win, name='inst_main_target_m_60_go',
    size=target_size, vertices='circle',
    ori=0.0, pos=(-0.3247, 0.1875), anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col_go, fillColor=target_fill_col_go,
    opacity=show_go, depth=-5.0, interpolate=True)
cursor_adj_go = visual.ShapeStim(
    win=win, name='cursor_adj_go',
    size=cursor_size, vertices='triangle',
    ori=targets_catch_degrees["-60"], pos=(-0.3247, 0.1875), anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_cursor_adj, depth=-6.0, interpolate=True)
key_resp_6 = keyboard.Keyboard()

# --- Initialize components for Routine "InstructionsMain_3" ---
instr_main_msg_2 = visual.TextStim(win=win, name='instr_main_msg_2',
    text="After each ACTION trial you will be asked to bring the cursor back to the neutral position. This will be indicated by a change of the cursor's color from red to orange.",
    font='Open Sans',
    pos=(0.0, -0.2), height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
arc_instructions_main_4 = visual.ImageStim(
    win=win,
    name='arc_instructions_main_4', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-1.0)
fixation_cross_instructions_main_4 = visual.ShapeStim(
    win=win, name='fixation_cross_instructions_main_4', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=-2.0, interpolate=True)
cursor_instructions_main_4 = visual.ShapeStim(
    win=win, name='cursor_instructions_main_4',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=[cursor_pos[id_pos]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_border_col, fillColor=cursor_fill_col,
    opacity=show_cursor, depth=-3.0, interpolate=True)
instr_main_target_m_60_4 = visual.ShapeStim(
    win=win, name='instr_main_target_m_60_4',
    size=target_size, vertices='circle',
    ori=0.0, pos=[target_pos[id_pos]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=show_target, depth=-4.0, interpolate=True)
instr_main_target_m_60_go = visual.ShapeStim(
    win=win, name='instr_main_target_m_60_go',
    size=target_size, vertices='circle',
    ori=0.0, pos=[target_pos[id_pos]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col_go, fillColor=target_fill_col_go,
    opacity=show_go, depth=-5.0, interpolate=True)
cursor_adj_instructions = visual.ShapeStim(
    win=win, name='cursor_adj_instructions',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=[target_pos[id_pos]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_cursor_adj, depth=-6.0, interpolate=True)
cursor_instructions_main_6 = visual.ShapeStim(
    win=win, name='cursor_instructions_main_6',
    size=target_size, vertices='circle',
    ori=0.0, pos=[cursor_pos[id_pos]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=show_cursor_adj, depth=-7.0, interpolate=True)
key_resp_7 = keyboard.Keyboard()

# --- Initialize components for Routine "InstructionsMain_4" ---
instr_main_msg_3 = visual.TextStim(win=win, name='instr_main_msg_3',
    text='Unlike ACTION trials, you will not be making any hand movements during IMAGERY trials.\n\nAt the end of an IMAGERY trial the cursor will automatically return to the neutral position.',
    font='Open Sans',
    pos=(0.0, -0.2), height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
arc_instructions_main_7 = visual.ImageStim(
    win=win,
    name='arc_instructions_main_7', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-1.0)
fixation_cross_instructions_main_6 = visual.ShapeStim(
    win=win, name='fixation_cross_instructions_main_6', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=-2.0, interpolate=True)
cursor_instructions_main_5 = visual.ShapeStim(
    win=win, name='cursor_instructions_main_5',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=[cursor_pos[id_pos]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_border_col, fillColor=cursor_fill_col,
    opacity=show_cursor, depth=-3.0, interpolate=True)
instr_main_target_m_60_5 = visual.ShapeStim(
    win=win, name='instr_main_target_m_60_5',
    size=target_size, vertices='circle',
    ori=0.0, pos=[target_pos[id_pos]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=show_target, depth=-4.0, interpolate=True)
cursor_adj_instructions_2 = visual.ShapeStim(
    win=win, name='cursor_adj_instructions_2',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=[target_pos[id_pos]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_cursor_adj, depth=-5.0, interpolate=True)
cursor_instructions_main_7 = visual.ShapeStim(
    win=win, name='cursor_instructions_main_7',
    size=target_size, vertices='circle',
    ori=0.0, pos=[cursor_pos[id_pos]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=show_cursor_adj, depth=-6.0, interpolate=True)
key_resp_9 = keyboard.Keyboard()

# --- Initialize components for Routine "InstructionsMain_5" ---
instr_main_msg_4 = visual.TextStim(win=win, name='instr_main_msg_4',
    text='During a small proportion of randomly chosen IMAGERY trials you will be asked to evaluate the degree of the movement you have imagined.\n\nThis will be indicated be a change in the color of the arch path and the disappearance of the fixation cross. Then, you will be able to move a cursor on the arc by pressing any button.\n\nBy pressing the button repeatedly you will be able to choose between different positions. Once you have chosen the position that approximately matches your imagined movement, stop pressing the button for a few seconds. The next IMAGERY trial will soon start.',
    font='Open Sans',
    pos=(0.0, -0.2), height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
arc_instructions_main_6 = visual.ImageStim(
    win=win,
    name='arc_instructions_main_6', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-1.0)
arc_instructions_main_5 = visual.ImageStim(
    win=win,
    name='arc_instructions_main_5', 
    image=arc_catch_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-2.0)
fixation_cross_instructions_main_5 = visual.ShapeStim(
    win=win, name='fixation_cross_instructions_main_5', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=-3.0, interpolate=True)
instr_catch_target_0 = visual.ShapeStim(
    win=win, name='instr_catch_target_0',
    size=cursor_size, vertices='triangle',
    ori=targets_catch_degrees["0"], pos=[targets_catch_coords["0"]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_target, depth=-4.0, interpolate=True)
instr_catch_target_20 = visual.ShapeStim(
    win=win, name='instr_catch_target_20',
    size=cursor_size, vertices='triangle',
    ori=targets_catch_degrees["20"], pos=[targets_catch_coords["20"]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_target, depth=-5.0, interpolate=True)
instr_catch_target_30 = visual.ShapeStim(
    win=win, name='instr_catch_target_30',
    size=cursor_size, vertices='triangle',
    ori=targets_catch_degrees["30"], pos=[targets_catch_coords["30"]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_target, depth=-6.0, interpolate=True)
instr_catch_target_45 = visual.ShapeStim(
    win=win, name='instr_catch_target_45',
    size=cursor_size, vertices='triangle',
    ori=targets_catch_degrees["45"], pos=[targets_catch_coords["45"]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_target, depth=-7.0, interpolate=True)
instr_catch_target_60 = visual.ShapeStim(
    win=win, name='instr_catch_target_60',
    size=cursor_size, vertices='triangle',
    ori=targets_catch_degrees["60"], pos=[targets_catch_coords["60"]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_target, depth=-8.0, interpolate=True)
instr_catch_target_75 = visual.ShapeStim(
    win=win, name='instr_catch_target_75',
    size=cursor_size, vertices='triangle',
    ori=targets_catch_degrees["75"], pos=[targets_catch_coords["75"]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_target, depth=-9.0, interpolate=True)
instr_catch_target_m20 = visual.ShapeStim(
    win=win, name='instr_catch_target_m20',
    size=cursor_size, vertices='triangle',
    ori=targets_catch_degrees["-20"], pos=[targets_catch_coords["-20"]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_target, depth=-10.0, interpolate=True)
instr_catch_target_m30 = visual.ShapeStim(
    win=win, name='instr_catch_target_m30',
    size=cursor_size, vertices='triangle',
    ori=targets_catch_degrees["-30"], pos=[targets_catch_coords["-30"]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_target, depth=-11.0, interpolate=True)
instr_catch_target_m45 = visual.ShapeStim(
    win=win, name='instr_catch_target_m45',
    size=cursor_size, vertices='triangle',
    ori=targets_catch_degrees["-45"], pos=[targets_catch_coords["-45"]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_target, depth=-12.0, interpolate=True)
instr_catch_target_m60 = visual.ShapeStim(
    win=win, name='instr_catch_target_m60',
    size=cursor_size, vertices='triangle',
    ori=targets_catch_degrees["-60"], pos=[targets_catch_coords["-60"]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_target, depth=-13.0, interpolate=True)
instr_catch_target_m75 = visual.ShapeStim(
    win=win, name='instr_catch_target_m75',
    size=cursor_size, vertices='triangle',
    ori=targets_catch_degrees["-75"], pos=[targets_catch_coords["-75"]], anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=show_target, depth=-14.0, interpolate=True)
key_resp_10 = keyboard.Keyboard()

# --- Initialize components for Routine "ByeByeScreen" ---
bye_bye = visual.TextStim(win=win, name='bye_bye',
    text='The training session has ended.',
    font='Open Sans',
    pos=(0.0, 0.0), height=text_size, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp_8 = keyboard.Keyboard()

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.Clock()  # to track time remaining of each (possibly non-slip) routine 

# --- Prepare to start Routine "LoadData" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
# keep track of which components have finished
LoadDataComponents = []
for thisComponent in LoadDataComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "LoadData" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in LoadDataComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "LoadData" ---
for thisComponent in LoadDataComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "LoadData" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "WelcomeScreen" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp.keys = []
key_resp.rt = []
_key_resp_allKeys = []
# Run 'Begin Routine' code from hide_mouse
#-----#
# Hide mouse.
if debug_mode == True:
    win.mouseVisible = False
# keep track of which components have finished
WelcomeScreenComponents = [welcome_message, key_resp]
for thisComponent in WelcomeScreenComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "WelcomeScreen" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *welcome_message* updates
    if welcome_message.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        welcome_message.frameNStart = frameN  # exact frame index
        welcome_message.tStart = t  # local t and not account for scr refresh
        welcome_message.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(welcome_message, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'welcome_message.started')
        welcome_message.setAutoDraw(True)
    
    # *key_resp* updates
    waitOnFlip = False
    if key_resp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp.frameNStart = frameN  # exact frame index
        key_resp.tStart = t  # local t and not account for scr refresh
        key_resp.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp.started')
        key_resp.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp.status == STARTED and not waitOnFlip:
        theseKeys = key_resp.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
        _key_resp_allKeys.extend(theseKeys)
        if len(_key_resp_allKeys):
            key_resp.keys = _key_resp_allKeys[-1].name  # just the last key pressed
            key_resp.rt = _key_resp_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in WelcomeScreenComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "WelcomeScreen" ---
for thisComponent in WelcomeScreenComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp.keys in ['', [], None]:  # No response was made
    key_resp.keys = None
thisExp.addData('key_resp.keys',key_resp.keys)
if key_resp.keys != None:  # we had a response
    thisExp.addData('key_resp.rt', key_resp.rt)
thisExp.nextEntry()
# the Routine "WelcomeScreen" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "InstructionsInit" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_2.keys = []
key_resp_2.rt = []
_key_resp_2_allKeys = []
# Run 'Begin Routine' code from hide_mouse_2
#-----#
# Hide mouse.
if debug_mode == True:
    win.mouseVisible = False
# keep track of which components have finished
InstructionsInitComponents = [instr_init_msg_1, key_resp_2]
for thisComponent in InstructionsInitComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "InstructionsInit" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instr_init_msg_1* updates
    if instr_init_msg_1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instr_init_msg_1.frameNStart = frameN  # exact frame index
        instr_init_msg_1.tStart = t  # local t and not account for scr refresh
        instr_init_msg_1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_init_msg_1, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_init_msg_1.started')
        instr_init_msg_1.setAutoDraw(True)
    
    # *key_resp_2* updates
    waitOnFlip = False
    if key_resp_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_2.frameNStart = frameN  # exact frame index
        key_resp_2.tStart = t  # local t and not account for scr refresh
        key_resp_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_2, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_2.started')
        key_resp_2.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_2.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_2.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_2.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_2.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
        _key_resp_2_allKeys.extend(theseKeys)
        if len(_key_resp_2_allKeys):
            key_resp_2.keys = _key_resp_2_allKeys[-1].name  # just the last key pressed
            key_resp_2.rt = _key_resp_2_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in InstructionsInitComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "InstructionsInit" ---
for thisComponent in InstructionsInitComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_2.keys in ['', [], None]:  # No response was made
    key_resp_2.keys = None
thisExp.addData('key_resp_2.keys',key_resp_2.keys)
if key_resp_2.keys != None:  # we had a response
    thisExp.addData('key_resp_2.rt', key_resp_2.rt)
thisExp.nextEntry()
# the Routine "InstructionsInit" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "InstructionsInit_2" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_4.keys = []
key_resp_4.rt = []
_key_resp_4_allKeys = []
# Run 'Begin Routine' code from hide_mouse_3
#-----#
# Hide mouse.
if debug_mode == True:
    win.mouseVisible = False
# keep track of which components have finished
InstructionsInit_2Components = [instr_init_msg, instr_init_fix_cross_2, key_resp_4]
for thisComponent in InstructionsInit_2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "InstructionsInit_2" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instr_init_msg* updates
    if instr_init_msg.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instr_init_msg.frameNStart = frameN  # exact frame index
        instr_init_msg.tStart = t  # local t and not account for scr refresh
        instr_init_msg.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_init_msg, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_init_msg.started')
        instr_init_msg.setAutoDraw(True)
    
    # *instr_init_fix_cross_2* updates
    if instr_init_fix_cross_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instr_init_fix_cross_2.frameNStart = frameN  # exact frame index
        instr_init_fix_cross_2.tStart = t  # local t and not account for scr refresh
        instr_init_fix_cross_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_init_fix_cross_2, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_init_fix_cross_2.started')
        instr_init_fix_cross_2.setAutoDraw(True)
    
    # *key_resp_4* updates
    waitOnFlip = False
    if key_resp_4.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_4.frameNStart = frameN  # exact frame index
        key_resp_4.tStart = t  # local t and not account for scr refresh
        key_resp_4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_4, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_4.started')
        key_resp_4.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_4.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_4.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_4.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_4.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
        _key_resp_4_allKeys.extend(theseKeys)
        if len(_key_resp_4_allKeys):
            key_resp_4.keys = _key_resp_4_allKeys[-1].name  # just the last key pressed
            key_resp_4.rt = _key_resp_4_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in InstructionsInit_2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "InstructionsInit_2" ---
for thisComponent in InstructionsInit_2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_4.keys in ['', [], None]:  # No response was made
    key_resp_4.keys = None
thisExp.addData('key_resp_4.keys',key_resp_4.keys)
if key_resp_4.keys != None:  # we had a response
    thisExp.addData('key_resp_4.rt', key_resp_4.rt)
thisExp.nextEntry()
# the Routine "InstructionsInit_2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "InstructionsInit_3" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_3.keys = []
key_resp_3.rt = []
_key_resp_3_allKeys = []
# Run 'Begin Routine' code from hide_mouse_4
#-----#
# Hide mouse.
if debug_mode == True:
    win.mouseVisible = False
# keep track of which components have finished
InstructionsInit_3Components = [instr_init_msg_3, key_resp_3]
for thisComponent in InstructionsInit_3Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "InstructionsInit_3" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instr_init_msg_3* updates
    if instr_init_msg_3.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_init_msg_3.frameNStart = frameN  # exact frame index
        instr_init_msg_3.tStart = t  # local t and not account for scr refresh
        instr_init_msg_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_init_msg_3, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_init_msg_3.started')
        instr_init_msg_3.setAutoDraw(True)
    
    # *key_resp_3* updates
    waitOnFlip = False
    if key_resp_3.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_3.frameNStart = frameN  # exact frame index
        key_resp_3.tStart = t  # local t and not account for scr refresh
        key_resp_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_3, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_3.started')
        key_resp_3.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_3.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_3.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_3.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_3.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
        _key_resp_3_allKeys.extend(theseKeys)
        if len(_key_resp_3_allKeys):
            key_resp_3.keys = _key_resp_3_allKeys[-1].name  # just the last key pressed
            key_resp_3.rt = _key_resp_3_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in InstructionsInit_3Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "InstructionsInit_3" ---
for thisComponent in InstructionsInit_3Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_3.keys in ['', [], None]:  # No response was made
    key_resp_3.keys = None
thisExp.addData('key_resp_3.keys',key_resp_3.keys)
if key_resp_3.keys != None:  # we had a response
    thisExp.addData('key_resp_3.rt', key_resp_3.rt)
thisExp.nextEntry()
# the Routine "InstructionsInit_3" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "InstructionsMain" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_5.keys = []
key_resp_5.rt = []
_key_resp_5_allKeys = []
# Run 'Begin Routine' code from hide_mouse_5
#-----#
# Hide mouse.
if debug_mode == True:
    win.mouseVisible = False
# keep track of which components have finished
InstructionsMainComponents = [instr_main_msg_1, arc_instructions_main, fixation_cross_instructions_main, instr_main_target_p_30, instr_main_target_m_30, instr_main_target_p_60, instr_main_target_m_60, key_resp_5]
for thisComponent in InstructionsMainComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "InstructionsMain" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instr_main_msg_1* updates
    if instr_main_msg_1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_msg_1.frameNStart = frameN  # exact frame index
        instr_main_msg_1.tStart = t  # local t and not account for scr refresh
        instr_main_msg_1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_msg_1, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_msg_1.started')
        instr_main_msg_1.setAutoDraw(True)
    
    # *arc_instructions_main* updates
    if arc_instructions_main.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        arc_instructions_main.frameNStart = frameN  # exact frame index
        arc_instructions_main.tStart = t  # local t and not account for scr refresh
        arc_instructions_main.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(arc_instructions_main, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'arc_instructions_main.started')
        arc_instructions_main.setAutoDraw(True)
    
    # *fixation_cross_instructions_main* updates
    if fixation_cross_instructions_main.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        fixation_cross_instructions_main.frameNStart = frameN  # exact frame index
        fixation_cross_instructions_main.tStart = t  # local t and not account for scr refresh
        fixation_cross_instructions_main.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(fixation_cross_instructions_main, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'fixation_cross_instructions_main.started')
        fixation_cross_instructions_main.setAutoDraw(True)
    
    # *instr_main_target_p_30* updates
    if instr_main_target_p_30.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_target_p_30.frameNStart = frameN  # exact frame index
        instr_main_target_p_30.tStart = t  # local t and not account for scr refresh
        instr_main_target_p_30.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_target_p_30, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_target_p_30.started')
        instr_main_target_p_30.setAutoDraw(True)
    
    # *instr_main_target_m_30* updates
    if instr_main_target_m_30.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_target_m_30.frameNStart = frameN  # exact frame index
        instr_main_target_m_30.tStart = t  # local t and not account for scr refresh
        instr_main_target_m_30.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_target_m_30, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_target_m_30.started')
        instr_main_target_m_30.setAutoDraw(True)
    
    # *instr_main_target_p_60* updates
    if instr_main_target_p_60.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_target_p_60.frameNStart = frameN  # exact frame index
        instr_main_target_p_60.tStart = t  # local t and not account for scr refresh
        instr_main_target_p_60.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_target_p_60, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_target_p_60.started')
        instr_main_target_p_60.setAutoDraw(True)
    
    # *instr_main_target_m_60* updates
    if instr_main_target_m_60.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_target_m_60.frameNStart = frameN  # exact frame index
        instr_main_target_m_60.tStart = t  # local t and not account for scr refresh
        instr_main_target_m_60.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_target_m_60, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_target_m_60.started')
        instr_main_target_m_60.setAutoDraw(True)
    
    # *key_resp_5* updates
    waitOnFlip = False
    if key_resp_5.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_5.frameNStart = frameN  # exact frame index
        key_resp_5.tStart = t  # local t and not account for scr refresh
        key_resp_5.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_5, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_5.started')
        key_resp_5.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_5.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_5.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_5.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_5.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
        _key_resp_5_allKeys.extend(theseKeys)
        if len(_key_resp_5_allKeys):
            key_resp_5.keys = _key_resp_5_allKeys[-1].name  # just the last key pressed
            key_resp_5.rt = _key_resp_5_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in InstructionsMainComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "InstructionsMain" ---
for thisComponent in InstructionsMainComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_5.keys in ['', [], None]:  # No response was made
    key_resp_5.keys = None
thisExp.addData('key_resp_5.keys',key_resp_5.keys)
if key_resp_5.keys != None:  # we had a response
    thisExp.addData('key_resp_5.rt', key_resp_5.rt)
thisExp.nextEntry()
# the Routine "InstructionsMain" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "InstructionsMain_2" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_6.keys = []
key_resp_6.rt = []
_key_resp_6_allKeys = []
# Run 'Begin Routine' code from hide_mouse_6
#-----#
# Hide mouse.
if debug_mode == True:
    win.mouseVisible = False

#-----#
# Show cursor and target.
timer = core.Clock()
# keep track of which components have finished
InstructionsMain_2Components = [instr_main_msg, arc_instructions_main_3, fixation_cross_instructions_main_3, cursor_instructions_main, instr_main_target_m_60_3, inst_main_target_m_60_go, cursor_adj_go, key_resp_6]
for thisComponent in InstructionsMain_2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "InstructionsMain_2" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instr_main_msg* updates
    if instr_main_msg.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_msg.frameNStart = frameN  # exact frame index
        instr_main_msg.tStart = t  # local t and not account for scr refresh
        instr_main_msg.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_msg, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_msg.started')
        instr_main_msg.setAutoDraw(True)
    
    # *arc_instructions_main_3* updates
    if arc_instructions_main_3.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        arc_instructions_main_3.frameNStart = frameN  # exact frame index
        arc_instructions_main_3.tStart = t  # local t and not account for scr refresh
        arc_instructions_main_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(arc_instructions_main_3, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'arc_instructions_main_3.started')
        arc_instructions_main_3.setAutoDraw(True)
    
    # *fixation_cross_instructions_main_3* updates
    if fixation_cross_instructions_main_3.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        fixation_cross_instructions_main_3.frameNStart = frameN  # exact frame index
        fixation_cross_instructions_main_3.tStart = t  # local t and not account for scr refresh
        fixation_cross_instructions_main_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(fixation_cross_instructions_main_3, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'fixation_cross_instructions_main_3.started')
        fixation_cross_instructions_main_3.setAutoDraw(True)
    
    # *cursor_instructions_main* updates
    if cursor_instructions_main.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        cursor_instructions_main.frameNStart = frameN  # exact frame index
        cursor_instructions_main.tStart = t  # local t and not account for scr refresh
        cursor_instructions_main.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(cursor_instructions_main, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'cursor_instructions_main.started')
        cursor_instructions_main.setAutoDraw(True)
    
    # *instr_main_target_m_60_3* updates
    if instr_main_target_m_60_3.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_target_m_60_3.frameNStart = frameN  # exact frame index
        instr_main_target_m_60_3.tStart = t  # local t and not account for scr refresh
        instr_main_target_m_60_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_target_m_60_3, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_target_m_60_3.started')
        instr_main_target_m_60_3.setAutoDraw(True)
    
    # *inst_main_target_m_60_go* updates
    if inst_main_target_m_60_go.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        inst_main_target_m_60_go.frameNStart = frameN  # exact frame index
        inst_main_target_m_60_go.tStart = t  # local t and not account for scr refresh
        inst_main_target_m_60_go.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(inst_main_target_m_60_go, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'inst_main_target_m_60_go.started')
        inst_main_target_m_60_go.setAutoDraw(True)
    
    # *cursor_adj_go* updates
    if cursor_adj_go.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        cursor_adj_go.frameNStart = frameN  # exact frame index
        cursor_adj_go.tStart = t  # local t and not account for scr refresh
        cursor_adj_go.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(cursor_adj_go, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'cursor_adj_go.started')
        cursor_adj_go.setAutoDraw(True)
    
    # *key_resp_6* updates
    waitOnFlip = False
    if key_resp_6.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_6.frameNStart = frameN  # exact frame index
        key_resp_6.tStart = t  # local t and not account for scr refresh
        key_resp_6.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_6, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_6.started')
        key_resp_6.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_6.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_6.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_6.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_6.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
        _key_resp_6_allKeys.extend(theseKeys)
        if len(_key_resp_6_allKeys):
            key_resp_6.keys = _key_resp_6_allKeys[-1].name  # just the last key pressed
            key_resp_6.rt = _key_resp_6_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    # Run 'Each Frame' code from hide_mouse_6
    # Alternate between appearing cursor and target.
    tm = timer.getTime()
    
    if tm > 5:
        timer.reset(0.0)
    
    elif tm > 3.0 and\
        tm <= 5.0:
        cursor_instructions_main.setOpacity(0.0)
        instr_main_target_m_60_3.setOpacity(0.0)
        inst_main_target_m_60_go.setOpacity(1.0)
        cursor_adj_go.setOpacity(1.0)
    
    elif tm > 1.0 and\
        tm <= 3.0:
        cursor_instructions_main.setOpacity(1.0)
        instr_main_target_m_60_3.setOpacity(0.0)
        inst_main_target_m_60_go.setOpacity(1.0)
        cursor_adj_go.setOpacity(0.0)
    
    elif tm <= 1.0:
        cursor_instructions_main.setOpacity(1.0)
        instr_main_target_m_60_3.setOpacity(1.0)
        inst_main_target_m_60_go.setOpacity(0.0)
        cursor_adj_go.setOpacity(0.0)
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in InstructionsMain_2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "InstructionsMain_2" ---
for thisComponent in InstructionsMain_2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_6.keys in ['', [], None]:  # No response was made
    key_resp_6.keys = None
thisExp.addData('key_resp_6.keys',key_resp_6.keys)
if key_resp_6.keys != None:  # we had a response
    thisExp.addData('key_resp_6.rt', key_resp_6.rt)
thisExp.nextEntry()
# the Routine "InstructionsMain_2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "InstructionsMain_3" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_7.keys = []
key_resp_7.rt = []
_key_resp_7_allKeys = []
# Run 'Begin Routine' code from hide_mouse_7
#-----#
# Hide mouse.
if debug_mode == True:
    win.mouseVisible = False

#-----#
# Show cursor and target.
timer_2 = core.Clock()
# keep track of which components have finished
InstructionsMain_3Components = [instr_main_msg_2, arc_instructions_main_4, fixation_cross_instructions_main_4, cursor_instructions_main_4, instr_main_target_m_60_4, instr_main_target_m_60_go, cursor_adj_instructions, cursor_instructions_main_6, key_resp_7]
for thisComponent in InstructionsMain_3Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "InstructionsMain_3" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instr_main_msg_2* updates
    if instr_main_msg_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_msg_2.frameNStart = frameN  # exact frame index
        instr_main_msg_2.tStart = t  # local t and not account for scr refresh
        instr_main_msg_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_msg_2, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_msg_2.started')
        instr_main_msg_2.setAutoDraw(True)
    
    # *arc_instructions_main_4* updates
    if arc_instructions_main_4.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        arc_instructions_main_4.frameNStart = frameN  # exact frame index
        arc_instructions_main_4.tStart = t  # local t and not account for scr refresh
        arc_instructions_main_4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(arc_instructions_main_4, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'arc_instructions_main_4.started')
        arc_instructions_main_4.setAutoDraw(True)
    
    # *fixation_cross_instructions_main_4* updates
    if fixation_cross_instructions_main_4.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        fixation_cross_instructions_main_4.frameNStart = frameN  # exact frame index
        fixation_cross_instructions_main_4.tStart = t  # local t and not account for scr refresh
        fixation_cross_instructions_main_4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(fixation_cross_instructions_main_4, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'fixation_cross_instructions_main_4.started')
        fixation_cross_instructions_main_4.setAutoDraw(True)
    
    # *cursor_instructions_main_4* updates
    if cursor_instructions_main_4.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        cursor_instructions_main_4.frameNStart = frameN  # exact frame index
        cursor_instructions_main_4.tStart = t  # local t and not account for scr refresh
        cursor_instructions_main_4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(cursor_instructions_main_4, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'cursor_instructions_main_4.started')
        cursor_instructions_main_4.setAutoDraw(True)
    
    # *instr_main_target_m_60_4* updates
    if instr_main_target_m_60_4.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_target_m_60_4.frameNStart = frameN  # exact frame index
        instr_main_target_m_60_4.tStart = t  # local t and not account for scr refresh
        instr_main_target_m_60_4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_target_m_60_4, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_target_m_60_4.started')
        instr_main_target_m_60_4.setAutoDraw(True)
    
    # *instr_main_target_m_60_go* updates
    if instr_main_target_m_60_go.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_target_m_60_go.frameNStart = frameN  # exact frame index
        instr_main_target_m_60_go.tStart = t  # local t and not account for scr refresh
        instr_main_target_m_60_go.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_target_m_60_go, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_target_m_60_go.started')
        instr_main_target_m_60_go.setAutoDraw(True)
    
    # *cursor_adj_instructions* updates
    if cursor_adj_instructions.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        cursor_adj_instructions.frameNStart = frameN  # exact frame index
        cursor_adj_instructions.tStart = t  # local t and not account for scr refresh
        cursor_adj_instructions.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(cursor_adj_instructions, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'cursor_adj_instructions.started')
        cursor_adj_instructions.setAutoDraw(True)
    
    # *cursor_instructions_main_6* updates
    if cursor_instructions_main_6.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        cursor_instructions_main_6.frameNStart = frameN  # exact frame index
        cursor_instructions_main_6.tStart = t  # local t and not account for scr refresh
        cursor_instructions_main_6.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(cursor_instructions_main_6, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'cursor_instructions_main_6.started')
        cursor_instructions_main_6.setAutoDraw(True)
    
    # *key_resp_7* updates
    waitOnFlip = False
    if key_resp_7.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_7.frameNStart = frameN  # exact frame index
        key_resp_7.tStart = t  # local t and not account for scr refresh
        key_resp_7.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_7, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_7.started')
        key_resp_7.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_7.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_7.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_7.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_7.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
        _key_resp_7_allKeys.extend(theseKeys)
        if len(_key_resp_7_allKeys):
            key_resp_7.keys = _key_resp_7_allKeys[-1].name  # just the last key pressed
            key_resp_7.rt = _key_resp_7_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    # Run 'Each Frame' code from hide_mouse_7
    # Alternate between appearing cursor and target.
    tc = timer_2.getTime()
    
    if tc > 10.0:
    
        timer_2.reset(0.0)
    
        if id_pos == 0:
            id_pos = 1
        elif id_pos == 1:
            id_pos = 0
    
    elif tc > 9.0 and\
        tc <= 10.0:
        cursor_instructions_main_4.setOpacity(1.0)
        cursor_instructions_main_4.setPos(cursor_pos[id_pos])
    
        instr_main_target_m_60_4.setOpacity(0.0)
        instr_main_target_m_60_4.setPos(target_pos[id_pos])
    
        instr_main_target_m_60_go.setOpacity(0.0)
        instr_main_target_m_60_go.setPos(target_pos[id_pos])
    
        cursor_adj_instructions.setOpacity(0.0)
        cursor_adj_instructions.setPos(target_pos[id_pos])
    
        cursor_instructions_main_6.setOpacity(0.0)
        cursor_instructions_main_6.setPos(cursor_pos[id_pos])
    
    elif tc > 6.0 and\
        tc <= 9.0:
        cursor_instructions_main_4.setOpacity(0.0)
        cursor_instructions_main_4.setPos(cursor_pos[id_pos])
    
        instr_main_target_m_60_4.setOpacity(0.0)
        instr_main_target_m_60_4.setPos(target_pos[id_pos])
    
        instr_main_target_m_60_go.setOpacity(0.0)
        instr_main_target_m_60_go.setPos(target_pos[id_pos])
    
        cursor_adj_instructions.setOpacity(0.0)
        cursor_adj_instructions.setPos(target_pos[id_pos])
    
        cursor_instructions_main_6.setOpacity(1.0)
        cursor_instructions_main_6.setPos(cursor_pos[id_pos])
    
    elif tc > 5.0 and\
        tc <= 6.0:
        cursor_instructions_main_4.setOpacity(0.0)
        cursor_instructions_main_4.setPos(cursor_pos[id_pos])
    
        instr_main_target_m_60_4.setOpacity(0.0)
        instr_main_target_m_60_4.setPos(target_pos[id_pos])
    
        instr_main_target_m_60_go.setOpacity(0.0)
        instr_main_target_m_60_go.setPos(target_pos[id_pos])
    
        cursor_adj_instructions.setOpacity(1.0)
        cursor_adj_instructions.setPos(target_pos[id_pos])
        cursor_adj_instructions.setOri(target_degrees[id_pos])
    
        cursor_instructions_main_6.setOpacity(1.0)
        cursor_instructions_main_6.setPos(cursor_pos[id_pos])
    
    elif tc > 4.0 and\
        tc <= 5.0:
        cursor_instructions_main_4.setOpacity(0.0)
        cursor_instructions_main_4.setPos(cursor_pos[id_pos])
    
        instr_main_target_m_60_4.setOpacity(0.0)
        instr_main_target_m_60_4.setPos(target_pos[id_pos])
    
        instr_main_target_m_60_go.setOpacity(1.0)
        instr_main_target_m_60_go.setPos(target_pos[id_pos])
    
        cursor_adj_instructions.setOpacity(1.0)
        cursor_adj_instructions.setPos(target_pos[id_pos])
        cursor_adj_instructions.setOri(target_degrees[id_pos])
    
        cursor_instructions_main_6.setOpacity(0.0)
        cursor_instructions_main_6.setPos(cursor_pos[id_pos])
    
    elif tc > 3.0 and\
        tc <= 4.0:
        cursor_instructions_main_4.setOpacity(0.0)
        cursor_instructions_main_4.setPos(cursor_pos[id_pos])
    
        instr_main_target_m_60_4.setOpacity(0.0)
        instr_main_target_m_60_4.setPos(target_pos[id_pos])
    
        instr_main_target_m_60_go.setOpacity(1.0)
        instr_main_target_m_60_go.setPos(target_pos[id_pos])
    
        cursor_adj_instructions.setOpacity(0.0)
        cursor_adj_instructions.setPos(target_pos[id_pos])
    
        cursor_instructions_main_6.setOpacity(0.0)
        cursor_instructions_main_6.setPos(cursor_pos[id_pos])
    
    elif tc > 1.0 and\
        tc <= 3.0:
        cursor_instructions_main_4.setOpacity(1.0)
        cursor_instructions_main_4.setPos(cursor_pos[id_pos])
    
        instr_main_target_m_60_4.setOpacity(0.0)
        instr_main_target_m_60_4.setPos(target_pos[id_pos])
    
        instr_main_target_m_60_go.setOpacity(1.0)
        instr_main_target_m_60_go.setPos(target_pos[id_pos])
    
        cursor_adj_instructions.setOpacity(0.0)
        cursor_adj_instructions.setPos(target_pos[id_pos])
    
        cursor_instructions_main_6.setOpacity(0.0)
        cursor_instructions_main_6.setPos(cursor_pos[id_pos])
    
    elif tc <= 1.0:
        cursor_instructions_main_4.setOpacity(1.0)
        cursor_instructions_main_4.setPos(cursor_pos[id_pos])
    
        instr_main_target_m_60_4.setOpacity(1.0)
        instr_main_target_m_60_4.setPos(target_pos[id_pos])
    
        instr_main_target_m_60_go.setOpacity(0.0)
        instr_main_target_m_60_go.setPos(target_pos[id_pos])
    
        cursor_adj_instructions.setOpacity(0.0)
        cursor_adj_instructions.setPos(target_pos[id_pos])
    
        cursor_instructions_main_6.setOpacity(0.0)
        cursor_instructions_main_6.setPos(cursor_pos[id_pos])
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in InstructionsMain_3Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "InstructionsMain_3" ---
for thisComponent in InstructionsMain_3Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_7.keys in ['', [], None]:  # No response was made
    key_resp_7.keys = None
thisExp.addData('key_resp_7.keys',key_resp_7.keys)
if key_resp_7.keys != None:  # we had a response
    thisExp.addData('key_resp_7.rt', key_resp_7.rt)
thisExp.nextEntry()
# the Routine "InstructionsMain_3" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "InstructionsMain_4" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_9.keys = []
key_resp_9.rt = []
_key_resp_9_allKeys = []
# Run 'Begin Routine' code from hide_mouse_8
#-----#
# Hide mouse.
if debug_mode == True:
    win.mouseVisible = False

#-----#
# Show cursor and target.
timer_4 = core.Clock()
# keep track of which components have finished
InstructionsMain_4Components = [instr_main_msg_3, arc_instructions_main_7, fixation_cross_instructions_main_6, cursor_instructions_main_5, instr_main_target_m_60_5, cursor_adj_instructions_2, cursor_instructions_main_7, key_resp_9]
for thisComponent in InstructionsMain_4Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "InstructionsMain_4" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instr_main_msg_3* updates
    if instr_main_msg_3.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_msg_3.frameNStart = frameN  # exact frame index
        instr_main_msg_3.tStart = t  # local t and not account for scr refresh
        instr_main_msg_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_msg_3, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_msg_3.started')
        instr_main_msg_3.setAutoDraw(True)
    
    # *arc_instructions_main_7* updates
    if arc_instructions_main_7.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        arc_instructions_main_7.frameNStart = frameN  # exact frame index
        arc_instructions_main_7.tStart = t  # local t and not account for scr refresh
        arc_instructions_main_7.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(arc_instructions_main_7, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'arc_instructions_main_7.started')
        arc_instructions_main_7.setAutoDraw(True)
    
    # *fixation_cross_instructions_main_6* updates
    if fixation_cross_instructions_main_6.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        fixation_cross_instructions_main_6.frameNStart = frameN  # exact frame index
        fixation_cross_instructions_main_6.tStart = t  # local t and not account for scr refresh
        fixation_cross_instructions_main_6.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(fixation_cross_instructions_main_6, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'fixation_cross_instructions_main_6.started')
        fixation_cross_instructions_main_6.setAutoDraw(True)
    
    # *cursor_instructions_main_5* updates
    if cursor_instructions_main_5.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        cursor_instructions_main_5.frameNStart = frameN  # exact frame index
        cursor_instructions_main_5.tStart = t  # local t and not account for scr refresh
        cursor_instructions_main_5.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(cursor_instructions_main_5, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'cursor_instructions_main_5.started')
        cursor_instructions_main_5.setAutoDraw(True)
    
    # *instr_main_target_m_60_5* updates
    if instr_main_target_m_60_5.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_target_m_60_5.frameNStart = frameN  # exact frame index
        instr_main_target_m_60_5.tStart = t  # local t and not account for scr refresh
        instr_main_target_m_60_5.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_target_m_60_5, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_target_m_60_5.started')
        instr_main_target_m_60_5.setAutoDraw(True)
    
    # *cursor_adj_instructions_2* updates
    if cursor_adj_instructions_2.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        cursor_adj_instructions_2.frameNStart = frameN  # exact frame index
        cursor_adj_instructions_2.tStart = t  # local t and not account for scr refresh
        cursor_adj_instructions_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(cursor_adj_instructions_2, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'cursor_adj_instructions_2.started')
        cursor_adj_instructions_2.setAutoDraw(True)
    
    # *cursor_instructions_main_7* updates
    if cursor_instructions_main_7.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        cursor_instructions_main_7.frameNStart = frameN  # exact frame index
        cursor_instructions_main_7.tStart = t  # local t and not account for scr refresh
        cursor_instructions_main_7.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(cursor_instructions_main_7, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'cursor_instructions_main_7.started')
        cursor_instructions_main_7.setAutoDraw(True)
    
    # *key_resp_9* updates
    waitOnFlip = False
    if key_resp_9.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_9.frameNStart = frameN  # exact frame index
        key_resp_9.tStart = t  # local t and not account for scr refresh
        key_resp_9.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_9, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_9.started')
        key_resp_9.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_9.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_9.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_9.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_9.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
        _key_resp_9_allKeys.extend(theseKeys)
        if len(_key_resp_9_allKeys):
            key_resp_9.keys = _key_resp_9_allKeys[-1].name  # just the last key pressed
            key_resp_9.rt = _key_resp_9_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    # Run 'Each Frame' code from hide_mouse_8
    # Alternate between appearing cursor and target.
    tc = timer_4.getTime()
    
    if tc > 7.0:
    
        timer_4.reset(0.0)
    
        if id_pos == 0:
            id_pos = 1
        elif id_pos == 1:
            id_pos = 0
    
    elif tc > 5.0 and\
        tc <= 7.0:
        cursor_instructions_main_5.setOpacity(1.0)
        cursor_instructions_main_5.setPos(cursor_pos[id_pos])
    
        #instr_main_target_m_60_5.setOpacity(0.0)
        #instr_main_target_m_60_5.setPos(target_pos[id_pos])
        instr_main_target_m_60_5.setOpacity(0.0)
        instr_main_target_m_60_5.setPos(target_pos[id_pos])
    
        #instr_main_target_m_60_go.setOpacity(1.0)
        #instr_main_target_m_60_go.setPos(target_pos[id_pos])
    
        cursor_adj_instructions_2.setOpacity(0.0)
        cursor_adj_instructions_2.setPos(target_pos[id_pos])
    
        cursor_instructions_main_7.setOpacity(0.0)
        cursor_instructions_main_7.setPos(cursor_pos[id_pos])
    
    elif tc > 4.0 and\
        tc <= 5.0:
        cursor_instructions_main_5.setOpacity(0.0)
        cursor_instructions_main_5.setPos(cursor_pos[id_pos])
    
        #instr_main_target_m_60_5.setOpacity(0.0)
        #instr_main_target_m_60_5.setPos(target_pos[id_pos])
        instr_main_target_m_60_5.setOpacity(0.0)
        instr_main_target_m_60_5.setPos(target_pos[id_pos])
    
        #instr_main_target_m_60_go.setOpacity(1.0)
        #instr_main_target_m_60_go.setPos(target_pos[id_pos])
    
        cursor_adj_instructions_2.setOpacity(1.0)
        cursor_adj_instructions_2.setPos(target_pos[id_pos])
    
        cursor_instructions_main_7.setOpacity(0.0)
        cursor_instructions_main_7.setPos(cursor_pos[id_pos])
    
    elif tc > 3.0 and\
        tc <= 4.0:
        cursor_instructions_main_5.setOpacity(0.0)
        cursor_instructions_main_5.setPos(cursor_pos[id_pos])
    
        #instr_main_target_m_60_5.setOpacity(0.0)
        #instr_main_target_m_60_5.setPos(target_pos[id_pos])
        instr_main_target_m_60_5.setOpacity(1.0)
        instr_main_target_m_60_5.setPos(target_pos[id_pos])
    
        #instr_main_target_m_60_go.setOpacity(1.0)
        #instr_main_target_m_60_go.setPos(target_pos[id_pos])
    
        cursor_adj_instructions_2.setOpacity(1.0)
        cursor_adj_instructions_2.setPos(target_pos[id_pos])
        cursor_adj_instructions_2.setOri(target_degrees[id_pos])
    
        cursor_instructions_main_7.setOpacity(0.0)
        cursor_instructions_main_7.setPos(cursor_pos[id_pos])
    
    elif tc > 1.0 and\
        tc <= 3.0:
        cursor_instructions_main_5.setOpacity(0.0)
        cursor_instructions_main_5.setPos(cursor_pos[id_pos])
    
        #instr_main_target_m_60_5.setOpacity(0.0)
        #instr_main_target_m_60_5.setPos(target_pos[id_pos])
        instr_main_target_m_60_5.setOpacity(1.0)
        instr_main_target_m_60_5.setPos(target_pos[id_pos])
    
        #instr_main_target_m_60_go.setOpacity(1.0)
        #instr_main_target_m_60_go.setPos(target_pos[id_pos])
    
        cursor_adj_instructions_2.setOpacity(0.0)
        cursor_adj_instructions_2.setPos(target_pos[id_pos])
    
        cursor_instructions_main_7.setOpacity(0.0)
        cursor_instructions_main_7.setPos(cursor_pos[id_pos])
    
    elif tc <= 1.0:
        cursor_instructions_main_5.setOpacity(1.0)
        cursor_instructions_main_5.setPos(cursor_pos[id_pos])
    
        instr_main_target_m_60_5.setOpacity(1.0)
        instr_main_target_m_60_5.setPos(target_pos[id_pos])
    
        #instr_main_target_m_60_go.setOpacity(0.0)
        #instr_main_target_m_60_go.setPos(target_pos[id_pos])
    
        cursor_adj_instructions_2.setOpacity(0.0)
        cursor_adj_instructions_2.setPos(target_pos[id_pos])
    
        cursor_instructions_main_7.setOpacity(0.0)
        cursor_instructions_main_7.setPos(cursor_pos[id_pos])
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in InstructionsMain_4Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "InstructionsMain_4" ---
for thisComponent in InstructionsMain_4Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_9.keys in ['', [], None]:  # No response was made
    key_resp_9.keys = None
thisExp.addData('key_resp_9.keys',key_resp_9.keys)
if key_resp_9.keys != None:  # we had a response
    thisExp.addData('key_resp_9.rt', key_resp_9.rt)
thisExp.nextEntry()
# the Routine "InstructionsMain_4" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "InstructionsMain_5" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_10.keys = []
key_resp_10.rt = []
_key_resp_10_allKeys = []
# Run 'Begin Routine' code from hide_mouse_9
#-----#
# Hide mouse.
if debug_mode == True:
    win.mouseVisible = False

#-----#
# Show cursor and target.
timer_3 = core.Clock()
# keep track of which components have finished
InstructionsMain_5Components = [instr_main_msg_4, arc_instructions_main_6, arc_instructions_main_5, fixation_cross_instructions_main_5, instr_catch_target_0, instr_catch_target_20, instr_catch_target_30, instr_catch_target_45, instr_catch_target_60, instr_catch_target_75, instr_catch_target_m20, instr_catch_target_m30, instr_catch_target_m45, instr_catch_target_m60, instr_catch_target_m75, key_resp_10]
for thisComponent in InstructionsMain_5Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "InstructionsMain_5" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instr_main_msg_4* updates
    if instr_main_msg_4.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instr_main_msg_4.frameNStart = frameN  # exact frame index
        instr_main_msg_4.tStart = t  # local t and not account for scr refresh
        instr_main_msg_4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_main_msg_4, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_main_msg_4.started')
        instr_main_msg_4.setAutoDraw(True)
    
    # *arc_instructions_main_6* updates
    if arc_instructions_main_6.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        arc_instructions_main_6.frameNStart = frameN  # exact frame index
        arc_instructions_main_6.tStart = t  # local t and not account for scr refresh
        arc_instructions_main_6.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(arc_instructions_main_6, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'arc_instructions_main_6.started')
        arc_instructions_main_6.setAutoDraw(True)
    
    # *arc_instructions_main_5* updates
    if arc_instructions_main_5.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        arc_instructions_main_5.frameNStart = frameN  # exact frame index
        arc_instructions_main_5.tStart = t  # local t and not account for scr refresh
        arc_instructions_main_5.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(arc_instructions_main_5, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'arc_instructions_main_5.started')
        arc_instructions_main_5.setAutoDraw(True)
    
    # *fixation_cross_instructions_main_5* updates
    if fixation_cross_instructions_main_5.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        fixation_cross_instructions_main_5.frameNStart = frameN  # exact frame index
        fixation_cross_instructions_main_5.tStart = t  # local t and not account for scr refresh
        fixation_cross_instructions_main_5.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(fixation_cross_instructions_main_5, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'fixation_cross_instructions_main_5.started')
        fixation_cross_instructions_main_5.setAutoDraw(True)
    
    # *instr_catch_target_0* updates
    if instr_catch_target_0.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_catch_target_0.frameNStart = frameN  # exact frame index
        instr_catch_target_0.tStart = t  # local t and not account for scr refresh
        instr_catch_target_0.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_catch_target_0, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_catch_target_0.started')
        instr_catch_target_0.setAutoDraw(True)
    
    # *instr_catch_target_20* updates
    if instr_catch_target_20.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_catch_target_20.frameNStart = frameN  # exact frame index
        instr_catch_target_20.tStart = t  # local t and not account for scr refresh
        instr_catch_target_20.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_catch_target_20, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_catch_target_20.started')
        instr_catch_target_20.setAutoDraw(True)
    
    # *instr_catch_target_30* updates
    if instr_catch_target_30.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_catch_target_30.frameNStart = frameN  # exact frame index
        instr_catch_target_30.tStart = t  # local t and not account for scr refresh
        instr_catch_target_30.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_catch_target_30, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_catch_target_30.started')
        instr_catch_target_30.setAutoDraw(True)
    
    # *instr_catch_target_45* updates
    if instr_catch_target_45.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_catch_target_45.frameNStart = frameN  # exact frame index
        instr_catch_target_45.tStart = t  # local t and not account for scr refresh
        instr_catch_target_45.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_catch_target_45, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_catch_target_45.started')
        instr_catch_target_45.setAutoDraw(True)
    
    # *instr_catch_target_60* updates
    if instr_catch_target_60.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_catch_target_60.frameNStart = frameN  # exact frame index
        instr_catch_target_60.tStart = t  # local t and not account for scr refresh
        instr_catch_target_60.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_catch_target_60, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_catch_target_60.started')
        instr_catch_target_60.setAutoDraw(True)
    
    # *instr_catch_target_75* updates
    if instr_catch_target_75.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_catch_target_75.frameNStart = frameN  # exact frame index
        instr_catch_target_75.tStart = t  # local t and not account for scr refresh
        instr_catch_target_75.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_catch_target_75, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_catch_target_75.started')
        instr_catch_target_75.setAutoDraw(True)
    
    # *instr_catch_target_m20* updates
    if instr_catch_target_m20.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_catch_target_m20.frameNStart = frameN  # exact frame index
        instr_catch_target_m20.tStart = t  # local t and not account for scr refresh
        instr_catch_target_m20.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_catch_target_m20, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_catch_target_m20.started')
        instr_catch_target_m20.setAutoDraw(True)
    
    # *instr_catch_target_m30* updates
    if instr_catch_target_m30.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_catch_target_m30.frameNStart = frameN  # exact frame index
        instr_catch_target_m30.tStart = t  # local t and not account for scr refresh
        instr_catch_target_m30.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_catch_target_m30, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_catch_target_m30.started')
        instr_catch_target_m30.setAutoDraw(True)
    
    # *instr_catch_target_m45* updates
    if instr_catch_target_m45.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_catch_target_m45.frameNStart = frameN  # exact frame index
        instr_catch_target_m45.tStart = t  # local t and not account for scr refresh
        instr_catch_target_m45.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_catch_target_m45, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_catch_target_m45.started')
        instr_catch_target_m45.setAutoDraw(True)
    
    # *instr_catch_target_m60* updates
    if instr_catch_target_m60.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_catch_target_m60.frameNStart = frameN  # exact frame index
        instr_catch_target_m60.tStart = t  # local t and not account for scr refresh
        instr_catch_target_m60.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_catch_target_m60, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_catch_target_m60.started')
        instr_catch_target_m60.setAutoDraw(True)
    
    # *instr_catch_target_m75* updates
    if instr_catch_target_m75.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        instr_catch_target_m75.frameNStart = frameN  # exact frame index
        instr_catch_target_m75.tStart = t  # local t and not account for scr refresh
        instr_catch_target_m75.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instr_catch_target_m75, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'instr_catch_target_m75.started')
        instr_catch_target_m75.setAutoDraw(True)
    
    # *key_resp_10* updates
    waitOnFlip = False
    if key_resp_10.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_10.frameNStart = frameN  # exact frame index
        key_resp_10.tStart = t  # local t and not account for scr refresh
        key_resp_10.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_10, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_10.started')
        key_resp_10.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_10.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_10.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_10.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_10.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
        _key_resp_10_allKeys.extend(theseKeys)
        if len(_key_resp_10_allKeys):
            key_resp_10.keys = _key_resp_10_allKeys[-1].name  # just the last key pressed
            key_resp_10.rt = _key_resp_10_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    # Run 'Each Frame' code from hide_mouse_9
    # Alternate between appearing cursor and target.
    tc = timer_3.getTime()
    
    if tc > 14.0:
        instr_catch_target_0.setOpacity(0.0)
        instr_catch_target_20.setOpacity(0.0)
        instr_catch_target_30.setOpacity(0.0)
        instr_catch_target_45.setOpacity(0.0)
        instr_catch_target_60.setOpacity(0.0)
        instr_catch_target_75.setOpacity(0.0)
        instr_catch_target_m20.setOpacity(0.0)
        instr_catch_target_m30.setOpacity(0.0)
        instr_catch_target_m45.setOpacity(0.0)
        instr_catch_target_m60.setOpacity(0.0)
        instr_catch_target_m75.setOpacity(0.0)
    
        timer_3.reset(0.0)
    
    elif tc > 11.0 and\
        tc <= 14.0:
        instr_catch_target_0.setOpacity(0.0)
        instr_catch_target_20.setOpacity(0.0)
        instr_catch_target_30.setOpacity(0.0)
        instr_catch_target_45.setOpacity(0.0)
        instr_catch_target_60.setOpacity(0.0)
        instr_catch_target_75.setOpacity(0.0)
        instr_catch_target_m20.setOpacity(1.0)
        instr_catch_target_m30.setOpacity(0.0)
        instr_catch_target_m45.setOpacity(0.0)
        instr_catch_target_m60.setOpacity(0.0)
        instr_catch_target_m75.setOpacity(0.0)
    
    elif tc > 10.0 and\
        tc <= 11.0:
        instr_catch_target_0.setOpacity(0.0)
        instr_catch_target_20.setOpacity(0.0)
        instr_catch_target_30.setOpacity(0.0)
        instr_catch_target_45.setOpacity(0.0)
        instr_catch_target_60.setOpacity(0.0)
        instr_catch_target_75.setOpacity(0.0)
        instr_catch_target_m20.setOpacity(0.0)
        instr_catch_target_m30.setOpacity(1.0)
        instr_catch_target_m45.setOpacity(0.0)
        instr_catch_target_m60.setOpacity(0.0)
        instr_catch_target_m75.setOpacity(0.0)
    
    elif tc > 9.0 and\
        tc <= 10.0:
        instr_catch_target_0.setOpacity(0.0)
        instr_catch_target_20.setOpacity(0.0)
        instr_catch_target_30.setOpacity(0.0)
        instr_catch_target_45.setOpacity(0.0)
        instr_catch_target_60.setOpacity(0.0)
        instr_catch_target_75.setOpacity(0.0)
        instr_catch_target_m20.setOpacity(0.0)
        instr_catch_target_m30.setOpacity(0.0)
        instr_catch_target_m45.setOpacity(1.0)
        instr_catch_target_m60.setOpacity(0.0)
        instr_catch_target_m75.setOpacity(0.0)
    
    elif tc > 8.0 and\
        tc <= 9.0:
        instr_catch_target_0.setOpacity(0.0)
        instr_catch_target_20.setOpacity(0.0)
        instr_catch_target_30.setOpacity(0.0)
        instr_catch_target_45.setOpacity(0.0)
        instr_catch_target_60.setOpacity(0.0)
        instr_catch_target_75.setOpacity(0.0)
        instr_catch_target_m20.setOpacity(0.0)
        instr_catch_target_m30.setOpacity(0.0)
        instr_catch_target_m45.setOpacity(0.0)
        instr_catch_target_m60.setOpacity(1.0)
        instr_catch_target_m75.setOpacity(0.0)
    
    elif tc > 7.0 and\
        tc <= 8.0:
        instr_catch_target_0.setOpacity(0.0)
        instr_catch_target_20.setOpacity(0.0)
        instr_catch_target_30.setOpacity(0.0)
        instr_catch_target_45.setOpacity(0.0)
        instr_catch_target_60.setOpacity(0.0)
        instr_catch_target_75.setOpacity(0.0)
        instr_catch_target_m20.setOpacity(0.0)
        instr_catch_target_m30.setOpacity(0.0)
        instr_catch_target_m45.setOpacity(0.0)
        instr_catch_target_m60.setOpacity(0.0)
        instr_catch_target_m75.setOpacity(1.0)
    
    elif tc > 6.0 and\
        tc <= 7.0:
        instr_catch_target_0.setOpacity(0.0)
        instr_catch_target_20.setOpacity(0.0)
        instr_catch_target_30.setOpacity(0.0)
        instr_catch_target_45.setOpacity(0.0)
        instr_catch_target_60.setOpacity(0.0)
        instr_catch_target_75.setOpacity(1.0)
        instr_catch_target_m20.setOpacity(0.0)
        instr_catch_target_m30.setOpacity(0.0)
        instr_catch_target_m45.setOpacity(0.0)
        instr_catch_target_m60.setOpacity(0.0)
        instr_catch_target_m75.setOpacity(0.0)
    
    elif tc > 5.0 and\
        tc <= 6.0:
        instr_catch_target_0.setOpacity(0.0)
        instr_catch_target_20.setOpacity(0.0)
        instr_catch_target_30.setOpacity(0.0)
        instr_catch_target_45.setOpacity(0.0)
        instr_catch_target_60.setOpacity(1.0)
        instr_catch_target_75.setOpacity(0.0)
        instr_catch_target_m20.setOpacity(0.0)
        instr_catch_target_m30.setOpacity(0.0)
        instr_catch_target_m45.setOpacity(0.0)
        instr_catch_target_m60.setOpacity(0.0)
        instr_catch_target_m75.setOpacity(0.0)
    
    elif tc > 4.0 and\
        tc <= 5.0:
        instr_catch_target_0.setOpacity(0.0)
        instr_catch_target_20.setOpacity(0.0)
        instr_catch_target_30.setOpacity(0.0)
        instr_catch_target_45.setOpacity(1.0)
        instr_catch_target_60.setOpacity(0.0)
        instr_catch_target_75.setOpacity(0.0)
        instr_catch_target_m20.setOpacity(0.0)
        instr_catch_target_m30.setOpacity(0.0)
        instr_catch_target_m45.setOpacity(0.0)
        instr_catch_target_m60.setOpacity(0.0)
        instr_catch_target_m75.setOpacity(0.0)
    
    elif tc > 3.0 and\
        tc <= 4.0:
        instr_catch_target_0.setOpacity(0.0)
        instr_catch_target_20.setOpacity(0.0)
        instr_catch_target_30.setOpacity(1.0)
        instr_catch_target_45.setOpacity(0.0)
        instr_catch_target_60.setOpacity(0.0)
        instr_catch_target_75.setOpacity(0.0)
        instr_catch_target_m20.setOpacity(0.0)
        instr_catch_target_m30.setOpacity(0.0)
        instr_catch_target_m45.setOpacity(0.0)
        instr_catch_target_m60.setOpacity(0.0)
        instr_catch_target_m75.setOpacity(0.0)
    
    elif tc > 2.0 and\
        tc <= 3.0:
        instr_catch_target_0.setOpacity(0.0)
        instr_catch_target_20.setOpacity(1.0)
        instr_catch_target_30.setOpacity(0.0)
        instr_catch_target_45.setOpacity(0.0)
        instr_catch_target_60.setOpacity(0.0)
        instr_catch_target_75.setOpacity(0.0)
        instr_catch_target_m20.setOpacity(0.0)
        instr_catch_target_m30.setOpacity(0.0)
        instr_catch_target_m45.setOpacity(0.0)
        instr_catch_target_m60.setOpacity(0.0)
        instr_catch_target_m75.setOpacity(0.0)
    
    elif tc > 1.0 and\
        tc <= 2.0:
        arc_instructions_main_6.setOpacity(0.0)
        arc_instructions_main_5.setOpacity(1.0)
        fixation_cross_instructions_main_5.setOpacity(0.0)
        
        instr_catch_target_0.setOpacity(1.0)
        instr_catch_target_20.setOpacity(0.0)
        instr_catch_target_30.setOpacity(0.0)
        instr_catch_target_45.setOpacity(0.0)
        instr_catch_target_60.setOpacity(0.0)
        instr_catch_target_75.setOpacity(0.0)
        instr_catch_target_m20.setOpacity(0.0)
        instr_catch_target_m30.setOpacity(0.0)
        instr_catch_target_m45.setOpacity(0.0)
        instr_catch_target_m60.setOpacity(0.0)
        instr_catch_target_m75.setOpacity(0.0)
    
    elif tc <= 1.0:
        arc_instructions_main_6.setOpacity(1.0)
        arc_instructions_main_5.setOpacity(0.0)
        fixation_cross_instructions_main_5.setOpacity(1.0)
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in InstructionsMain_5Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "InstructionsMain_5" ---
for thisComponent in InstructionsMain_5Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_10.keys in ['', [], None]:  # No response was made
    key_resp_10.keys = None
thisExp.addData('key_resp_10.keys',key_resp_10.keys)
if key_resp_10.keys != None:  # we had a response
    thisExp.addData('key_resp_10.rt', key_resp_10.rt)
thisExp.nextEntry()
# the Routine "InstructionsMain_5" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "ByeByeScreen" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_8.keys = []
key_resp_8.rt = []
_key_resp_8_allKeys = []
# keep track of which components have finished
ByeByeScreenComponents = [bye_bye, key_resp_8]
for thisComponent in ByeByeScreenComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "ByeByeScreen" ---
while continueRoutine and routineTimer.getTime() < 15.0:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *bye_bye* updates
    if bye_bye.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        bye_bye.frameNStart = frameN  # exact frame index
        bye_bye.tStart = t  # local t and not account for scr refresh
        bye_bye.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(bye_bye, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'bye_bye.started')
        bye_bye.setAutoDraw(True)
    if bye_bye.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > bye_bye.tStartRefresh + 15.0-frameTolerance:
            # keep track of stop time/frame for later
            bye_bye.tStop = t  # not accounting for scr refresh
            bye_bye.frameNStop = frameN  # exact frame index
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'bye_bye.stopped')
            bye_bye.setAutoDraw(False)
    
    # *key_resp_8* updates
    waitOnFlip = False
    if key_resp_8.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_8.frameNStart = frameN  # exact frame index
        key_resp_8.tStart = t  # local t and not account for scr refresh
        key_resp_8.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_8, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_8.started')
        key_resp_8.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_8.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_8.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_8.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > key_resp_8.tStartRefresh + 15.0-frameTolerance:
            # keep track of stop time/frame for later
            key_resp_8.tStop = t  # not accounting for scr refresh
            key_resp_8.frameNStop = frameN  # exact frame index
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'key_resp_8.stopped')
            key_resp_8.status = FINISHED
    if key_resp_8.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_8.getKeys(keyList=['r', 'g', 'b', 'y', 'm', 'space'], waitRelease=False)
        _key_resp_8_allKeys.extend(theseKeys)
        if len(_key_resp_8_allKeys):
            key_resp_8.keys = _key_resp_8_allKeys[-1].name  # just the last key pressed
            key_resp_8.rt = _key_resp_8_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in ByeByeScreenComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "ByeByeScreen" ---
for thisComponent in ByeByeScreenComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_8.keys in ['', [], None]:  # No response was made
    key_resp_8.keys = None
thisExp.addData('key_resp_8.keys',key_resp_8.keys)
if key_resp_8.keys != None:  # we had a response
    thisExp.addData('key_resp_8.rt', key_resp_8.rt)
thisExp.nextEntry()
# using non-slip timing so subtract the expected duration of this Routine (unless ended on request)
if routineForceEnded:
    routineTimer.reset()
else:
    routineTimer.addTime(-15.000000)

# --- End experiment ---
# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
if eyetracker:
    eyetracker.setConnectionState(False)
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
