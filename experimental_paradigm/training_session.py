#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2022.2.5),
    on Δευ 21 Οκτ 2024 10:42:24 πμ 
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

# --- Import packages ---
from psychopy import locale_setup
from psychopy import prefs
from psychopy import sound, gui, visual, core, data, event, logging, clock, colors, layout, parallel
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle, choice as randchoice
import os  # handy system and path functions
import sys  # to get file system encoding

import psychopy.iohub as io
from psychopy.hardware import keyboard

# Run 'Before Experiment' code from load_data
import pickle
import time

from os import makedirs
from os.path import exists, join
from psychopy import prefs
from scipy.stats import linregress

#-----#
debug_mode = True

#-----#
show_mouse = False

#-----#
# Does the target appear at the same time as the cursor?
cursor_target_sync = True  # True, False

#-----#
# During ADJUST trials, should the cursors or
# the actual hand be brought back to the
# neutral position?
bring_to_neutral_pos = "hand"  # "cursor", "hand"

# Is the cursor being reset to the neutral
# position between trials?
mode = "zero"   # "zero", "continue"

#-----#
# Is this session gamified, so as to keep the
# subjects motivated?
gamification = False    # True, False

#-----#
# Which units are being used?
units = prefs.general["units"]
if units != "height":
    raise ValueError("This epxeriment has been designed only to run using 'height' as units.")

#-----#
# Print all exoerimental design choices.
print("Are debugging messages enabled? {}".format(debug_mode))
print("\n")
print("Trial mode (positioning of cursor at trial end): {}".format(mode))
print("Do ACTION cursor and target appear synchronously? {}".format(cursor_target_sync))
print("Do ADJUST trials ask for 'cursor' or 'hand' re-positioning? {}".format(bring_to_neutral_pos))
print("Is a score metric presented to the participants at the end of each block? {}".format(gamification))
print("\n")

#-----#
# Handy function to compute on-screen (x,y)
# coordinates based on degrees.
def deg_to_xy(
    degrees,
    offset=90,
    radious=0.375
):

    # Initialize dict.
    xy_coords = {}

    for key, value in degrees.items():

        # Take initial rotation (-90 deg) into account.
        radians = (offset - value) * np.pi / 180

        xy_coords[key] = (
            np.around(np.cos(radians) * radious, decimals=4),
            np.around(np.sin(radians) * radious, decimals=4),
        )
    
    return xy_coords

#-----#
# Handy function definition for tranforming
# the mouse coordinates system.
def transform_mouse(
    xlist,
    ylist,
    system,
    room,
    wrist_distance,
    unit_coords=[-1.0, 1.0],
    screen_coords=[-0.89, 0.89],
    rad=0.375,
    norm_units=False,
):
    
    # Tranform to array.
    if isinstance(xlist, float):
        xlist = [xlist]
    if isinstance(ylist, float):
        ylist = [ylist]
    
    # Define screen-to-arm scaling adjusted
    # to mouse moevement as recorded in
    # each operating system.
    if system == "windows":
        if room == "meg":
            x_adj = np.linspace(0.50, 3.11, 201)
            x_poss = np.arange(0.0, 2.01, 0.01)
        elif room == "eeg":
            x_adj = np.linspace(1.0, 2.34, 101)
            x_poss = np.arange(0.0, 1.01, 0.01)
        default_hole = 6
    elif system == "linux":
        x_adj = np.linspace(1.2, 2.81, 121)
        x_poss = np.arange(0.0, 1.21, 0.01)
        default_hole = 3
    
    y_adj = np.linspace(1.0, 1.2, 101)
    y_poss = np.arange(0.0, 1.01, 0.01)
    
    # Adjust according to wrist distance from
    # mouse (hole number).
    holes = np.around(
        np.arange(-0.45, 0.46, 0.05),
        2
    )
    
    selected_hole = wrist_distance - default_hole
    zero_hole = int(np.where(holes == 0)[0])

    x_adj += holes[zero_hole + selected_hole]

    # Use scaling.
    xlist = np.array(xlist) / x_adj[np.abs(xlist[-1]) <= x_poss][0]
    ylist = np.array(ylist) / y_adj[np.abs(ylist[-1]) <= y_poss][0]

    if norm_units == True:
        # Scale values from screen [min, max] to [-1, 1]
        # to have coordinates corresponding to the unit circle.
        xlist_scaled = unit_coords[0] + (xlist - screen_coords[0]) *\
            (unit_coords[1] - unit_coords[0]) / (screen_coords[1] - screen_coords[0])
        
        # Scale unit circle to custom radius.
        xlist_transformed = xlist_scaled * rad

        # Compute angle.
        xlist_angle = 90 - np.arccos(xlist_scaled) * 180 / np.pi

        # y is defined by x on the unit circle, then scaled
        # to custom radius.
        ylist_transformed = np.sin(np.arccos(xlist_scaled)) * rad

    elif norm_units == False:
        # Restrict values to [-rad, rad].
        if xlist.shape[0] == 1:
            if xlist > rad:
                xlist = rad
            elif xlist < -rad:
                xlist = -rad
        elif xlist.shape[0] > 1:
            over = xlist > rad
            under = xlist < -rad
            xlist[over] = rad
            xlist[under] = -rad
        
        # Restrict values to [0, rad]
        if ylist.shape[0] == 1:
            if ylist > rad:
                ylist = rad
            elif ylist < 0.0:
                ylist = 0.0
        elif ylist.shape[0] > 1:
            over = ylist > rad
            under = ylist < 0.0
            ylist[over] = rad
            ylist[under] = 0.0
        
        # Rename for consistency.
        xlist_transformed = xlist
        
        if isinstance(xlist_transformed, float):
            xlist_transformed = [xlist_transformed]
        xlist_transformed = np.array(xlist_transformed)
        
        # Compute angle.
        xlist_true_angle = np.arccos(xlist / rad)
        xlist_angle = 90 - xlist_true_angle * 180 / np.pi
        
        if isinstance(xlist_angle, float):
            xlist_angle = [xlist_angle]
        xlist_angle = np.array(xlist_angle)

        # y is defined on circle with custom radius.
        ylist_scaled = np.sin(xlist_true_angle) * rad 
        ylist_transformed = ylist_scaled - (rad - ylist)

        if isinstance(ylist_transformed, float):
            ylist_transformed = [ylist_transformed]
        ylist_transformed = np.array(ylist_transformed)

        # Restrict to [ylist_scaled, rad].
        if ylist_transformed.shape[0] == 1:
            if ylist_transformed > rad:
                ylist_transformed = rad
            elif ylist_transformed < ylist_scaled:
                ylist_transformed = ylist_scaled
        elif ylist_transformed.shape[0] > 1:
            over = ylist_transformed > rad
            under = ylist_transformed < ylist_scaled
            ylist_transformed[over] = rad
            ylist_transformed[under] = ylist_scaled[under]
       
        if isinstance(ylist_transformed, float):
            ylist_transformed = [ylist_transformed]
        ylist_transformed = np.array(ylist_transformed)
        
    return xlist_transformed, ylist_transformed, xlist_angle

#-----#
# Handy function definition for adjusting
# timing of IMAGERY trials based on ACTION
# trials.
def read_action_tr(blocks_timings, blocks_degrees, catch_offsets):
    """
    Read saved behavioral parameters from a list of blocks.
    Find 25th and 75th iqrs for movement execution
    depending on target.

    Inputs
    ------
    blocks_timings: list
             List of .csv files. Each file corresponds to
             measurements of a different block.
    blocks_degrees: list
             List of .csv files. Each file corresponds to
             measurements of a different block.
     catch_offsets: np.array
            Array of offsets before switching from
            IMAGERY to CATCH trial. Used to provide
            'default' values in case some target is
            missing in the provided blocks.
    
    Outputs
    -------
    action_timings: dict
            Dictionary with metrics across blocks and trials.
            Each key corresponds to a different experimental
            condition.
    """

    # Initialize dictionary and targets.
    action_timings = {
        "-60": {
            "timings": np.array([]),
            "time_percs": np.array([]),
            "speeds": np.array([]),
            "move_percs": np.array([]),
        },
        "-30": {
            "timings": np.array([]),
            "time_percs": np.array([]),
            "speeds": np.array([]),
            "move_percs": np.array([]),
        },
        "30": {
            "timings": np.array([]),
            "time_percs": np.array([]),
            "speeds": np.array([]),
            "move_percs": np.array([]),
        },
        "60": {
            "timings": np.array([]),
            "time_percs": np.array([]),
            "speeds": np.array([]),
            "move_percs": np.array([]),
        },
    }

    unique_targets = np.array([-60, -30,  30, 60])

    # Read each block.
    for b, (block_t, block_d) in enumerate(zip(blocks_timings, blocks_degrees)):

        # Abs path and loading.
        flnm_t = join(_thisDir, subject_dir, room, block_t)
        flnm_d = join(_thisDir, subject_dir, room, block_d)
        
        data_t = np.load(flnm_t)
        
        with open(flnm_d, "rb") as f:
            data_d = pickle.load(f)
        
        # Separate targets and timings.
        targets = data_t[0]
        timings = data_t[1].astype(np.float64)

        # Populate dictionary.
        for target in unique_targets:
            
            # Target name.
            target_name = str(int(target))
            
            # Find corresponding timings' ids.
            tids = np.where(targets == target)[0]
            
            if len(tids) > 0:
                
                # Add per-trial values in corresponding keys.
                # Time needed to complete a movement.
                action_timings[target_name]["timings"] = np.append(
                    action_timings[target_name]["timings"], 
                    timings[tids],
                )
                
                # Degrees per second for a movement.
                time_elapsed = [data_d[1][str(id)] for id in tids]
                degrees_moved = [data_d[0][str(id)] for id in tids]
                non_zero = [np.nonzero(data_d[0][str(id)])[0][0] for id in tids]

                for r, nz in enumerate(non_zero):
                    lr = linregress(
                        time_elapsed[r][nz:],
                        degrees_moved[r][nz:],
                    )
                
                action_timings[target_name]["speeds"] = np.append(
                    action_timings[target_name]["speeds"],
                    lr.slope,
                )
            
            else:
                
                # Add "fall-back" values.
                action_timings[target_name]["timings"] = np.append(
                    action_timings[target_name]["timings"], 
                    catch_offsets,
                )

                action_timings[target_name]["speeds"] = np.append(
                    action_timings[target_name]["speeds"],
                    np.random.randint(20, high=41, size=100),
                )

    # Compute 25th and 75th percentiles for
    # time required to perform a movement
    # and speed of corresponding movement.
    for target in unique_targets:
        
        # Target name.
        target_name = str(int(target))

        action_timings[target_name]["time_percs"] = np.percentile(action_timings[str(target)]["timings"], [25, 75])
        action_timings[target_name]["move_percs"] = np.percentile(action_timings[str(target)]["speeds"], [25, 75])
    
    return action_timings


# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)
# Store info about the experiment session
psychopyVersion = '2022.2.5'
expName = 'training_session'  # from the Builder filename that created this script
expInfo = {
    'participant': '01',
    'initials': 'XX',
    'session': '01',
    'run': '01',
    'locale': 'en',
    'system': 'linux',
    'room': 'eeg',
    'wrist_distance': '3',
    'block_id': '0',
}
# --- Show participant info dialog --
dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/sub_%s/%s_%s_%s' % (expInfo['participant'], expInfo['initials'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='/home/sotpapad/git_stuff/moefee/experimental_paradigm/training_session.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp
frameTolerance = 0.001  # how close to onset before 'same' frame

# Start Code - component code to be run after the window creation

# --- Setup the Window ---
win = visual.Window(
    size=[1920, 1080], fullscr=True, screen=1, 
    winType='pyglet', allowStencil=False,
    monitor='Laptop_monitor', color=[0,0,0], colorSpace='rgb',
    blendMode='avg', useFBO=True)
win.mouseVisible = False
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess
# --- Setup input devices ---
ioConfig = {}

# Setup iohub keyboard
ioConfig['Keyboard'] = dict(use_keymap='psychopy')

ioSession = '1'
if 'session' in expInfo:
    ioSession = str(expInfo['session'])
ioServer = io.launchHubServer(window=win, **ioConfig)
eyetracker = None

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard(backend='iohub')

# --- Initialize components for Routine "LoadData" ---
# Run 'Begin Experiment' code from load_data
#-----#
# Locale.
locale = expInfo["locale"]
if locale != "en" and locale != "fr":
    raise ValueError("Locale can only correspond to 'en' or 'fr' but '{}' provided.".format(locale))

if locale == "en":
    valid_keys = ["space", "r", "g", "b", "y", "m"]
elif locale == "fr":
    valid_keys = ["space", "r", "g", "b", "y", ","]

#-----#
# Recording room (requires different mouse calibration).
room = str(expInfo["room"])
if room != "meg" and room != "eeg":
    raise ValueError("Room can only correspond to 'meg' or 'eeg' but '{}' provided.".format(locale))

# Operating system.
system = str(expInfo["system"])
if system != "linux" and system != "windows":
    raise ValueError("System can only correspond to 'linux' or 'windows' but '{}' provided.".format(system))

# Loading of all data for randomization.
if system == "linux":
    datapath = "/home/sotpapad/Codes/experiment/"
elif system == "windows":
    if room == "meg":
        datapath = "C:\Python_users\Hifi\\"
    elif room == "eeg":
        datapath = "C:\\Users\\Dycog\\Desktop\\hifi\\"

# What type of session is this?
session_type_str =  "_training"
if session_type_str == "_training":
    session_type_msg = "Training"
elif session_type_str == "_calibration":
    session_type_msg = "Calibration"
if session_type_str == "":
    session_type_msg = "Experiment"
 
datafile = "{}exp_design_{}{}.pickle".format(datapath, mode, session_type_str)

with open(datafile, "rb") as file:
    subjects_data = pickle.load(file)

print("Working on {} system.".format(system))
print("Session type: {}.".format(session_type_msg))
print("\n")

#-----#
# Wrist placement.
wrist_distance = int(expInfo["wrist_distance"])
if wrist_distance < 1 or wrist_distance > 15:
    raise ValueError("Wrist distance must be an integer between 1 and 15.")

#-----#
# Relative coordinates of targets positions on screen.
targets_degrees = {
        "-60": (-60.0),
        "-30": (-30.0),
        "0": (0.0),
        "30": (30.0),
        "60": (60.0),
}
targets_coords = deg_to_xy(targets_degrees)

targets_catch_degrees = {
        "-75": (-75.0),
        "-60": (-60.0),
        "-45": (-45.0),
        "-30": (-30.0),
        "-15": (-15.0),
        "0": (0.0),
        "15": (15.0),
        "30": (30.0),
        "45": (45.0),
        "60": (60.0),
        "75": (75.0),
}
targets_catch_coords = deg_to_xy(targets_catch_degrees)
 
# Get names of CATCH target positions.
catch_keys = list(targets_catch_coords.keys())

#-----#
# Initial pseudo-position for target and intitial cursor position (0).
target_coords = targets_coords["0"]
cursor_coords = targets_coords["0"]

# Initial pseudo-state for online cursor adjustment.
mouse_is_moving = False
mouse_x = 0
mouse_y = 0

# Confidence degrees for cursor and target overlap check.
mouse_conf = 2
mouse_adj_conf = 2

# Variables that saves the state of trial with
# unacceptable reaction time.
too_slow = False
too_fast = False
do_not_move = False
too_far = 0
not_far_enough = 0
far_enough = 1
 
#-----#
# Targets, cursor, arc and fixation cross properties.
target_size = (0.10)
target_fill_col = (-1.0000, -1.0000, 1.0000)
target_border_col = (-1.0000, -1.0000, 1.0000)
target_fill_col_go = (1.0000, -1.0000, 1.0000)
target_border_col_go = (1.0000, -1.0000, 1.0000)

cursor_size = (0.06)
cursor_fill_col = (1.0000, -1.0000, -1.0000)
cursor_border_col = (1.0000, -1.0000, -1.0000)
cursor_adj_fill_col = (1.0000, 0.2941, -1.0000)
cursor_adj_border_col = (1.0000, 0.2941, -1.0000)

arc_size = (0.75, 0.4)
arc_position = (0.0, 0.18)
arc_image = "images/arc_new.png"
arc_catch_image = "images/arc_catch_new.png"
arc_fg_col = [1,1,1]

fix_size = (0.04)
fix_position = (0.0, 0.0)
fix_col = "white"

screen_lims = (0.888, 0.5)

#-----#
# Text properties.
text_size = 0.03
text_pos = (0.0, 0.25)
text_pos_upper = (0.0, 0.45)
text_pos_slow_fast = (0.0, 0.25)
text_wrap = 1.5

#-----#
# Random number generator initialization.
rng = np.random.default_rng()

# Uniform distribution of ITPs from 2 to 4 seconds.
list_of_durs = np.arange(2.0, 4.05, 0.1)

# Time offset in order to avoid flickering (s).
avoid_flicker = 0.05

# Time offset to avoid 'gestalt' effect during
# imagery and catch cursor disappearance and
# convertion of target to go cue (s).
avoid_gestalt = 0.1

# Time offsets to switch from normal to CATCH
# arc in CATCH trials (s).
catch_offsets = np.arange(0.5, 2.01, 0.1)

#-----#
# Trigger values.

# Start and end of block of trials.
tr_begin_block = 220.0
tr_end_block = 230.0

# Start and end of trial (following ITP, starts
# when stimuli appear;  includes rebound
# periods).
tr_begin_trial = 200.0
tr_end_trial = 210.0

# Start of ITP (fixation.)
tr_begin_itp = 180.0

# Start and end of generic rebound period
# (after ADJUST, IMAGERY and CATCH trials).
tr_begin_gen_rebound = 20.0
tr_end_gen_rebound = 30.0

# Start and end of rebound period following
# ACTION trials.
tr_begin_rebound = 40.0
tr_end_rebound = 50.0

# ACTION trial GO cue onset and end.
tr_go_trial = 60.0
tr_go_trial_end = 70.0

# ADJUST trial onset and end.
tr_adj_trial = 80.0
tr_adj_trial_end = 90.0

# IMAGERY trial go cue onset and end.
tr_imag_trial = 100.0
tr_imag_trial_end = 110.0

# CATCH trial
tr_catch_trial = 120.0
tr_catch_trial_end = 130.0

# Trigger duration (s).
tr_dur = 0.1

#-----#
# Screen refresh rate.
screen_refresh_period = win.monitorFramePeriod

# Duration of mouse not moving to prematurelly end
# an action trial.
not_moving_dur = 1.0
not_moved_id = int(not_moving_dur / screen_refresh_period)

# Duration of no keypress to end a catch trial.
catch_inertia = 10.0
catch_threshold = 3.0
not_pressed_id = int(catch_threshold / screen_refresh_period)

# --- Initialize components for Routine "WelcomeScreen" ---
welcome_message = visual.TextStim(win=win, name='welcome_message',
    text=None,
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp = keyboard.Keyboard()
# Run 'Begin Experiment' code from welcome_code
#-----#
# Variable that allows the first action bloc
# to be repated.
action_block_counter = 0

# --- Initialize components for Routine "TrainingInstr" ---
training_str_1 = visual.TextStim(win=win, name='training_str_1',
    text=None,
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp_2 = keyboard.Keyboard()

# --- Initialize components for Routine "TrainingInstr_2" ---
training_str_2 = visual.TextStim(win=win, name='training_str_2',
    text=None,
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp_3 = keyboard.Keyboard()

# --- Initialize components for Routine "TrainingInstr_3" ---
training_str_3 = visual.TextStim(win=win, name='training_str_3',
    text=None,
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp_33 = keyboard.Keyboard()

# --- Initialize components for Routine "BeginScreen" ---
begin_session = visual.TextStim(win=win, name='begin_session',
    text=None,
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp_4 = keyboard.Keyboard()

# --- Initialize components for Routine "InitBlock" ---
block_info = visual.TextStim(win=win, name='block_info',
    text=None,
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);

# --- Initialize components for Routine "BeginTrial" ---
fixation_cross_itp = visual.ShapeStim(
    win=win, name='fixation_cross_itp', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=0.0, interpolate=True)
arc_itp = visual.ImageStim(
    win=win,
    name='arc_itp', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-1.0)

# --- Initialize components for Routine "ActionTrial" ---
fixation_cross_action_trial = visual.ShapeStim(
    win=win, name='fixation_cross_action_trial', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=0.0, interpolate=True)
arc_action_trial = visual.ImageStim(
    win=win,
    name='arc_action_trial', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-1.0)
mouse_action_trial = event.Mouse(win=win)
x, y = [None, None]
mouse_action_trial.mouseClock = core.Clock()
cursor_action_trial = visual.ShapeStim(
    win=win, name='cursor_action_trial',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_border_col, fillColor=cursor_fill_col,
    opacity=None, depth=-3.0, interpolate=True)
target_action_trial = visual.ShapeStim(
    win=win, name='target_action_trial',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-4.0, interpolate=True)
target_action_trial_sync = visual.ShapeStim(
    win=win, name='target_action_trial_sync',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-5.0, interpolate=True)
target_action_trial_go = visual.ShapeStim(
    win=win, name='target_action_trial_go',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col_go, fillColor=target_fill_col_go,
    opacity=None, depth=-6.0, interpolate=True)
cursor_action_trial_online = visual.ShapeStim(
    win=win, name='cursor_action_trial_online',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_border_col, fillColor=cursor_fill_col,
    opacity=None, depth=-7.0, interpolate=True)
experimenter_controls = keyboard.Keyboard()

# --- Initialize components for Routine "ActionRebound" ---
fixation_cross_action_rebound = visual.ShapeStim(
    win=win, name='fixation_cross_action_rebound', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=0.0, interpolate=True)
arc_action_rebound = visual.ImageStim(
    win=win,
    name='arc_action_rebound', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-1.0)
target_rebound = visual.ShapeStim(
    win=win, name='target_rebound',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col_go, fillColor=target_fill_col_go,
    opacity=None, depth=-2.0, interpolate=True)
cursor_rebound = visual.ShapeStim(
    win=win, name='cursor_rebound',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_border_col, fillColor=cursor_fill_col,
    opacity=None, depth=-3.0, interpolate=True)

# --- Initialize components for Routine "ActionTrialAdj" ---
slow_trial_info = visual.TextStim(win=win, name='slow_trial_info',
    text=None,
    font='Open Sans',
    pos=text_pos_slow_fast, height=text_size, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
fast_trial_info = visual.TextStim(win=win, name='fast_trial_info',
    text=None,
    font='Open Sans',
    pos=text_pos_slow_fast, height=text_size, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=-1.0);
text_adjust = visual.TextStim(win=win, name='text_adjust',
    text=None,
    font='Open Sans',
    pos=text_pos_upper, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=-2.0);
fixation_cross_adj_trial = visual.ShapeStim(
    win=win, name='fixation_cross_adj_trial', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=-3.0, interpolate=True)
arc_adj_trial = visual.ImageStim(
    win=win,
    name='arc_adj_trial', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-4.0)
mouse_adj_trial = event.Mouse(win=win)
x, y = [None, None]
mouse_adj_trial.mouseClock = core.Clock()
target_adj_trial = visual.ShapeStim(
    win=win, name='target_adj_trial',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-6.0, interpolate=True)
cursor_adj_trial = visual.ShapeStim(
    win=win, name='cursor_adj_trial',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=None, depth=-7.0, interpolate=True)
key_resp_adj = keyboard.Keyboard()

# --- Initialize components for Routine "GenericRebound" ---
arc_gen_rebound = visual.ImageStim(
    win=win,
    name='arc_gen_rebound', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=0.0)

# --- Initialize components for Routine "ActionMetrics" ---

# --- Initialize components for Routine "ImageryTrial" ---
fixation_cross_imag_trial = visual.ShapeStim(
    win=win, name='fixation_cross_imag_trial', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=0.0, interpolate=True)
arc_imag_trial = visual.ImageStim(
    win=win,
    name='arc_imag_trial', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-1.0)
cursor_imag_trial = visual.ShapeStim(
    win=win, name='cursor_imag_trial',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_border_col, fillColor=cursor_fill_col,
    opacity=None, depth=-2.0, interpolate=True)
target_imag_trial = visual.ShapeStim(
    win=win, name='target_imag_trial',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-3.0, interpolate=True)
target_imag_trial_sync = visual.ShapeStim(
    win=win, name='target_imag_trial_sync',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-4.0, interpolate=True)
target_imag_trial_go = visual.ShapeStim(
    win=win, name='target_imag_trial_go',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col_go, fillColor=target_fill_col_go,
    opacity=None, depth=-5.0, interpolate=True)

# --- Initialize components for Routine "ImageryRebound" ---
moved_hand_in_imag = visual.TextStim(win=win, name='moved_hand_in_imag',
    text=None,
    font='Open Sans',
    pos=text_pos_slow_fast, height=text_size, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
fixation_cross_imagery_rebound = visual.ShapeStim(
    win=win, name='fixation_cross_imagery_rebound', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=-1.0, interpolate=True)
arc_imagery_rebound = visual.ImageStim(
    win=win,
    name='arc_imagery_rebound', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-2.0)

# --- Initialize components for Routine "GenericRebound" ---
arc_gen_rebound = visual.ImageStim(
    win=win,
    name='arc_gen_rebound', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=0.0)

# --- Initialize components for Routine "CatchTrial" ---
text_catch = visual.TextStim(win=win, name='text_catch',
    text=None,
    font='Open Sans',
    pos=text_pos_upper, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
fixation_cross_catch_trial = visual.ShapeStim(
    win=win, name='fixation_cross_catch_trial', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=-1.0, interpolate=True)
arc_catch_trial_normal = visual.ImageStim(
    win=win,
    name='arc_catch_trial_normal', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-2.0)
arc_catch_trial = visual.ImageStim(
    win=win,
    name='arc_catch_trial', 
    image=arc_catch_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-3.0)
cursor_catch_trial = visual.ShapeStim(
    win=win, name='cursor_catch_trial',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_border_col, fillColor=cursor_fill_col,
    opacity=None, depth=-4.0, interpolate=True)
cursor_catch_trial_bp = visual.ShapeStim(
    win=win, name='cursor_catch_trial_bp',
    size=cursor_size, vertices='triangle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=cursor_adj_border_col, fillColor=cursor_adj_fill_col,
    opacity=None, depth=-5.0, interpolate=True)
target_catch_trial = visual.ShapeStim(
    win=win, name='target_catch_trial',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col, fillColor=target_fill_col,
    opacity=None, depth=-6.0, interpolate=True)
target_catch_trial_go = visual.ShapeStim(
    win=win, name='target_catch_trial_go',
    size=target_size, vertices='circle',
    ori=0.0, pos=None, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=target_border_col_go, fillColor=target_fill_col_go,
    opacity=None, depth=-7.0, interpolate=True)
key_resp_catch = keyboard.Keyboard()

# --- Initialize components for Routine "ImageryRebound" ---
moved_hand_in_imag = visual.TextStim(win=win, name='moved_hand_in_imag',
    text=None,
    font='Open Sans',
    pos=text_pos_slow_fast, height=text_size, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
fixation_cross_imagery_rebound = visual.ShapeStim(
    win=win, name='fixation_cross_imagery_rebound', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=-1.0, interpolate=True)
arc_imagery_rebound = visual.ImageStim(
    win=win,
    name='arc_imagery_rebound', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-2.0)

# --- Initialize components for Routine "CatchRebound" ---
text_catch_rebound = visual.TextStim(win=win, name='text_catch_rebound',
    text=None,
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp_reb = keyboard.Keyboard()

# --- Initialize components for Routine "GenericRebound" ---
arc_gen_rebound = visual.ImageStim(
    win=win,
    name='arc_gen_rebound', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=0.0)

# --- Initialize components for Routine "EndTrial" ---
fixation_cross_end = visual.ShapeStim(
    win=win, name='fixation_cross_end', vertices='cross',
    size=fix_size,
    ori=0.0, pos=fix_position, anchor='center',
    lineWidth=1.0,     colorSpace='rgb',  lineColor=fix_col, fillColor=fix_col,
    opacity=None, depth=0.0, interpolate=True)
arc_itp_end = visual.ImageStim(
    win=win,
    name='arc_itp_end', 
    image=arc_image, mask=None, anchor='center',
    ori=0.0, pos=arc_position, size=arc_size,
    color=arc_fg_col, colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=512.0, interpolate=True, depth=-1.0)

# --- Initialize components for Routine "InterBlockInterval" ---
IBI = visual.TextStim(win=win, name='IBI',
    text=None,
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
IBI_2 = visual.TextStim(win=win, name='IBI_2',
    text=None,
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=-1.0);
IBI_gamification = visual.TextStim(win=win, name='IBI_gamification',
    text='Press any button to proceed to the next block.',
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=text_wrap, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=-2.0);
key_resp_5 = keyboard.Keyboard()

# --- Initialize components for Routine "ByeByeScreen" ---
bye_bye = visual.TextStim(win=win, name='bye_bye',
    text='The training session has ended.',
    font='Open Sans',
    pos=text_pos, height=text_size, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp_6 = keyboard.Keyboard()

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.Clock()  # to track time remaining of each (possibly non-slip) routine 

# --- Prepare to start Routine "LoadData" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
# keep track of which components have finished
LoadDataComponents = []
for thisComponent in LoadDataComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "LoadData" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in LoadDataComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "LoadData" ---
for thisComponent in LoadDataComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# Run 'End Routine' code from load_data
#-----#
# Specific subject data.
sub_id_bids = expInfo["participant"]
subject_id = "subject_{}".format(int("01"))#int(sub_id_bids))

this_sub_data_rec_order = subjects_data[subject_id]["rec_order"]
this_sub_data_hand_order = subjects_data[subject_id]["hand_order"]
this_sub_data_cond_order = subjects_data[subject_id]["condition_order"]

# Remove keys not encoding block structure.
this_sub_data_block_order = subjects_data[subject_id].keys()
this_sub_data_block_order = list(this_sub_data_block_order)
this_sub_data_block_order.remove("rec_order")
this_sub_data_block_order.remove("hand_order")
this_sub_data_block_order.remove("condition_order")

#-----#
# Create a subject-specific folder.
subject_dir = join(_thisDir, "data", "sub_{}".format(sub_id_bids))
if not exists(subject_dir):
    makedirs(subject_dir)

# Create appropriate subdirectory.
sub_room_dir = join(_thisDir, subject_dir, room)
if not exists(sub_room_dir):
    makedirs(sub_room_dir)

if debug_mode == True:
    print("Subject directory: {}".format(subject_dir))
    print("Recording room directory: {}".format(sub_room_dir))

#-----#
# Print out information.
ses_num = int(expInfo["session"]) - 1
run_num = int(expInfo["run"]) - 1
print("Session {}".format(ses_num + 1))
print("Run {}".format(run_num + 1))

print("Subject's 1st recording modality: {}.".format(this_sub_data_rec_order[0]))
print("This session starts with: {}.".format(this_sub_data_hand_order[ses_num][run_num]))
print("This session starts with: {}.".format(this_sub_data_cond_order[ses_num][run_num]))

# Adjust trigger values based on hand.
if this_sub_data_hand_order[ses_num][run_num] == "left_hand":
    trigger_hand_value = 0.0
elif this_sub_data_hand_order[ses_num][run_num] == "right_hand":
    trigger_hand_value = 4.0

#-----#
# Only use variables of current recording.
sub_block_strct = this_sub_data_block_order
 
#-----#
# Variables that keep track of the loop ids.
n_blocks = 2
block_id = int(expInfo["block_id"])
session_id = 0
# the Routine "LoadData" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "WelcomeScreen" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp.keys = []
key_resp.rt = []
_key_resp_allKeys = []
# Run 'Begin Routine' code from welcome_code
#-----#
# Set mouse visibility.
if show_mouse == True:
    win.mouseVisible = True
elif show_mouse == False:
    win.mouseVisible = False

#-----#
if locale == "en":
    welc_msg = "Welcome to the training session.\n\nPress any button to continue."
elif locale == "fr":
    welc_msg = "Bienvenue à la session d'entraînement.\n\nAppuyez sur n'importe quel bouton pour continuer."

welcome_message.text = welc_msg
# keep track of which components have finished
WelcomeScreenComponents = [welcome_message, key_resp]
for thisComponent in WelcomeScreenComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "WelcomeScreen" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *welcome_message* updates
    if welcome_message.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        welcome_message.frameNStart = frameN  # exact frame index
        welcome_message.tStart = t  # local t and not account for scr refresh
        welcome_message.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(welcome_message, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'welcome_message.started')
        welcome_message.setAutoDraw(True)
    
    # *key_resp* updates
    waitOnFlip = False
    if key_resp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp.frameNStart = frameN  # exact frame index
        key_resp.tStart = t  # local t and not account for scr refresh
        key_resp.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp.started')
        key_resp.status = STARTED
        # AllowedKeys looks like a variable named `valid_keys`
        if not type(valid_keys) in [list, tuple, np.ndarray]:
            if not isinstance(valid_keys, str):
                logging.error('AllowedKeys variable `valid_keys` is not string- or list-like.')
                core.quit()
            elif not ',' in valid_keys:
                valid_keys = (valid_keys,)
            else:
                valid_keys = eval(valid_keys)
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp.status == STARTED and not waitOnFlip:
        theseKeys = key_resp.getKeys(keyList=list(valid_keys), waitRelease=False)
        _key_resp_allKeys.extend(theseKeys)
        if len(_key_resp_allKeys):
            key_resp.keys = _key_resp_allKeys[-1].name  # just the last key pressed
            key_resp.rt = _key_resp_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in WelcomeScreenComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "WelcomeScreen" ---
for thisComponent in WelcomeScreenComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp.keys in ['', [], None]:  # No response was made
    key_resp.keys = None
thisExp.addData('key_resp.keys',key_resp.keys)
if key_resp.keys != None:  # we had a response
    thisExp.addData('key_resp.rt', key_resp.rt)
thisExp.nextEntry()
# the Routine "WelcomeScreen" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "TrainingInstr" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_2.keys = []
key_resp_2.rt = []
_key_resp_2_allKeys = []
# Run 'Begin Routine' code from hide_mouse_training_screen
#-----#
# Set mouse visibility.
if show_mouse == True:
    win.mouseVisible = True
elif show_mouse == False:
    win.mouseVisible = False

#-----#
if locale == "en":
    tr1_msg = "We will now proceed with a training session with 3 blocks of trials."
    tr1_msg_2 = "\n\nPress any button to continue."
elif locale == "fr":
    tr1_msg = "Nous allons maintenant procéder à une session d'entraînement avec 3 blocs d'essais."
    tr1_msg_2 = "\n\nAppuyez sur n'importe quel bouton pour continuer."

training_str_1.text = tr1_msg + tr1_msg_2
# keep track of which components have finished
TrainingInstrComponents = [training_str_1, key_resp_2]
for thisComponent in TrainingInstrComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "TrainingInstr" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *training_str_1* updates
    if training_str_1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        training_str_1.frameNStart = frameN  # exact frame index
        training_str_1.tStart = t  # local t and not account for scr refresh
        training_str_1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(training_str_1, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'training_str_1.started')
        training_str_1.setAutoDraw(True)
    
    # *key_resp_2* updates
    waitOnFlip = False
    if key_resp_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_2.frameNStart = frameN  # exact frame index
        key_resp_2.tStart = t  # local t and not account for scr refresh
        key_resp_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_2, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_2.started')
        key_resp_2.status = STARTED
        # AllowedKeys looks like a variable named `valid_keys`
        if not type(valid_keys) in [list, tuple, np.ndarray]:
            if not isinstance(valid_keys, str):
                logging.error('AllowedKeys variable `valid_keys` is not string- or list-like.')
                core.quit()
            elif not ',' in valid_keys:
                valid_keys = (valid_keys,)
            else:
                valid_keys = eval(valid_keys)
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_2.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_2.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_2.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_2.getKeys(keyList=list(valid_keys), waitRelease=False)
        _key_resp_2_allKeys.extend(theseKeys)
        if len(_key_resp_2_allKeys):
            key_resp_2.keys = _key_resp_2_allKeys[-1].name  # just the last key pressed
            key_resp_2.rt = _key_resp_2_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in TrainingInstrComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "TrainingInstr" ---
for thisComponent in TrainingInstrComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_2.keys in ['', [], None]:  # No response was made
    key_resp_2.keys = None
thisExp.addData('key_resp_2.keys',key_resp_2.keys)
if key_resp_2.keys != None:  # we had a response
    thisExp.addData('key_resp_2.rt', key_resp_2.rt)
thisExp.nextEntry()
# the Routine "TrainingInstr" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "TrainingInstr_2" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_3.keys = []
key_resp_3.rt = []
_key_resp_3_allKeys = []
# Run 'Begin Routine' code from train_2
#-----#
# Set mouse visibility.
if show_mouse == True:
    win.mouseVisible = True
elif show_mouse == False:
    win.mouseVisible = False

#-----#
if locale == "en":
    tr2_msg = (
    "The first two blocks will be ACTION blocks. "
    "In each trial, you will be required to flex or extend "
    "your hand in order to bring the cursor to the indicated target. "
    "Before proceeding to the next trial you will be asked to bring "
    "your hand back to neutral position."
    "\n\nDuring the first ACTION block you will be provided with a "
    "feedback of the cursor movement in order to get accustomed to it. "
    "Then, as in the experiment, no feedback will be provided."
    "'\n\nPress any button to continue."
    )
elif locale == "fr":
    tr2_msg = (
    "Les deux premiers blocs seront des blocs d'ACTION. "
    "Dans chaque essai, il vous sera demandé de fléchir ou d'étendre "
    "votre main afin d'amener le curseur sur la cible indiquée. "
    "Avant de passer à l'essai suivant, il vous sera demandé de ramener "
    "votre main en position neutre."
    "\n\nAu cours du premier bloc d'ACTION, vous recevrez un "
    "retour d'information sur le mouvement du curseur afin de vous y habituer. "
    "Ensuite, comme dans l'expérience, aucun retour d'information ne sera fourni."
    "\n\nAppuyez sur n'importe quel bouton pour continuer."
    )

training_str_2.text = tr2_msg
# keep track of which components have finished
TrainingInstr_2Components = [training_str_2, key_resp_3]
for thisComponent in TrainingInstr_2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "TrainingInstr_2" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *training_str_2* updates
    if training_str_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        training_str_2.frameNStart = frameN  # exact frame index
        training_str_2.tStart = t  # local t and not account for scr refresh
        training_str_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(training_str_2, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'training_str_2.started')
        training_str_2.setAutoDraw(True)
    
    # *key_resp_3* updates
    waitOnFlip = False
    if key_resp_3.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_3.frameNStart = frameN  # exact frame index
        key_resp_3.tStart = t  # local t and not account for scr refresh
        key_resp_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_3, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_3.started')
        key_resp_3.status = STARTED
        # AllowedKeys looks like a variable named `valid_keys`
        if not type(valid_keys) in [list, tuple, np.ndarray]:
            if not isinstance(valid_keys, str):
                logging.error('AllowedKeys variable `valid_keys` is not string- or list-like.')
                core.quit()
            elif not ',' in valid_keys:
                valid_keys = (valid_keys,)
            else:
                valid_keys = eval(valid_keys)
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_3.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_3.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_3.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_3.getKeys(keyList=list(valid_keys), waitRelease=False)
        _key_resp_3_allKeys.extend(theseKeys)
        if len(_key_resp_3_allKeys):
            key_resp_3.keys = _key_resp_3_allKeys[-1].name  # just the last key pressed
            key_resp_3.rt = _key_resp_3_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in TrainingInstr_2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "TrainingInstr_2" ---
for thisComponent in TrainingInstr_2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_3.keys in ['', [], None]:  # No response was made
    key_resp_3.keys = None
thisExp.addData('key_resp_3.keys',key_resp_3.keys)
if key_resp_3.keys != None:  # we had a response
    thisExp.addData('key_resp_3.rt', key_resp_3.rt)
thisExp.nextEntry()
# the Routine "TrainingInstr_2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "TrainingInstr_3" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_33.keys = []
key_resp_33.rt = []
_key_resp_33_allKeys = []
# Run 'Begin Routine' code from train_3
#-----#
# Set mouse visibility.
if show_mouse == True:
    win.mouseVisible = True
elif show_mouse == False:
    win.mouseVisible = False

#-----#
if locale == "en":
    tr3_msg = (
    "The final block will be an IMAGERY one. "
    "During IMAGERY trials, try to imagine the "
    "experience of your hand moving. DO NOT "
    "try to mentally visualize your hand moving."
    "\n\nPress any button to continue."
    )
elif locale == "fr":
    tr3_msg = (
    "Le dernier bloc sera un bloc IMAGERY. "
    "Pendant les essais d'IMAGERY, essayez d'imaginer "
    "le mouvement de votre main. N'essayez PAS "
    "de visualiser mentalement votre main en mouvement."
    "\n\nAppuyez sur n'importe quel bouton pour continuer."
    )

training_str_3.text = tr3_msg
# keep track of which components have finished
TrainingInstr_3Components = [training_str_3, key_resp_33]
for thisComponent in TrainingInstr_3Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "TrainingInstr_3" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *training_str_3* updates
    if training_str_3.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        training_str_3.frameNStart = frameN  # exact frame index
        training_str_3.tStart = t  # local t and not account for scr refresh
        training_str_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(training_str_3, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'training_str_3.started')
        training_str_3.setAutoDraw(True)
    
    # *key_resp_33* updates
    waitOnFlip = False
    if key_resp_33.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_33.frameNStart = frameN  # exact frame index
        key_resp_33.tStart = t  # local t and not account for scr refresh
        key_resp_33.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_33, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_33.started')
        key_resp_33.status = STARTED
        # AllowedKeys looks like a variable named `valid_keys`
        if not type(valid_keys) in [list, tuple, np.ndarray]:
            if not isinstance(valid_keys, str):
                logging.error('AllowedKeys variable `valid_keys` is not string- or list-like.')
                core.quit()
            elif not ',' in valid_keys:
                valid_keys = (valid_keys,)
            else:
                valid_keys = eval(valid_keys)
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_33.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_33.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_33.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_33.getKeys(keyList=list(valid_keys), waitRelease=False)
        _key_resp_33_allKeys.extend(theseKeys)
        if len(_key_resp_33_allKeys):
            key_resp_33.keys = _key_resp_33_allKeys[-1].name  # just the last key pressed
            key_resp_33.rt = _key_resp_33_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in TrainingInstr_3Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "TrainingInstr_3" ---
for thisComponent in TrainingInstr_3Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_33.keys in ['', [], None]:  # No response was made
    key_resp_33.keys = None
thisExp.addData('key_resp_33.keys',key_resp_33.keys)
if key_resp_33.keys != None:  # we had a response
    thisExp.addData('key_resp_33.rt', key_resp_33.rt)
thisExp.nextEntry()
# the Routine "TrainingInstr_3" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# --- Prepare to start Routine "BeginScreen" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_4.keys = []
key_resp_4.rt = []
_key_resp_4_allKeys = []
# Run 'Begin Routine' code from begin_screen_code
#-----#
# Set mouse visibility.
if show_mouse == True:
    win.mouseVisible = True
elif show_mouse == False:
    win.mouseVisible = False

#-----#
if locale == "en":
    begin_msg = "Press any button to start this session."
elif locale == "fr":
    begin_msg = "Appuyez sur n'importe quel bouton pour démarrer cette session."

begin_session.text = begin_msg
# keep track of which components have finished
BeginScreenComponents = [begin_session, key_resp_4]
for thisComponent in BeginScreenComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "BeginScreen" ---
while continueRoutine:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *begin_session* updates
    if begin_session.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        begin_session.frameNStart = frameN  # exact frame index
        begin_session.tStart = t  # local t and not account for scr refresh
        begin_session.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(begin_session, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'begin_session.started')
        begin_session.setAutoDraw(True)
    
    # *key_resp_4* updates
    waitOnFlip = False
    if key_resp_4.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_4.frameNStart = frameN  # exact frame index
        key_resp_4.tStart = t  # local t and not account for scr refresh
        key_resp_4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_4, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_4.started')
        key_resp_4.status = STARTED
        # AllowedKeys looks like a variable named `valid_keys`
        if not type(valid_keys) in [list, tuple, np.ndarray]:
            if not isinstance(valid_keys, str):
                logging.error('AllowedKeys variable `valid_keys` is not string- or list-like.')
                core.quit()
            elif not ',' in valid_keys:
                valid_keys = (valid_keys,)
            else:
                valid_keys = eval(valid_keys)
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_4.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_4.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_4.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_4.getKeys(keyList=list(valid_keys), waitRelease=False)
        _key_resp_4_allKeys.extend(theseKeys)
        if len(_key_resp_4_allKeys):
            key_resp_4.keys = _key_resp_4_allKeys[-1].name  # just the last key pressed
            key_resp_4.rt = _key_resp_4_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in BeginScreenComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "BeginScreen" ---
for thisComponent in BeginScreenComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_4.keys in ['', [], None]:  # No response was made
    key_resp_4.keys = None
thisExp.addData('key_resp_4.keys',key_resp_4.keys)
if key_resp_4.keys != None:  # we had a response
    thisExp.addData('key_resp_4.rt', key_resp_4.rt)
thisExp.nextEntry()
# the Routine "BeginScreen" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
BLOCK = data.TrialHandler(nReps=n_blocks + 2, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=[None],
    seed=None, name='BLOCK')
thisExp.addLoop(BLOCK)  # add the loop to the experiment
thisBLOCK = BLOCK.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisBLOCK.rgb)
if thisBLOCK != None:
    for paramName in thisBLOCK:
        exec('{} = thisBLOCK[paramName]'.format(paramName))

for thisBLOCK in BLOCK:
    currentLoop = BLOCK
    # abbreviate parameter names if possible (e.g. rgb = thisBLOCK.rgb)
    if thisBLOCK != None:
        for paramName in thisBLOCK:
            exec('{} = thisBLOCK[paramName]'.format(paramName))
    
    # --- Prepare to start Routine "InitBlock" ---
    continueRoutine = True
    routineForceEnded = False
    # update component parameters for each repeat
    # Run 'Begin Routine' code from itp_n_block
    #-----#
    # Set mouse visibility.
    if show_mouse == True:
        win.mouseVisible = True
    elif show_mouse == False:
        win.mouseVisible = False
    
    #-----#
    # Load correct block of trials.
    block_name = "block_{}".format(block_id + 1)
    this_block = subjects_data[subject_id][sub_block_strct[session_id]][block_name]
    
    print("\nBlock: {}, {}".format((block_id + 1),  this_block["condition"]))
    
    # Unpack block variables.
    this_block_cursor_seq = this_block["cursor_seq"]
    this_block_target_seq = this_block["target_seq"]
    #this_block_ampl_seq = this_block["ampl_seq"]
    this_block_catch_seq = this_block["catch_seq"]
    
    if debug_mode == True:
        print("\nCatch sequence: {}\n".format(this_block_catch_seq))
    
    n_trials = len(this_block_target_seq)
    
    # Inform type of block.
    this_block_type = this_block["condition"]
    block_info.text = this_block_type.upper()
    
    #-----#
    # Recording durations based on recording modality.
    if this_block_type == "imagery":
        concentration_period = 2.0
    elif this_block_type == "action":
        concentration_period = 1.0
    
    if cursor_target_sync == False:
        reaction_period = concentration_period + 1.0
    elif cursor_target_sync == True:
        if this_block_type == "imagery":
            reaction_period = concentration_period + 1.5
        elif this_block_type == "action":
            concentration_period += 1.0
            go_period = concentration_period + 0.5
            reaction_period = go_period + 1.0
    
    task_period = reaction_period + 3.0
    rebound_period = 2.0
    if action_block_counter == 1:
        rebound_period = 2.5
    
    too_slow_period = 3.0
    too_fast_period = 3.0
    
    #-----#
    # Trial count.
    trial_id = 0
    
    #-----#
    # Set initial target and cursor position.
    target_coords = targets_coords[str(this_block_target_seq[trial_id])]
    if this_block_type == "imagery":
        
        cursor_imag_trial.setPos(cursor_coords)
        cursor_catch_trial.setPos(cursor_coords)
        cursor_catch_trial_bp.setPos(cursor_coords)
    
        if cursor_target_sync == False:
            target_imag_trial.setPos(target_coords)
        elif cursor_target_sync == True:
            target_imag_trial_sync.setPos(target_coords)
            target_imag_trial_go.setPos(target_coords)
        
        target_catch_trial.setPos(target_coords)
        target_catch_trial_go.setPos(target_coords)
    
    elif this_block_type == "action":
           
        cursor_action_trial.setPos(cursor_coords)
        cursor_adj_trial.setPos(cursor_coords)
    
        if cursor_target_sync == False:
            target_action_trial.setPos(target_coords)
        elif cursor_target_sync == True:
            cursor_action_trial_online.setPos(cursor_coords)
            target_action_trial_sync.setPos(target_coords)
            target_action_trial_go.setPos(target_coords)
            target_rebound.setPos(target_coords)
        
        target_adj_trial.setPos(cursor_coords)
    
        # Set relative mouse position to target zero
        # when a block starts.
        mouse_action_trial.setPos(cursor_coords)
        mouse_adj_trial.setPos(cursor_coords)
        
        # Reset mouse position for adjustment period
        # when a block starts.
        mouse_x = 0
        mouse_y = 0
    # keep track of which components have finished
    InitBlockComponents = [block_info]
    for thisComponent in InitBlockComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "InitBlock" ---
    while continueRoutine and routineTimer.getTime() < 2.0:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *block_info* updates
        if block_info.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            block_info.frameNStart = frameN  # exact frame index
            block_info.tStart = t  # local t and not account for scr refresh
            block_info.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(block_info, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'block_info.started')
            block_info.setAutoDraw(True)
        if block_info.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > block_info.tStartRefresh + 2.0-frameTolerance:
                # keep track of stop time/frame for later
                block_info.tStop = t  # not accounting for scr refresh
                block_info.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'block_info.stopped')
                block_info.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in InitBlockComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "InitBlock" ---
    for thisComponent in InitBlockComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # Run 'End Routine' code from itp_n_block
    # Set up variables to store the duration of all
    # ACTION trials that are considered as valid, meaning
    # the reaction time was not too slow or too fast
    # compared to some preset thresholds, and
    # the calculated angle per frame.
    if this_block_type == "action":
        valid_move_trials = np.zeros(n_trials)
        valid_move_timings = np.zeros(n_trials)
    
        all_mouse_degrees = {}
        all_frame_times = {}
    # using non-slip timing so subtract the expected duration of this Routine (unless ended on request)
    if routineForceEnded:
        routineTimer.reset()
    else:
        routineTimer.addTime(-2.000000)
    
    # set up handler to look after randomisation of conditions etc
    TRIAL = data.TrialHandler(nReps=n_trials, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=[None],
        seed=None, name='TRIAL')
    thisExp.addLoop(TRIAL)  # add the loop to the experiment
    thisTRIAL = TRIAL.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisTRIAL.rgb)
    if thisTRIAL != None:
        for paramName in thisTRIAL:
            exec('{} = thisTRIAL[paramName]'.format(paramName))
    
    for thisTRIAL in TRIAL:
        currentLoop = TRIAL
        # abbreviate parameter names if possible (e.g. rgb = thisTRIAL.rgb)
        if thisTRIAL != None:
            for paramName in thisTRIAL:
                exec('{} = thisTRIAL[paramName]'.format(paramName))
        
        # --- Prepare to start Routine "BeginTrial" ---
        continueRoutine = True
        routineForceEnded = False
        # update component parameters for each repeat
        # Run 'Begin Routine' code from initialize_trial
        #-----#
        # Uniform random sample of ITP.
        itp_duration = rng.choice(list_of_durs, 1)
        
        #-----#
        # Monitor trials.
        n_action_trial = 0
        n_imag_trial = 0
        n_catch_trial = 0
        catch_eval = False  # evaluation of catch trial pending
        
        if this_block_type == "imagery":
            
            n_action_trial = 0
            
            # Non-zero values correspond to "IMAGERY" blocks.
            if this_block_catch_seq[trial_id] == 1.0:
                print("Catch trial (# {}) !".format(trial_id + 1))
                is_catch_trial = True
                
                # Execute a "CATCH" trial, instead of a normal "IMAGERY" one.
                n_imag_trial = 0
                n_catch_trial = 1
                
                # We need to evaluate the "IMAGERY" quality,
                # but have not done it yet.
                catch_eval = False
                
                # Time offset to switch from normal to CATCH
                # arc in CATCH trials.
                catch_offset = rng.choice(catch_offsets, 1)
            
            else:
                # Execute a normal "IMAGERY" trial.
                n_imag_trial = 1
                n_catch_trial = 0
        
        elif this_block_type == "action":
            
            # Execute an "ACTION" trial.
            n_action_trial = 1
            n_imag_trial = 0
            n_catch_trial = 0
        
        #-----#
        # Adjust trigger values based on trial target.
        # Retrieve target position for current trial.
        target_pos = str(this_block_target_seq[trial_id])
        target_deg = targets_degrees[target_pos]
        
        if target_deg == 30.0:
            trigger_deg_value = 0.0
        elif target_deg == 60.0:
            trigger_deg_value = 1.0
        elif target_deg == -30.0:
            trigger_deg_value = 2.0
        elif target_deg == -60.0:
            trigger_deg_value = 3.0
        # keep track of which components have finished
        BeginTrialComponents = [fixation_cross_itp, arc_itp]
        for thisComponent in BeginTrialComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "BeginTrial" ---
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *fixation_cross_itp* updates
            if fixation_cross_itp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                fixation_cross_itp.frameNStart = frameN  # exact frame index
                fixation_cross_itp.tStart = t  # local t and not account for scr refresh
                fixation_cross_itp.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(fixation_cross_itp, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'fixation_cross_itp.started')
                fixation_cross_itp.setAutoDraw(True)
            if fixation_cross_itp.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > fixation_cross_itp.tStartRefresh + itp_duration + avoid_flicker-frameTolerance:
                    # keep track of stop time/frame for later
                    fixation_cross_itp.tStop = t  # not accounting for scr refresh
                    fixation_cross_itp.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'fixation_cross_itp.stopped')
                    fixation_cross_itp.setAutoDraw(False)
            
            # *arc_itp* updates
            if arc_itp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                arc_itp.frameNStart = frameN  # exact frame index
                arc_itp.tStart = t  # local t and not account for scr refresh
                arc_itp.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(arc_itp, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'arc_itp.started')
                arc_itp.setAutoDraw(True)
            if arc_itp.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > arc_itp.tStartRefresh + itp_duration + avoid_flicker-frameTolerance:
                    # keep track of stop time/frame for later
                    arc_itp.tStop = t  # not accounting for scr refresh
                    arc_itp.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'arc_itp.stopped')
                    arc_itp.setAutoDraw(False)
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in BeginTrialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "BeginTrial" ---
        for thisComponent in BeginTrialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        # the Routine "BeginTrial" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        
        # set up handler to look after randomisation of conditions etc
        ACTIONTRIAL = data.TrialHandler(nReps=n_action_trial, method='random', 
            extraInfo=expInfo, originPath=-1,
            trialList=[None],
            seed=None, name='ACTIONTRIAL')
        thisExp.addLoop(ACTIONTRIAL)  # add the loop to the experiment
        thisACTIONTRIAL = ACTIONTRIAL.trialList[0]  # so we can initialise stimuli with some values
        # abbreviate parameter names if possible (e.g. rgb = thisACTIONTRIAL.rgb)
        if thisACTIONTRIAL != None:
            for paramName in thisACTIONTRIAL:
                exec('{} = thisACTIONTRIAL[paramName]'.format(paramName))
        
        for thisACTIONTRIAL in ACTIONTRIAL:
            currentLoop = ACTIONTRIAL
            # abbreviate parameter names if possible (e.g. rgb = thisACTIONTRIAL.rgb)
            if thisACTIONTRIAL != None:
                for paramName in thisACTIONTRIAL:
                    exec('{} = thisACTIONTRIAL[paramName]'.format(paramName))
            
            # --- Prepare to start Routine "ActionTrial" ---
            continueRoutine = True
            routineForceEnded = False
            # update component parameters for each repeat
            # setup some python lists for storing info about the mouse_action_trial
            mouse_action_trial.x = []
            mouse_action_trial.y = []
            mouse_action_trial.leftButton = []
            mouse_action_trial.midButton = []
            mouse_action_trial.rightButton = []
            mouse_action_trial.time = []
            gotValidClick = False  # until a click is received
            mouse_action_trial.mouseClock.reset()
            experimenter_controls.keys = []
            experimenter_controls.rt = []
            _experimenter_controls_allKeys = []
            # Run 'Begin Routine' code from track_mouse
            #-----#
            # Set mouse visibility.
            if show_mouse == True:
                win.mouseVisible = True
            elif show_mouse == False:
                win.mouseVisible = False
            
            #-----#
            # Retrieve cursor position of current trial.
            init_pos = str(this_block_cursor_seq[trial_id])
            init_pos_x, init_pos_y, init_deg = transform_mouse(
                targets_coords[init_pos][0],
                targets_coords[init_pos][1],
                system,
                room,
                wrist_distance,
            )
            
            # Ignore movements made between trials.
            mouse_action_trial.setPos(targets_coords[init_pos])
            
            # Retrieve target position for current trial.
            target_pos = str(this_block_target_seq[trial_id])
            target_x, target_y = targets_coords[target_pos]
            target_deg = targets_degrees[target_pos]
            
            if debug_mode == True:
                print("Trial's start position: {}, {}, {}".format(init_pos_x, init_pos_y, init_deg))
                print("Trial's target position: {}, {}, {}".format(target_x, target_y, target_deg))
                print("Trial timings (c/r/t/rb): {}, {}, {}, {}".format(concentration_period, reaction_period, task_period, rebound_period))
            
            #-----#
            # Reset variable that saves the state of trial with
            # unacceptable reaction time.
            too_slow = False
            too_fast = False
            
            # Save mouse status in the trial, but only check
            # it following the concntration period.
            mouse_is_not_moving_r = np.array([])
            mouse_is_not_moving_t = np.array([])
            mouse_is_not_moving_count_r = 1.0
            mouse_is_not_moving_count_t = 1.0
            
            #-----#
            # Save mouse angle at every frame.
            mouse_degrees = np.array([])
            frame_times = np.array([])
            # keep track of which components have finished
            ActionTrialComponents = [fixation_cross_action_trial, arc_action_trial, mouse_action_trial, cursor_action_trial, target_action_trial, target_action_trial_sync, target_action_trial_go, cursor_action_trial_online, experimenter_controls]
            for thisComponent in ActionTrialComponents:
                thisComponent.tStart = None
                thisComponent.tStop = None
                thisComponent.tStartRefresh = None
                thisComponent.tStopRefresh = None
                if hasattr(thisComponent, 'status'):
                    thisComponent.status = NOT_STARTED
            # reset timers
            t = 0
            _timeToFirstFrame = win.getFutureFlipTime(clock="now")
            frameN = -1
            
            # --- Run Routine "ActionTrial" ---
            while continueRoutine:
                # get current time
                t = routineTimer.getTime()
                tThisFlip = win.getFutureFlipTime(clock=routineTimer)
                tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                # update/draw components on each frame
                
                # *fixation_cross_action_trial* updates
                if fixation_cross_action_trial.status == NOT_STARTED and n_action_trial == 1.0:
                    # keep track of start time/frame for later
                    fixation_cross_action_trial.frameNStart = frameN  # exact frame index
                    fixation_cross_action_trial.tStart = t  # local t and not account for scr refresh
                    fixation_cross_action_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(fixation_cross_action_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'fixation_cross_action_trial.started')
                    fixation_cross_action_trial.setAutoDraw(True)
                if fixation_cross_action_trial.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > fixation_cross_action_trial.tStartRefresh + task_period + avoid_flicker-frameTolerance:
                        # keep track of stop time/frame for later
                        fixation_cross_action_trial.tStop = t  # not accounting for scr refresh
                        fixation_cross_action_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'fixation_cross_action_trial.stopped')
                        fixation_cross_action_trial.setAutoDraw(False)
                
                # *arc_action_trial* updates
                if arc_action_trial.status == NOT_STARTED and n_action_trial == 1.0:
                    # keep track of start time/frame for later
                    arc_action_trial.frameNStart = frameN  # exact frame index
                    arc_action_trial.tStart = t  # local t and not account for scr refresh
                    arc_action_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(arc_action_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'arc_action_trial.started')
                    arc_action_trial.setAutoDraw(True)
                if arc_action_trial.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > arc_action_trial.tStartRefresh + task_period + avoid_flicker-frameTolerance:
                        # keep track of stop time/frame for later
                        arc_action_trial.tStop = t  # not accounting for scr refresh
                        arc_action_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'arc_action_trial.stopped')
                        arc_action_trial.setAutoDraw(False)
                # *mouse_action_trial* updates
                if mouse_action_trial.status == NOT_STARTED and n_action_trial == 1.0:
                    # keep track of start time/frame for later
                    mouse_action_trial.frameNStart = frameN  # exact frame index
                    mouse_action_trial.tStart = t  # local t and not account for scr refresh
                    mouse_action_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(mouse_action_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'mouse_action_trial.started')
                    mouse_action_trial.status = STARTED
                    prevButtonState = mouse_action_trial.getPressed()  # if button is down already this ISN'T a new click
                if mouse_action_trial.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > mouse_action_trial.tStartRefresh + task_period-frameTolerance:
                        # keep track of stop time/frame for later
                        mouse_action_trial.tStop = t  # not accounting for scr refresh
                        mouse_action_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'mouse_action_trial.stopped')
                        mouse_action_trial.status = FINISHED
                if mouse_action_trial.status == STARTED:  # only update if started and not finished!
                    x, y = mouse_action_trial.getPos()
                    mouse_action_trial.x.append(x)
                    mouse_action_trial.y.append(y)
                    buttons = mouse_action_trial.getPressed()
                    mouse_action_trial.leftButton.append(buttons[0])
                    mouse_action_trial.midButton.append(buttons[1])
                    mouse_action_trial.rightButton.append(buttons[2])
                    mouse_action_trial.time.append(mouse_action_trial.mouseClock.getTime())
                
                # *cursor_action_trial* updates
                if cursor_action_trial.status == NOT_STARTED and n_action_trial == 1.0:
                    # keep track of start time/frame for later
                    cursor_action_trial.frameNStart = frameN  # exact frame index
                    cursor_action_trial.tStart = t  # local t and not account for scr refresh
                    cursor_action_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(cursor_action_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'cursor_action_trial.started')
                    cursor_action_trial.setAutoDraw(True)
                if cursor_action_trial.status == STARTED:
                    if bool((t > concentration_period and mouse_is_not_moving_count_r == 0) or (t > reaction_period)):
                        # keep track of stop time/frame for later
                        cursor_action_trial.tStop = t  # not accounting for scr refresh
                        cursor_action_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'cursor_action_trial.stopped')
                        cursor_action_trial.setAutoDraw(False)
                
                # *target_action_trial* updates
                if target_action_trial.status == NOT_STARTED and (n_action_trial == 1.0) and (cursor_target_sync == False) and (t > concentration_period):
                    # keep track of start time/frame for later
                    target_action_trial.frameNStart = frameN  # exact frame index
                    target_action_trial.tStart = t  # local t and not account for scr refresh
                    target_action_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(target_action_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_action_trial.started')
                    target_action_trial.setAutoDraw(True)
                if target_action_trial.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > target_action_trial.tStartRefresh + task_period + avoid_flicker-frameTolerance:
                        # keep track of stop time/frame for later
                        target_action_trial.tStop = t  # not accounting for scr refresh
                        target_action_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'target_action_trial.stopped')
                        target_action_trial.setAutoDraw(False)
                
                # *target_action_trial_sync* updates
                if target_action_trial_sync.status == NOT_STARTED and (n_action_trial == 1.0) and (cursor_target_sync == True):
                    # keep track of start time/frame for later
                    target_action_trial_sync.frameNStart = frameN  # exact frame index
                    target_action_trial_sync.tStart = t  # local t and not account for scr refresh
                    target_action_trial_sync.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(target_action_trial_sync, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_action_trial_sync.started')
                    target_action_trial_sync.setAutoDraw(True)
                if target_action_trial_sync.status == STARTED:
                    if bool((t > concentration_period)):
                        # keep track of stop time/frame for later
                        target_action_trial_sync.tStop = t  # not accounting for scr refresh
                        target_action_trial_sync.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'target_action_trial_sync.stopped')
                        target_action_trial_sync.setAutoDraw(False)
                
                # *target_action_trial_go* updates
                if target_action_trial_go.status == NOT_STARTED and (n_action_trial == 1.0) and (cursor_target_sync == True) and (t >= concentration_period):
                    # keep track of start time/frame for later
                    target_action_trial_go.frameNStart = frameN  # exact frame index
                    target_action_trial_go.tStart = t  # local t and not account for scr refresh
                    target_action_trial_go.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(target_action_trial_go, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_action_trial_go.started')
                    target_action_trial_go.setAutoDraw(True)
                if target_action_trial_go.status == STARTED:
                    if bool((t >= task_period)):
                        # keep track of stop time/frame for later
                        target_action_trial_go.tStop = t  # not accounting for scr refresh
                        target_action_trial_go.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'target_action_trial_go.stopped')
                        target_action_trial_go.setAutoDraw(False)
                
                # *cursor_action_trial_online* updates
                if cursor_action_trial_online.status == NOT_STARTED and (t > concentration_period and mouse_is_not_moving_count_r == 0) or (t > reaction_period):
                    # keep track of start time/frame for later
                    cursor_action_trial_online.frameNStart = frameN  # exact frame index
                    cursor_action_trial_online.tStart = t  # local t and not account for scr refresh
                    cursor_action_trial_online.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(cursor_action_trial_online, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'cursor_action_trial_online.started')
                    cursor_action_trial_online.setAutoDraw(True)
                
                # *experimenter_controls* updates
                waitOnFlip = False
                if experimenter_controls.status == NOT_STARTED and n_action_trial == 1.0:
                    # keep track of start time/frame for later
                    experimenter_controls.frameNStart = frameN  # exact frame index
                    experimenter_controls.tStart = t  # local t and not account for scr refresh
                    experimenter_controls.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(experimenter_controls, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'experimenter_controls.started')
                    experimenter_controls.status = STARTED
                    # keyboard checking is just starting
                    waitOnFlip = True
                    win.callOnFlip(experimenter_controls.clock.reset)  # t=0 on next screen flip
                    win.callOnFlip(experimenter_controls.clearEvents, eventType='keyboard')  # clear events on next screen flip
                if experimenter_controls.status == STARTED and not waitOnFlip:
                    theseKeys = experimenter_controls.getKeys(keyList=['f','s'], waitRelease=False)
                    _experimenter_controls_allKeys.extend(theseKeys)
                    if len(_experimenter_controls_allKeys):
                        experimenter_controls.keys = _experimenter_controls_allKeys[-1].name  # just the last key pressed
                        experimenter_controls.rt = _experimenter_controls_allKeys[-1].rt
                        # a response ends the routine
                        continueRoutine = False
                # Run 'Each Frame' code from track_mouse
                #-----#
                # Update cursor position.
                # Mouse control has been passed to the ACTION TRIAL ADJ routine !!!
                # (because mouse_adj_trial is spawned 2nd ???)
                # Yet the mouseMoved() check has to be made for the current window !!!
                # (using mouse_action_trial)
                mouse_x, mouse_y = mouse_adj_trial.getPos()
                
                #-----#
                # Apply necessary tranformations in the recorded coordinates.
                cursor_x_tr, cursor_y_tr, cursor_x_angle = transform_mouse(
                    mouse_x,
                    mouse_y,
                    system,
                    room,
                    wrist_distance,
                )
                
                #-----#
                # Forcefully end the trial if the reaction time
                # is very fasrt, very slow, if the cursor has
                # reached the target before the task
                # duration limit has exprired, or if it has
                # stopped moving for a while.
                
                # Check for cases of early trial termination.
                if t < concentration_period:
                
                    # The trial is aborted if the mouse is moved
                    # before the target appears on screen.
                    if  (cursor_x_angle < init_deg - mouse_conf) or\
                        (cursor_x_angle > init_deg + mouse_conf):
                        
                        # Trigger "TOO FAST" message in "ActionTrialAdj" Routine.
                        too_fast = True
                        continueRoutine = False
                        if debug_mode == True:
                            print("Stop reason: TOO FAST")
                
                elif (t >= concentration_period) and (t < reaction_period):
                
                    # Store last mouse state.
                    mouse_degrees = np.append(mouse_degrees, cursor_x_angle[-1])
                    frame_times = np.append(frame_times, t - concentration_period)
                    
                    # Provide online feedback of cursor position.
                    cursor_action_trial_online.setPos([cursor_x_tr[-1], cursor_y_tr[-1]])
                    cursor_action_trial_online.setOri(cursor_x_angle[-1])
                    
                    if action_block_counter != 0:
                        cursor_action_trial_online.setOpacity(0.0)
                
                    # Monitor consecutive frames of mouse inactivity.
                    mmv_r = str(mouse_action_trial.mouseMoved())
                    if mmv_r == "True":
                        mouse_is_not_moving_r = np.append(mouse_is_not_moving_r, 0)
                    elif mmv_r == "False":
                       mouse_is_not_moving_r = np.append(mouse_is_not_moving_r, 1)
                    
                    #mouse_is_not_moving_count_r = np.sum(np.flip(mouse_is_not_moving_r)[:not_moved_id])
                    mouse_is_not_moving_count_r = mouse_is_not_moving_r[-1]
                
                elif (t >= reaction_period) and (t < task_period):
                
                    # Store last mouse state.
                    mouse_degrees = np.append(mouse_degrees, cursor_x_angle[-1])
                    frame_times = np.append(frame_times, t - concentration_period)
                    
                    # Provide online feedback of cursor position.
                    cursor_action_trial_online.setPos([cursor_x_tr[-1], cursor_y_tr[-1]])
                    cursor_action_trial_online.setOri(cursor_x_angle[-1])
                    
                    if action_block_counter != 0:
                        cursor_action_trial_online.setOpacity(0.0)
                    
                    # Monitor consecutive frames of mouse inactivity.
                    mmv_t = str(mouse_action_trial.mouseMoved())
                    if mmv_t == "True":
                        mouse_is_not_moving_t = np.append(mouse_is_not_moving_t, 0)
                    elif mmv_t == "False":
                       mouse_is_not_moving_t = np.append(mouse_is_not_moving_t, 1)
                    
                    mouse_is_not_moving_count_t = np.sum(np.flip(mouse_is_not_moving_t)[:not_moved_id])
                
                    # End trial if the mouse hasn't moved within
                    # the given reaction period.
                    if  (cursor_x_angle > init_deg - mouse_conf) and\
                        (cursor_x_angle < init_deg + mouse_conf):
                        
                        # Trigger "TOO SLOW" message in "ActionTrialAdj" Routine.
                        too_slow = True
                        continueRoutine = False
                        if debug_mode == True:
                            print("Stop reason: TOO SLOW")
                
                    # End trial if the mouse has reached the target
                    # before the trial max duration.
                    #elif  (cursor_x_angle > target_deg - mouse_conf) and\
                    #    (cursor_x_angle < target_deg + mouse_conf):
                        
                    #    continueRoutine = False
                    #    if debug_mode == True:
                    #        print("Stop reason: TARGET REACHED")
                    
                    # End trial if the mouse has stopped moving
                    # for a certain amount of time, before reaching
                    # the trial max duration.
                    elif (mouse_is_not_moving_count_t >= not_moved_id):
                    
                        continueRoutine = False
                        if debug_mode == True:
                            print("Stop reason: MOUSE NOT MOVING FOR {}s".format(not_moving_dur))
                
                # End trial if mouse keeps moving.
                elif t >= task_period:
                        
                    continueRoutine = False
                    if debug_mode == True:
                        print("Stop reason: REACHED MAX TRIAL DURATION")
                
                # check for quit (typically the Esc key)
                if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                    core.quit()
                
                # check if all components have finished
                if not continueRoutine:  # a component has requested a forced-end of Routine
                    routineForceEnded = True
                    break
                continueRoutine = False  # will revert to True if at least one component still running
                for thisComponent in ActionTrialComponents:
                    if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                        continueRoutine = True
                        break  # at least one component has not yet finished
                
                # refresh the screen
                if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                    win.flip()
            
            # --- Ending Routine "ActionTrial" ---
            for thisComponent in ActionTrialComponents:
                if hasattr(thisComponent, "setAutoDraw"):
                    thisComponent.setAutoDraw(False)
            # store data for ACTIONTRIAL (TrialHandler)
            ACTIONTRIAL.addData('mouse_action_trial.x', mouse_action_trial.x)
            ACTIONTRIAL.addData('mouse_action_trial.y', mouse_action_trial.y)
            ACTIONTRIAL.addData('mouse_action_trial.leftButton', mouse_action_trial.leftButton)
            ACTIONTRIAL.addData('mouse_action_trial.midButton', mouse_action_trial.midButton)
            ACTIONTRIAL.addData('mouse_action_trial.rightButton', mouse_action_trial.rightButton)
            ACTIONTRIAL.addData('mouse_action_trial.time', mouse_action_trial.time)
            # check responses
            if experimenter_controls.keys in ['', [], None]:  # No response was made
                experimenter_controls.keys = None
            ACTIONTRIAL.addData('experimenter_controls.keys',experimenter_controls.keys)
            if experimenter_controls.keys != None:  # we had a response
                ACTIONTRIAL.addData('experimenter_controls.rt', experimenter_controls.rt)
            # Run 'End Routine' code from track_mouse
            #-----#
            # Debug message.
            if debug_mode == True:
                print("Frames' threshold for mouse inactivity: {}".format(not_moved_id))
                print("Number of consecutive frames with mouse not moving: {} -> {}, {} -> {}".format(
                        mouse_is_not_moving_r,
                        mouse_is_not_moving_count_r,
                        mouse_is_not_moving_t,
                        mouse_is_not_moving_count_t
                  )
                )
                print("Trial duration: {}s".format(t))
            
            #-----#
            # Force 'too_slow' or 'too_fast' as the experimenter.
            keys = event.getKeys()
            if len(keys) > 0:
                if keys[-1] == "f":
                    too_fast = True
                elif keys[-1] == "s":
                    too_slow = True
            
            #-----#
            # Store the time needed to complete each trial.
            if too_slow == False and too_fast == False:
                
                valid_move_trials[trial_id] = target_pos
                valid_move_timings[trial_id] = t - concentration_period
            
                all_mouse_degrees[str(trial_id)] = mouse_degrees
                all_frame_times[str(trial_id)] = frame_times
            
                if debug_mode == True:
                    print("Valid {} trial duration: {}s".format(target_pos, t))
            # the Routine "ActionTrial" was not non-slip safe, so reset the non-slip timer
            routineTimer.reset()
            
            # --- Prepare to start Routine "ActionRebound" ---
            continueRoutine = True
            routineForceEnded = False
            # update component parameters for each repeat
            # Run 'Begin Routine' code from rebound
            #-----#
            # Set mouse visibility.
            if show_mouse == True:
                win.mouseVisible = True
            elif show_mouse == False:
                win.mouseVisible = False
            
            #-----#
            #continueRoutine = False
            # Only happen following a valid ACTION trial.
            if (too_slow == True or too_fast == True):
                
                continueRoutine = False
            
            #-----#
            # Second training block provides short feedback
            # of target's landing position.
            if action_block_counter == 1:
                
                mouse_x, mouse_y = mouse_adj_trial.getPos()
                cursor_x_tr, cursor_y_tr, cursor_x_angle = transform_mouse(
                    mouse_x,
                    mouse_y,
                    system,
                    room,
                    wrist_distance,
                )
                
                cursor_rebound.setPos([cursor_x_tr[-1], cursor_y_tr[-1]])
                cursor_rebound.setOri(cursor_x_angle[-1])
                
                cursor_rebound.setOpacity(1.0)
                target_rebound.setOpacity(1.0)
            
            else:
                cursor_rebound.setOpacity(0.0)
                target_rebound.setOpacity(0.0)
            # keep track of which components have finished
            ActionReboundComponents = [fixation_cross_action_rebound, arc_action_rebound, target_rebound, cursor_rebound]
            for thisComponent in ActionReboundComponents:
                thisComponent.tStart = None
                thisComponent.tStop = None
                thisComponent.tStartRefresh = None
                thisComponent.tStopRefresh = None
                if hasattr(thisComponent, 'status'):
                    thisComponent.status = NOT_STARTED
            # reset timers
            t = 0
            _timeToFirstFrame = win.getFutureFlipTime(clock="now")
            frameN = -1
            
            # --- Run Routine "ActionRebound" ---
            while continueRoutine:
                # get current time
                t = routineTimer.getTime()
                tThisFlip = win.getFutureFlipTime(clock=routineTimer)
                tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                # update/draw components on each frame
                
                # *fixation_cross_action_rebound* updates
                if fixation_cross_action_rebound.status == NOT_STARTED and (too_slow == False and too_fast == False):
                    # keep track of start time/frame for later
                    fixation_cross_action_rebound.frameNStart = frameN  # exact frame index
                    fixation_cross_action_rebound.tStart = t  # local t and not account for scr refresh
                    fixation_cross_action_rebound.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(fixation_cross_action_rebound, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'fixation_cross_action_rebound.started')
                    fixation_cross_action_rebound.setAutoDraw(True)
                if fixation_cross_action_rebound.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > fixation_cross_action_rebound.tStartRefresh + rebound_period + avoid_flicker-frameTolerance:
                        # keep track of stop time/frame for later
                        fixation_cross_action_rebound.tStop = t  # not accounting for scr refresh
                        fixation_cross_action_rebound.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'fixation_cross_action_rebound.stopped')
                        fixation_cross_action_rebound.setAutoDraw(False)
                
                # *arc_action_rebound* updates
                if arc_action_rebound.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                    # keep track of start time/frame for later
                    arc_action_rebound.frameNStart = frameN  # exact frame index
                    arc_action_rebound.tStart = t  # local t and not account for scr refresh
                    arc_action_rebound.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(arc_action_rebound, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'arc_action_rebound.started')
                    arc_action_rebound.setAutoDraw(True)
                if arc_action_rebound.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > arc_action_rebound.tStartRefresh + rebound_period + avoid_flicker-frameTolerance:
                        # keep track of stop time/frame for later
                        arc_action_rebound.tStop = t  # not accounting for scr refresh
                        arc_action_rebound.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'arc_action_rebound.stopped')
                        arc_action_rebound.setAutoDraw(False)
                
                # *target_rebound* updates
                if target_rebound.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                    # keep track of start time/frame for later
                    target_rebound.frameNStart = frameN  # exact frame index
                    target_rebound.tStart = t  # local t and not account for scr refresh
                    target_rebound.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(target_rebound, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_rebound.started')
                    target_rebound.setAutoDraw(True)
                if target_rebound.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > target_rebound.tStartRefresh + 0.5-frameTolerance:
                        # keep track of stop time/frame for later
                        target_rebound.tStop = t  # not accounting for scr refresh
                        target_rebound.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'target_rebound.stopped')
                        target_rebound.setAutoDraw(False)
                
                # *cursor_rebound* updates
                if cursor_rebound.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                    # keep track of start time/frame for later
                    cursor_rebound.frameNStart = frameN  # exact frame index
                    cursor_rebound.tStart = t  # local t and not account for scr refresh
                    cursor_rebound.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(cursor_rebound, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'cursor_rebound.started')
                    cursor_rebound.setAutoDraw(True)
                if cursor_rebound.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > cursor_rebound.tStartRefresh + 0.5-frameTolerance:
                        # keep track of stop time/frame for later
                        cursor_rebound.tStop = t  # not accounting for scr refresh
                        cursor_rebound.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'cursor_rebound.stopped')
                        cursor_rebound.setAutoDraw(False)
                # Run 'Each Frame' code from rebound
                #-----#
                # Update cursor position.
                #mouse_x, mouse_y = mouse_adj_trial.getPos()
                
                # check for quit (typically the Esc key)
                if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                    core.quit()
                
                # check if all components have finished
                if not continueRoutine:  # a component has requested a forced-end of Routine
                    routineForceEnded = True
                    break
                continueRoutine = False  # will revert to True if at least one component still running
                for thisComponent in ActionReboundComponents:
                    if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                        continueRoutine = True
                        break  # at least one component has not yet finished
                
                # refresh the screen
                if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                    win.flip()
            
            # --- Ending Routine "ActionRebound" ---
            for thisComponent in ActionReboundComponents:
                if hasattr(thisComponent, "setAutoDraw"):
                    thisComponent.setAutoDraw(False)
            # the Routine "ActionRebound" was not non-slip safe, so reset the non-slip timer
            routineTimer.reset()
            
            # --- Prepare to start Routine "ActionTrialAdj" ---
            continueRoutine = True
            routineForceEnded = False
            # update component parameters for each repeat
            # setup some python lists for storing info about the mouse_adj_trial
            mouse_adj_trial.x = []
            mouse_adj_trial.y = []
            mouse_adj_trial.leftButton = []
            mouse_adj_trial.midButton = []
            mouse_adj_trial.rightButton = []
            mouse_adj_trial.time = []
            gotValidClick = False  # until a click is received
            mouse_adj_trial.mouseClock.reset()
            key_resp_adj.keys = []
            key_resp_adj.rt = []
            _key_resp_adj_allKeys = []
            # Run 'Begin Routine' code from adj_cursor_position
            #-----#
            # Set mouse visibility.
            if show_mouse == True:
                win.mouseVisible = True
            elif show_mouse == False:
                win.mouseVisible = False
            
            #-----#
            if locale == "en":
                slow_msg = "TOO SLOW"
                fast_msg = "TOO FAST"
                adjust_msg = "Bring your hand to neutral position and press any button to continue."
            elif locale == "fr":
                slow_msg = "TROP LENT"
                fast_msg = "TROP VITE"
                adjust_msg = "Ramenez votre main en position neutre et appuyez sur n'importe quel bouton pour continuer."
            
            slow_trial_info.text = slow_msg
            fast_trial_info.text = fast_msg
            text_adjust.text = adjust_msg
            
            #-----#
            # Retrieve target position for completed trial
            # and set initial position of cursor.
            
            # Cursor's starting position.
            cursor_sx, cursor_sy = mouse_adj_trial.getPos()
            
            # Cursor opacity is re-set to 1.0
            cursor_adj_trial.setOpacity(1.0)
            
            if action_block_counter != 0:
                
                # Completed trial target position.
                target_pos = str(this_block_target_seq[trial_id])
                target_x, target_y = targets_coords[target_pos]
                target_angle = targets_degrees[target_pos]
            
                cursor_adj_trial.setPos([target_x, target_y])
                cursor_adj_trial.setOri(target_angle)
            
            #-----#
            # Target position for trial. We just make sure
            # to bring the mouse to the neutral position.
            if bring_to_neutral_pos == "cursor":
                target_adj_x, target_adj_y = targets_coords["0"]
                target_adj_deg = targets_degrees["0"]
            
            #-----#
            if debug_mode == True:
                print("Last mouse state: {}".format([mouse_x, mouse_y]))
                print("Was the trial 'slow'? : {}".format(too_slow))
                print("Was the trial 'fast'? : {}".format(too_fast))
            # keep track of which components have finished
            ActionTrialAdjComponents = [slow_trial_info, fast_trial_info, text_adjust, fixation_cross_adj_trial, arc_adj_trial, mouse_adj_trial, target_adj_trial, cursor_adj_trial, key_resp_adj]
            for thisComponent in ActionTrialAdjComponents:
                thisComponent.tStart = None
                thisComponent.tStop = None
                thisComponent.tStartRefresh = None
                thisComponent.tStopRefresh = None
                if hasattr(thisComponent, 'status'):
                    thisComponent.status = NOT_STARTED
            # reset timers
            t = 0
            _timeToFirstFrame = win.getFutureFlipTime(clock="now")
            frameN = -1
            
            # --- Run Routine "ActionTrialAdj" ---
            while continueRoutine:
                # get current time
                t = routineTimer.getTime()
                tThisFlip = win.getFutureFlipTime(clock=routineTimer)
                tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                # update/draw components on each frame
                
                # *slow_trial_info* updates
                if slow_trial_info.status == NOT_STARTED and too_slow == True:
                    # keep track of start time/frame for later
                    slow_trial_info.frameNStart = frameN  # exact frame index
                    slow_trial_info.tStart = t  # local t and not account for scr refresh
                    slow_trial_info.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(slow_trial_info, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'slow_trial_info.started')
                    slow_trial_info.setAutoDraw(True)
                if slow_trial_info.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > slow_trial_info.tStartRefresh + too_slow_period-frameTolerance:
                        # keep track of stop time/frame for later
                        slow_trial_info.tStop = t  # not accounting for scr refresh
                        slow_trial_info.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'slow_trial_info.stopped')
                        slow_trial_info.setAutoDraw(False)
                
                # *fast_trial_info* updates
                if fast_trial_info.status == NOT_STARTED and too_fast == True:
                    # keep track of start time/frame for later
                    fast_trial_info.frameNStart = frameN  # exact frame index
                    fast_trial_info.tStart = t  # local t and not account for scr refresh
                    fast_trial_info.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(fast_trial_info, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'fast_trial_info.started')
                    fast_trial_info.setAutoDraw(True)
                if fast_trial_info.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > fast_trial_info.tStartRefresh + too_fast_period-frameTolerance:
                        # keep track of stop time/frame for later
                        fast_trial_info.tStop = t  # not accounting for scr refresh
                        fast_trial_info.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'fast_trial_info.stopped')
                        fast_trial_info.setAutoDraw(False)
                
                # *text_adjust* updates
                if text_adjust.status == NOT_STARTED and (too_slow == False and too_fast == False) or (too_slow == True and t > too_slow_period) or (too_fast == True and t > too_fast_period):
                    # keep track of start time/frame for later
                    text_adjust.frameNStart = frameN  # exact frame index
                    text_adjust.tStart = t  # local t and not account for scr refresh
                    text_adjust.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(text_adjust, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'text_adjust.started')
                    text_adjust.setAutoDraw(True)
                
                # *fixation_cross_adj_trial* updates
                if fixation_cross_adj_trial.status == NOT_STARTED and (bring_to_neutral_pos != "hand") and ((too_slow == False and too_fast == False) or (too_slow == True and t > too_slow_period) or (too_fast == True and t > too_fast_period)):
                    # keep track of start time/frame for later
                    fixation_cross_adj_trial.frameNStart = frameN  # exact frame index
                    fixation_cross_adj_trial.tStart = t  # local t and not account for scr refresh
                    fixation_cross_adj_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(fixation_cross_adj_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'fixation_cross_adj_trial.started')
                    fixation_cross_adj_trial.setAutoDraw(True)
                
                # *arc_adj_trial* updates
                if arc_adj_trial.status == NOT_STARTED and (too_slow == False and too_fast == False) or (too_slow == True and t > too_slow_period) or (too_fast == True and t > too_fast_period):
                    # keep track of start time/frame for later
                    arc_adj_trial.frameNStart = frameN  # exact frame index
                    arc_adj_trial.tStart = t  # local t and not account for scr refresh
                    arc_adj_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(arc_adj_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'arc_adj_trial.started')
                    arc_adj_trial.setAutoDraw(True)
                # *mouse_adj_trial* updates
                if mouse_adj_trial.status == NOT_STARTED and (too_slow == False and too_fast == False) or (too_slow == True and t > too_slow_period) or (too_fast == True and t > too_fast_period):
                    # keep track of start time/frame for later
                    mouse_adj_trial.frameNStart = frameN  # exact frame index
                    mouse_adj_trial.tStart = t  # local t and not account for scr refresh
                    mouse_adj_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(mouse_adj_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'mouse_adj_trial.started')
                    mouse_adj_trial.status = STARTED
                    prevButtonState = mouse_adj_trial.getPressed()  # if button is down already this ISN'T a new click
                if mouse_adj_trial.status == STARTED:  # only update if started and not finished!
                    x, y = mouse_adj_trial.getPos()
                    mouse_adj_trial.x.append(x)
                    mouse_adj_trial.y.append(y)
                    buttons = mouse_adj_trial.getPressed()
                    mouse_adj_trial.leftButton.append(buttons[0])
                    mouse_adj_trial.midButton.append(buttons[1])
                    mouse_adj_trial.rightButton.append(buttons[2])
                    mouse_adj_trial.time.append(mouse_adj_trial.mouseClock.getTime())
                
                # *target_adj_trial* updates
                if target_adj_trial.status == NOT_STARTED and (too_slow == False and too_fast == False) or (too_slow == True and t > too_slow_period) or (too_fast == True and t > too_slow_period):
                    # keep track of start time/frame for later
                    target_adj_trial.frameNStart = frameN  # exact frame index
                    target_adj_trial.tStart = t  # local t and not account for scr refresh
                    target_adj_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(target_adj_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_adj_trial.started')
                    target_adj_trial.setAutoDraw(True)
                
                # *cursor_adj_trial* updates
                if cursor_adj_trial.status == NOT_STARTED and (too_slow == False and too_fast == False) or (too_slow == True and t > too_slow_period) or (too_fast == True and t > too_fast_period):
                    # keep track of start time/frame for later
                    cursor_adj_trial.frameNStart = frameN  # exact frame index
                    cursor_adj_trial.tStart = t  # local t and not account for scr refresh
                    cursor_adj_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(cursor_adj_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'cursor_adj_trial.started')
                    cursor_adj_trial.setAutoDraw(True)
                
                # *key_resp_adj* updates
                waitOnFlip = False
                if key_resp_adj.status == NOT_STARTED and (bring_to_neutral_pos == "hand") and ((too_slow == False and too_fast == False) or (too_slow == True and t > too_slow_period) or (too_fast == True and t > too_fast_period)):
                    # keep track of start time/frame for later
                    key_resp_adj.frameNStart = frameN  # exact frame index
                    key_resp_adj.tStart = t  # local t and not account for scr refresh
                    key_resp_adj.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(key_resp_adj, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'key_resp_adj.started')
                    key_resp_adj.status = STARTED
                    # AllowedKeys looks like a variable named `valid_keys`
                    if not type(valid_keys) in [list, tuple, np.ndarray]:
                        if not isinstance(valid_keys, str):
                            logging.error('AllowedKeys variable `valid_keys` is not string- or list-like.')
                            core.quit()
                        elif not ',' in valid_keys:
                            valid_keys = (valid_keys,)
                        else:
                            valid_keys = eval(valid_keys)
                    # keyboard checking is just starting
                    waitOnFlip = True
                    win.callOnFlip(key_resp_adj.clock.reset)  # t=0 on next screen flip
                    win.callOnFlip(key_resp_adj.clearEvents, eventType='keyboard')  # clear events on next screen flip
                if key_resp_adj.status == STARTED and not waitOnFlip:
                    theseKeys = key_resp_adj.getKeys(keyList=list(valid_keys), waitRelease=False)
                    _key_resp_adj_allKeys.extend(theseKeys)
                    if len(_key_resp_adj_allKeys):
                        key_resp_adj.keys = _key_resp_adj_allKeys[-1].name  # just the last key pressed
                        key_resp_adj.rt = _key_resp_adj_allKeys[-1].rt
                        # a response ends the routine
                        continueRoutine = False
                # Run 'Each Frame' code from adj_cursor_position
                #-----#
                # Update cursor position.
                mouse_x, mouse_y = mouse_adj_trial.getPos()
                
                #-----#
                # Apply necessary tranformations in the recorded coordinates.
                if action_block_counter == 0:
                    cursor_x_tr, cursor_y_tr, cursor_x_angle = transform_mouse(
                        mouse_x,
                        mouse_y,
                        system,
                        room,
                        wrist_distance,
                    )
                    
                    cursor_adj_trial.setPos([cursor_x_tr[-1], cursor_y_tr[-1]])
                    cursor_adj_trial.setOri(cursor_x_angle[-1])
                
                #elif action_block_counter != 0:
                #    
                cursor_adj_trial.setOpacity(0.0)
                
                # End adjustment when the cursor has moved
                # to the target.
                # If the cursor has not moved at all from the
                # starting position the adjustment will end
                # before drawing any components (which is
                # the intended behavior when a trial is too
                # slow and the mouse has not moved at all).
                if bring_to_neutral_pos == "cursor" and t > too_slow_period:
                    if  (cursor_x_angle < target_adj_deg - mouse_adj_conf) or\
                        (cursor_x_angle > target_adj_deg + mouse_adj_conf):
                        
                        continueRoutine = False
                
                # check for quit (typically the Esc key)
                if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                    core.quit()
                
                # check if all components have finished
                if not continueRoutine:  # a component has requested a forced-end of Routine
                    routineForceEnded = True
                    break
                continueRoutine = False  # will revert to True if at least one component still running
                for thisComponent in ActionTrialAdjComponents:
                    if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                        continueRoutine = True
                        break  # at least one component has not yet finished
                
                # refresh the screen
                if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                    win.flip()
            
            # --- Ending Routine "ActionTrialAdj" ---
            for thisComponent in ActionTrialAdjComponents:
                if hasattr(thisComponent, "setAutoDraw"):
                    thisComponent.setAutoDraw(False)
            # store data for ACTIONTRIAL (TrialHandler)
            ACTIONTRIAL.addData('mouse_adj_trial.x', mouse_adj_trial.x)
            ACTIONTRIAL.addData('mouse_adj_trial.y', mouse_adj_trial.y)
            ACTIONTRIAL.addData('mouse_adj_trial.leftButton', mouse_adj_trial.leftButton)
            ACTIONTRIAL.addData('mouse_adj_trial.midButton', mouse_adj_trial.midButton)
            ACTIONTRIAL.addData('mouse_adj_trial.rightButton', mouse_adj_trial.rightButton)
            ACTIONTRIAL.addData('mouse_adj_trial.time', mouse_adj_trial.time)
            # check responses
            if key_resp_adj.keys in ['', [], None]:  # No response was made
                key_resp_adj.keys = None
            ACTIONTRIAL.addData('key_resp_adj.keys',key_resp_adj.keys)
            if key_resp_adj.keys != None:  # we had a response
                ACTIONTRIAL.addData('key_resp_adj.rt', key_resp_adj.rt)
            # the Routine "ActionTrialAdj" was not non-slip safe, so reset the non-slip timer
            routineTimer.reset()
            
            # --- Prepare to start Routine "GenericRebound" ---
            continueRoutine = True
            routineForceEnded = False
            # update component parameters for each repeat
            # Run 'Begin Routine' code from rebound_gen
            #-----#
            # Set mouse visibility.
            if show_mouse == True:
                win.mouseVisible = True
            elif show_mouse == False:
                win.mouseVisible = False
            
            #-----#
            if debug_mode == True:
                print("Into Generic Rebound.")
            
            #-----#
            # Only happen following a valid ACTION trial.
            #if (too_slow == False or too_fast == False):
            #    
            #    continueRoutine = False
            # keep track of which components have finished
            GenericReboundComponents = [arc_gen_rebound]
            for thisComponent in GenericReboundComponents:
                thisComponent.tStart = None
                thisComponent.tStop = None
                thisComponent.tStartRefresh = None
                thisComponent.tStopRefresh = None
                if hasattr(thisComponent, 'status'):
                    thisComponent.status = NOT_STARTED
            # reset timers
            t = 0
            _timeToFirstFrame = win.getFutureFlipTime(clock="now")
            frameN = -1
            
            # --- Run Routine "GenericRebound" ---
            while continueRoutine:
                # get current time
                t = routineTimer.getTime()
                tThisFlip = win.getFutureFlipTime(clock=routineTimer)
                tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                # update/draw components on each frame
                
                # *arc_gen_rebound* updates
                if arc_gen_rebound.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                    # keep track of start time/frame for later
                    arc_gen_rebound.frameNStart = frameN  # exact frame index
                    arc_gen_rebound.tStart = t  # local t and not account for scr refresh
                    arc_gen_rebound.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(arc_gen_rebound, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'arc_gen_rebound.started')
                    arc_gen_rebound.setAutoDraw(True)
                if arc_gen_rebound.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > arc_gen_rebound.tStartRefresh + (rebound_period + avoid_flicker)-frameTolerance:
                        # keep track of stop time/frame for later
                        arc_gen_rebound.tStop = t  # not accounting for scr refresh
                        arc_gen_rebound.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'arc_gen_rebound.stopped')
                        arc_gen_rebound.setAutoDraw(False)
                
                # check for quit (typically the Esc key)
                if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                    core.quit()
                
                # check if all components have finished
                if not continueRoutine:  # a component has requested a forced-end of Routine
                    routineForceEnded = True
                    break
                continueRoutine = False  # will revert to True if at least one component still running
                for thisComponent in GenericReboundComponents:
                    if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                        continueRoutine = True
                        break  # at least one component has not yet finished
                
                # refresh the screen
                if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                    win.flip()
            
            # --- Ending Routine "GenericRebound" ---
            for thisComponent in GenericReboundComponents:
                if hasattr(thisComponent, "setAutoDraw"):
                    thisComponent.setAutoDraw(False)
            # the Routine "GenericRebound" was not non-slip safe, so reset the non-slip timer
            routineTimer.reset()
            thisExp.nextEntry()
            
        # completed n_action_trial repeats of 'ACTIONTRIAL'
        
        
        # --- Prepare to start Routine "ActionMetrics" ---
        continueRoutine = True
        routineForceEnded = False
        # update component parameters for each repeat
        # Run 'Begin Routine' code from code
        #-----#
        # Process the data from the action trials in order to
        # train the subjects on proper imagery timings.
        # Essentially, find the average time need to
        # complete an action towards a specific target.
        
        # We need to perform the calculations following
        # the completion of the ACTION trials and before
        # IMAGERY / CATCH.
        if this_block_type == "imagery":
            
            action_metrics_fn00 = "sub-{}-{}_{}-ses-00_block-00_{}.npy".format(
                expInfo['participant'],
                expInfo['initials'],
                "Training",
               "action_metrics",
            )
            action_metrics_fn10 = "sub-{}-{}_{}-ses-00_block-10_{}.npy".format(
                expInfo['participant'],
                expInfo['initials'],
                "Training",
               "action_metrics",
            )
            
            action_degrees_fn00 = "sub-{}-{}_{}-ses-00_block-00_{}.pickle".format(
                expInfo['participant'],
                expInfo['initials'],
                "Training",
               "action_degrees",
            )
            action_degrees_fn10 = "sub-{}-{}_{}-ses-00_block-10_{}.pickle".format(
                expInfo['participant'],
                expInfo['initials'],
                "Training",
               "action_degrees",
            )
            
            # Load saved ACTION trials' metrics.
            action_task_metrics = read_action_tr(
                [action_metrics_fn00, action_metrics_fn10],
                [action_degrees_fn00, action_degrees_fn10],
                catch_offsets,
            ) 
                
            # Retrieve target position for current trial.
            target_pos = str(this_block_target_seq[trial_id])
            
            action_task_metrics_target = action_task_metrics[target_pos]
            
            if debug_mode == True:
                print("Custom CATCH offset values: {}".format(action_task_metrics_target))
        # keep track of which components have finished
        ActionMetricsComponents = []
        for thisComponent in ActionMetricsComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "ActionMetrics" ---
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in ActionMetricsComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "ActionMetrics" ---
        for thisComponent in ActionMetricsComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        # the Routine "ActionMetrics" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        
        # set up handler to look after randomisation of conditions etc
        IMAGERYTRIAL = data.TrialHandler(nReps=n_imag_trial, method='random', 
            extraInfo=expInfo, originPath=-1,
            trialList=[None],
            seed=None, name='IMAGERYTRIAL')
        thisExp.addLoop(IMAGERYTRIAL)  # add the loop to the experiment
        thisIMAGERYTRIAL = IMAGERYTRIAL.trialList[0]  # so we can initialise stimuli with some values
        # abbreviate parameter names if possible (e.g. rgb = thisIMAGERYTRIAL.rgb)
        if thisIMAGERYTRIAL != None:
            for paramName in thisIMAGERYTRIAL:
                exec('{} = thisIMAGERYTRIAL[paramName]'.format(paramName))
        
        for thisIMAGERYTRIAL in IMAGERYTRIAL:
            currentLoop = IMAGERYTRIAL
            # abbreviate parameter names if possible (e.g. rgb = thisIMAGERYTRIAL.rgb)
            if thisIMAGERYTRIAL != None:
                for paramName in thisIMAGERYTRIAL:
                    exec('{} = thisIMAGERYTRIAL[paramName]'.format(paramName))
            
            # --- Prepare to start Routine "ImageryTrial" ---
            continueRoutine = True
            routineForceEnded = False
            # update component parameters for each repeat
            # Run 'Begin Routine' code from dur_adj
            #-----#
            # Set mouse visibility.
            if show_mouse == True:
                win.mouseVisible = True
            elif show_mouse == False:
                win.mouseVisible = False
            
            #-----#
            # Ignore movements made between trials.
            mouse_adj_trial.setPos(targets_coords["0"])
            
            # Get neutral position degrees.
            cursor_imag_deg = targets_degrees["0"]
            
            # Reset mouse state.
            do_not_move = False
            # keep track of which components have finished
            ImageryTrialComponents = [fixation_cross_imag_trial, arc_imag_trial, cursor_imag_trial, target_imag_trial, target_imag_trial_sync, target_imag_trial_go]
            for thisComponent in ImageryTrialComponents:
                thisComponent.tStart = None
                thisComponent.tStop = None
                thisComponent.tStartRefresh = None
                thisComponent.tStopRefresh = None
                if hasattr(thisComponent, 'status'):
                    thisComponent.status = NOT_STARTED
            # reset timers
            t = 0
            _timeToFirstFrame = win.getFutureFlipTime(clock="now")
            frameN = -1
            
            # --- Run Routine "ImageryTrial" ---
            while continueRoutine:
                # get current time
                t = routineTimer.getTime()
                tThisFlip = win.getFutureFlipTime(clock=routineTimer)
                tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                # update/draw components on each frame
                
                # *fixation_cross_imag_trial* updates
                if fixation_cross_imag_trial.status == NOT_STARTED and n_imag_trial == 1.0:
                    # keep track of start time/frame for later
                    fixation_cross_imag_trial.frameNStart = frameN  # exact frame index
                    fixation_cross_imag_trial.tStart = t  # local t and not account for scr refresh
                    fixation_cross_imag_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(fixation_cross_imag_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'fixation_cross_imag_trial.started')
                    fixation_cross_imag_trial.setAutoDraw(True)
                if fixation_cross_imag_trial.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > fixation_cross_imag_trial.tStartRefresh + task_period + avoid_flicker-frameTolerance:
                        # keep track of stop time/frame for later
                        fixation_cross_imag_trial.tStop = t  # not accounting for scr refresh
                        fixation_cross_imag_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'fixation_cross_imag_trial.stopped')
                        fixation_cross_imag_trial.setAutoDraw(False)
                
                # *arc_imag_trial* updates
                if arc_imag_trial.status == NOT_STARTED and n_imag_trial == 1.0:
                    # keep track of start time/frame for later
                    arc_imag_trial.frameNStart = frameN  # exact frame index
                    arc_imag_trial.tStart = t  # local t and not account for scr refresh
                    arc_imag_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(arc_imag_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'arc_imag_trial.started')
                    arc_imag_trial.setAutoDraw(True)
                if arc_imag_trial.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > arc_imag_trial.tStartRefresh + task_period + avoid_flicker-frameTolerance:
                        # keep track of stop time/frame for later
                        arc_imag_trial.tStop = t  # not accounting for scr refresh
                        arc_imag_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'arc_imag_trial.stopped')
                        arc_imag_trial.setAutoDraw(False)
                
                # *cursor_imag_trial* updates
                if cursor_imag_trial.status == NOT_STARTED and n_imag_trial == 1.0:
                    # keep track of start time/frame for later
                    cursor_imag_trial.frameNStart = frameN  # exact frame index
                    cursor_imag_trial.tStart = t  # local t and not account for scr refresh
                    cursor_imag_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(cursor_imag_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'cursor_imag_trial.started')
                    cursor_imag_trial.setAutoDraw(True)
                if cursor_imag_trial.status == STARTED:
                    if bool((t >= concentration_period + avoid_gestalt)):
                        # keep track of stop time/frame for later
                        cursor_imag_trial.tStop = t  # not accounting for scr refresh
                        cursor_imag_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'cursor_imag_trial.stopped')
                        cursor_imag_trial.setAutoDraw(False)
                
                # *target_imag_trial* updates
                if target_imag_trial.status == NOT_STARTED and (n_imag_trial == 1.0) and (cursor_target_sync == False):
                    # keep track of start time/frame for later
                    target_imag_trial.frameNStart = frameN  # exact frame index
                    target_imag_trial.tStart = t  # local t and not account for scr refresh
                    target_imag_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(target_imag_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_imag_trial.started')
                    target_imag_trial.setAutoDraw(True)
                if target_imag_trial.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > target_imag_trial.tStartRefresh + (t >= reaction_period)-frameTolerance:
                        # keep track of stop time/frame for later
                        target_imag_trial.tStop = t  # not accounting for scr refresh
                        target_imag_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'target_imag_trial.stopped')
                        target_imag_trial.setAutoDraw(False)
                
                # *target_imag_trial_sync* updates
                if target_imag_trial_sync.status == NOT_STARTED and (n_imag_trial == 1.0) and (cursor_target_sync == True):
                    # keep track of start time/frame for later
                    target_imag_trial_sync.frameNStart = frameN  # exact frame index
                    target_imag_trial_sync.tStart = t  # local t and not account for scr refresh
                    target_imag_trial_sync.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(target_imag_trial_sync, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_imag_trial_sync.started')
                    target_imag_trial_sync.setAutoDraw(True)
                if target_imag_trial_sync.status == STARTED:
                    if bool((t >= concentration_period)):
                        # keep track of stop time/frame for later
                        target_imag_trial_sync.tStop = t  # not accounting for scr refresh
                        target_imag_trial_sync.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'target_imag_trial_sync.stopped')
                        target_imag_trial_sync.setAutoDraw(False)
                
                # *target_imag_trial_go* updates
                if target_imag_trial_go.status == NOT_STARTED and (n_imag_trial == 1.0) and (cursor_target_sync == True) and (t >= concentration_period):
                    # keep track of start time/frame for later
                    target_imag_trial_go.frameNStart = frameN  # exact frame index
                    target_imag_trial_go.tStart = t  # local t and not account for scr refresh
                    target_imag_trial_go.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(target_imag_trial_go, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_imag_trial_go.started')
                    target_imag_trial_go.setAutoDraw(True)
                if target_imag_trial_go.status == STARTED:
                    if bool((t >= task_period + avoid_flicker)):
                        # keep track of stop time/frame for later
                        target_imag_trial_go.tStop = t  # not accounting for scr refresh
                        target_imag_trial_go.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'target_imag_trial_go.stopped')
                        target_imag_trial_go.setAutoDraw(False)
                # Run 'Each Frame' code from dur_adj
                #-----#
                if t < task_period:
                
                    # Update cursor position.
                    mouse_x, mouse_y = mouse_adj_trial.getPos()
                    cursor_x_tr, cursor_y_tr, cursor_x_angle = transform_mouse(
                        mouse_x,
                        mouse_y,
                        system,
                        room,
                        wrist_distance,
                    )
                
                    # The trial is aborted if the mouse is moved.
                    if  (cursor_x_angle < cursor_imag_deg - mouse_conf) or\
                        (cursor_x_angle > cursor_imag_deg + mouse_conf):
                        
                        # Trigger "DO NOT MOVE" message in "ActionTrialAdj" Routine.
                        do_not_move = True
                        
                        continueRoutine = False
                        if debug_mode == True:
                            print("Stop reason: MOVED DURING IMAGERY")
                
                #-----#
                # End trial in appropriate time.
                elif t >= task_period:
                        
                    continueRoutine = False
                    if debug_mode == True:
                        print("Stop reason: REACHED MAX TRIAL DURATION")
                
                # check for quit (typically the Esc key)
                if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                    core.quit()
                
                # check if all components have finished
                if not continueRoutine:  # a component has requested a forced-end of Routine
                    routineForceEnded = True
                    break
                continueRoutine = False  # will revert to True if at least one component still running
                for thisComponent in ImageryTrialComponents:
                    if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                        continueRoutine = True
                        break  # at least one component has not yet finished
                
                # refresh the screen
                if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                    win.flip()
            
            # --- Ending Routine "ImageryTrial" ---
            for thisComponent in ImageryTrialComponents:
                if hasattr(thisComponent, "setAutoDraw"):
                    thisComponent.setAutoDraw(False)
            # the Routine "ImageryTrial" was not non-slip safe, so reset the non-slip timer
            routineTimer.reset()
            
            # --- Prepare to start Routine "ImageryRebound" ---
            continueRoutine = True
            routineForceEnded = False
            # update component parameters for each repeat
            # Run 'Begin Routine' code from imagery_reb_code
            #-----#
            # Set mouse visibility.
            if show_mouse == True:
                win.mouseVisible = True
            elif show_mouse == False:
                win.mouseVisible = False
            
            #-----#
            # Show message to not move the hand during
            # imagery trials.
            if do_not_move == True:
                if locale == "en":
                    msg = "Please do not move your hand!"
                elif locale == "fr":
                    msg = "Ne bougez pas votre main !"
                
                moved_hand_in_imag.text = msg
            # keep track of which components have finished
            ImageryReboundComponents = [moved_hand_in_imag, fixation_cross_imagery_rebound, arc_imagery_rebound]
            for thisComponent in ImageryReboundComponents:
                thisComponent.tStart = None
                thisComponent.tStop = None
                thisComponent.tStartRefresh = None
                thisComponent.tStopRefresh = None
                if hasattr(thisComponent, 'status'):
                    thisComponent.status = NOT_STARTED
            # reset timers
            t = 0
            _timeToFirstFrame = win.getFutureFlipTime(clock="now")
            frameN = -1
            
            # --- Run Routine "ImageryRebound" ---
            while continueRoutine:
                # get current time
                t = routineTimer.getTime()
                tThisFlip = win.getFutureFlipTime(clock=routineTimer)
                tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                # update/draw components on each frame
                
                # *moved_hand_in_imag* updates
                if moved_hand_in_imag.status == NOT_STARTED and (do_not_move == True):
                    # keep track of start time/frame for later
                    moved_hand_in_imag.frameNStart = frameN  # exact frame index
                    moved_hand_in_imag.tStart = t  # local t and not account for scr refresh
                    moved_hand_in_imag.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(moved_hand_in_imag, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'moved_hand_in_imag.started')
                    moved_hand_in_imag.setAutoDraw(True)
                if moved_hand_in_imag.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > moved_hand_in_imag.tStartRefresh + too_fast_period-frameTolerance:
                        # keep track of stop time/frame for later
                        moved_hand_in_imag.tStop = t  # not accounting for scr refresh
                        moved_hand_in_imag.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'moved_hand_in_imag.stopped')
                        moved_hand_in_imag.setAutoDraw(False)
                
                # *fixation_cross_imagery_rebound* updates
                if fixation_cross_imagery_rebound.status == NOT_STARTED and (do_not_move == False):
                    # keep track of start time/frame for later
                    fixation_cross_imagery_rebound.frameNStart = frameN  # exact frame index
                    fixation_cross_imagery_rebound.tStart = t  # local t and not account for scr refresh
                    fixation_cross_imagery_rebound.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(fixation_cross_imagery_rebound, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'fixation_cross_imagery_rebound.started')
                    fixation_cross_imagery_rebound.setAutoDraw(True)
                if fixation_cross_imagery_rebound.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > fixation_cross_imagery_rebound.tStartRefresh + (rebound_period + avoid_flicker)-frameTolerance:
                        # keep track of stop time/frame for later
                        fixation_cross_imagery_rebound.tStop = t  # not accounting for scr refresh
                        fixation_cross_imagery_rebound.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'fixation_cross_imagery_rebound.stopped')
                        fixation_cross_imagery_rebound.setAutoDraw(False)
                
                # *arc_imagery_rebound* updates
                if arc_imagery_rebound.status == NOT_STARTED and (do_not_move == False):
                    # keep track of start time/frame for later
                    arc_imagery_rebound.frameNStart = frameN  # exact frame index
                    arc_imagery_rebound.tStart = t  # local t and not account for scr refresh
                    arc_imagery_rebound.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(arc_imagery_rebound, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'arc_imagery_rebound.started')
                    arc_imagery_rebound.setAutoDraw(True)
                if arc_imagery_rebound.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > arc_imagery_rebound.tStartRefresh + (rebound_period + avoid_flicker)-frameTolerance:
                        # keep track of stop time/frame for later
                        arc_imagery_rebound.tStop = t  # not accounting for scr refresh
                        arc_imagery_rebound.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'arc_imagery_rebound.stopped')
                        arc_imagery_rebound.setAutoDraw(False)
                # Run 'Each Frame' code from imagery_reb_code
                #-----#
                # End routine in appropriate timing.
                if (
                    (do_not_move == True and t > too_fast_period)
                    or (do_not_move == False and t >= rebound_period + avoid_flicker)
                ):
                
                    continueRoutine = False
                
                # check for quit (typically the Esc key)
                if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                    core.quit()
                
                # check if all components have finished
                if not continueRoutine:  # a component has requested a forced-end of Routine
                    routineForceEnded = True
                    break
                continueRoutine = False  # will revert to True if at least one component still running
                for thisComponent in ImageryReboundComponents:
                    if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                        continueRoutine = True
                        break  # at least one component has not yet finished
                
                # refresh the screen
                if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                    win.flip()
            
            # --- Ending Routine "ImageryRebound" ---
            for thisComponent in ImageryReboundComponents:
                if hasattr(thisComponent, "setAutoDraw"):
                    thisComponent.setAutoDraw(False)
            # the Routine "ImageryRebound" was not non-slip safe, so reset the non-slip timer
            routineTimer.reset()
            
            # --- Prepare to start Routine "GenericRebound" ---
            continueRoutine = True
            routineForceEnded = False
            # update component parameters for each repeat
            # Run 'Begin Routine' code from rebound_gen
            #-----#
            # Set mouse visibility.
            if show_mouse == True:
                win.mouseVisible = True
            elif show_mouse == False:
                win.mouseVisible = False
            
            #-----#
            if debug_mode == True:
                print("Into Generic Rebound.")
            
            #-----#
            # Only happen following a valid ACTION trial.
            #if (too_slow == False or too_fast == False):
            #    
            #    continueRoutine = False
            # keep track of which components have finished
            GenericReboundComponents = [arc_gen_rebound]
            for thisComponent in GenericReboundComponents:
                thisComponent.tStart = None
                thisComponent.tStop = None
                thisComponent.tStartRefresh = None
                thisComponent.tStopRefresh = None
                if hasattr(thisComponent, 'status'):
                    thisComponent.status = NOT_STARTED
            # reset timers
            t = 0
            _timeToFirstFrame = win.getFutureFlipTime(clock="now")
            frameN = -1
            
            # --- Run Routine "GenericRebound" ---
            while continueRoutine:
                # get current time
                t = routineTimer.getTime()
                tThisFlip = win.getFutureFlipTime(clock=routineTimer)
                tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                # update/draw components on each frame
                
                # *arc_gen_rebound* updates
                if arc_gen_rebound.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                    # keep track of start time/frame for later
                    arc_gen_rebound.frameNStart = frameN  # exact frame index
                    arc_gen_rebound.tStart = t  # local t and not account for scr refresh
                    arc_gen_rebound.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(arc_gen_rebound, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'arc_gen_rebound.started')
                    arc_gen_rebound.setAutoDraw(True)
                if arc_gen_rebound.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > arc_gen_rebound.tStartRefresh + (rebound_period + avoid_flicker)-frameTolerance:
                        # keep track of stop time/frame for later
                        arc_gen_rebound.tStop = t  # not accounting for scr refresh
                        arc_gen_rebound.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'arc_gen_rebound.stopped')
                        arc_gen_rebound.setAutoDraw(False)
                
                # check for quit (typically the Esc key)
                if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                    core.quit()
                
                # check if all components have finished
                if not continueRoutine:  # a component has requested a forced-end of Routine
                    routineForceEnded = True
                    break
                continueRoutine = False  # will revert to True if at least one component still running
                for thisComponent in GenericReboundComponents:
                    if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                        continueRoutine = True
                        break  # at least one component has not yet finished
                
                # refresh the screen
                if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                    win.flip()
            
            # --- Ending Routine "GenericRebound" ---
            for thisComponent in GenericReboundComponents:
                if hasattr(thisComponent, "setAutoDraw"):
                    thisComponent.setAutoDraw(False)
            # the Routine "GenericRebound" was not non-slip safe, so reset the non-slip timer
            routineTimer.reset()
            thisExp.nextEntry()
            
        # completed n_imag_trial repeats of 'IMAGERYTRIAL'
        
        
        # set up handler to look after randomisation of conditions etc
        CATCHTRIAL = data.TrialHandler(nReps=n_catch_trial, method='random', 
            extraInfo=expInfo, originPath=-1,
            trialList=[None],
            seed=None, name='CATCHTRIAL')
        thisExp.addLoop(CATCHTRIAL)  # add the loop to the experiment
        thisCATCHTRIAL = CATCHTRIAL.trialList[0]  # so we can initialise stimuli with some values
        # abbreviate parameter names if possible (e.g. rgb = thisCATCHTRIAL.rgb)
        if thisCATCHTRIAL != None:
            for paramName in thisCATCHTRIAL:
                exec('{} = thisCATCHTRIAL[paramName]'.format(paramName))
        
        for thisCATCHTRIAL in CATCHTRIAL:
            currentLoop = CATCHTRIAL
            # abbreviate parameter names if possible (e.g. rgb = thisCATCHTRIAL.rgb)
            if thisCATCHTRIAL != None:
                for paramName in thisCATCHTRIAL:
                    exec('{} = thisCATCHTRIAL[paramName]'.format(paramName))
            
            # --- Prepare to start Routine "CatchTrial" ---
            continueRoutine = True
            routineForceEnded = False
            # update component parameters for each repeat
            key_resp_catch.keys = []
            key_resp_catch.rt = []
            _key_resp_catch_allKeys = []
            # Run 'Begin Routine' code from update_trial
            #-----#
            # Set mouse visibility.
            if show_mouse == True:
                win.mouseVisible = True
            elif show_mouse == False:
                win.mouseVisible = False
            
            #-----#
            # Ignore movements made between trials.
            mouse_adj_trial.setPos(targets_coords["0"])
            
            # Get neutral position degrees.
            cursor_catch_deg = targets_degrees["0"]
            
            # Reset mouse state.
            do_not_move = False
            
            #-----#
            if locale == "en":
                catch_msg = "Indicate where you believe your cursor has landed by pressing any button."
            elif locale == "fr":
                catch_msg = "Indiquez l'endroit où vous pensez que votre curseur a atterri en appuyant sur n'importe quel bouton."
            
            text_catch.text = catch_msg
            
            #-----#
            # Reset variable that saves the state of trial with
            # unacceptable reaction time.
            too_far = 0
            not_far_enough = 0
            far_enough = 1
            
            # Reset variable that indicates the movement
            # direction.
            catch_sign = 0
            
            #-----#
            # Count the number of keypresses.
            key_count = 5
            
            #-----#
            # Monitor keypresses.
            #last_key_press = []
            key_not_pressed = np.array([])
            key_not_pressed_count_t = 0
            first_key_press = False
            
            # Clear any previous events.
            event.clearEvents()
            
            #-----#
            # Cursor first appears at neutral CATCH trial
            # position.
            cursor_catch_trial_bp.setPos(targets_catch_coords[catch_keys[key_count]])
            
            #-----#
            # Inform about random catch offset.
            if debug_mode == True:
                print("Catch offset: {}s".format(catch_offset))
            # keep track of which components have finished
            CatchTrialComponents = [text_catch, fixation_cross_catch_trial, arc_catch_trial_normal, arc_catch_trial, cursor_catch_trial, cursor_catch_trial_bp, target_catch_trial, target_catch_trial_go, key_resp_catch]
            for thisComponent in CatchTrialComponents:
                thisComponent.tStart = None
                thisComponent.tStop = None
                thisComponent.tStartRefresh = None
                thisComponent.tStopRefresh = None
                if hasattr(thisComponent, 'status'):
                    thisComponent.status = NOT_STARTED
            # reset timers
            t = 0
            _timeToFirstFrame = win.getFutureFlipTime(clock="now")
            frameN = -1
            
            # --- Run Routine "CatchTrial" ---
            while continueRoutine:
                # get current time
                t = routineTimer.getTime()
                tThisFlip = win.getFutureFlipTime(clock=routineTimer)
                tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                # update/draw components on each frame
                
                # *text_catch* updates
                if text_catch.status == NOT_STARTED and t > reaction_period + catch_offset:
                    # keep track of start time/frame for later
                    text_catch.frameNStart = frameN  # exact frame index
                    text_catch.tStart = t  # local t and not account for scr refresh
                    text_catch.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(text_catch, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'text_catch.started')
                    text_catch.setAutoDraw(True)
                if text_catch.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > text_catch.tStartRefresh + catch_eval == True-frameTolerance:
                        # keep track of stop time/frame for later
                        text_catch.tStop = t  # not accounting for scr refresh
                        text_catch.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'text_catch.stopped')
                        text_catch.setAutoDraw(False)
                
                # *fixation_cross_catch_trial* updates
                if fixation_cross_catch_trial.status == NOT_STARTED and n_catch_trial == 1.0:
                    # keep track of start time/frame for later
                    fixation_cross_catch_trial.frameNStart = frameN  # exact frame index
                    fixation_cross_catch_trial.tStart = t  # local t and not account for scr refresh
                    fixation_cross_catch_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(fixation_cross_catch_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'fixation_cross_catch_trial.started')
                    fixation_cross_catch_trial.setAutoDraw(True)
                if fixation_cross_catch_trial.status == STARTED:
                    if bool(t > reaction_period + catch_offset):
                        # keep track of stop time/frame for later
                        fixation_cross_catch_trial.tStop = t  # not accounting for scr refresh
                        fixation_cross_catch_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'fixation_cross_catch_trial.stopped')
                        fixation_cross_catch_trial.setAutoDraw(False)
                
                # *arc_catch_trial_normal* updates
                if arc_catch_trial_normal.status == NOT_STARTED and n_catch_trial == 1.0:
                    # keep track of start time/frame for later
                    arc_catch_trial_normal.frameNStart = frameN  # exact frame index
                    arc_catch_trial_normal.tStart = t  # local t and not account for scr refresh
                    arc_catch_trial_normal.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(arc_catch_trial_normal, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'arc_catch_trial_normal.started')
                    arc_catch_trial_normal.setAutoDraw(True)
                if arc_catch_trial_normal.status == STARTED:
                    if bool(t > reaction_period + catch_offset):
                        # keep track of stop time/frame for later
                        arc_catch_trial_normal.tStop = t  # not accounting for scr refresh
                        arc_catch_trial_normal.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'arc_catch_trial_normal.stopped')
                        arc_catch_trial_normal.setAutoDraw(False)
                
                # *arc_catch_trial* updates
                if arc_catch_trial.status == NOT_STARTED and t > reaction_period + catch_offset:
                    # keep track of start time/frame for later
                    arc_catch_trial.frameNStart = frameN  # exact frame index
                    arc_catch_trial.tStart = t  # local t and not account for scr refresh
                    arc_catch_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(arc_catch_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'arc_catch_trial.started')
                    arc_catch_trial.setAutoDraw(True)
                if arc_catch_trial.status == STARTED:
                    if bool(catch_eval == True):
                        # keep track of stop time/frame for later
                        arc_catch_trial.tStop = t  # not accounting for scr refresh
                        arc_catch_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'arc_catch_trial.stopped')
                        arc_catch_trial.setAutoDraw(False)
                
                # *cursor_catch_trial* updates
                if cursor_catch_trial.status == NOT_STARTED and n_catch_trial == 1.0:
                    # keep track of start time/frame for later
                    cursor_catch_trial.frameNStart = frameN  # exact frame index
                    cursor_catch_trial.tStart = t  # local t and not account for scr refresh
                    cursor_catch_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(cursor_catch_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'cursor_catch_trial.started')
                    cursor_catch_trial.setAutoDraw(True)
                if cursor_catch_trial.status == STARTED:
                    if bool((t >= concentration_period + avoid_gestalt)):
                        # keep track of stop time/frame for later
                        cursor_catch_trial.tStop = t  # not accounting for scr refresh
                        cursor_catch_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'cursor_catch_trial.stopped')
                        cursor_catch_trial.setAutoDraw(False)
                
                # *cursor_catch_trial_bp* updates
                if cursor_catch_trial_bp.status == NOT_STARTED and t > reaction_period + catch_offset:
                    # keep track of start time/frame for later
                    cursor_catch_trial_bp.frameNStart = frameN  # exact frame index
                    cursor_catch_trial_bp.tStart = t  # local t and not account for scr refresh
                    cursor_catch_trial_bp.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(cursor_catch_trial_bp, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'cursor_catch_trial_bp.started')
                    cursor_catch_trial_bp.setAutoDraw(True)
                if cursor_catch_trial_bp.status == STARTED:
                    if bool(catch_eval == True):
                        # keep track of stop time/frame for later
                        cursor_catch_trial_bp.tStop = t  # not accounting for scr refresh
                        cursor_catch_trial_bp.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'cursor_catch_trial_bp.stopped')
                        cursor_catch_trial_bp.setAutoDraw(False)
                
                # *target_catch_trial* updates
                if target_catch_trial.status == NOT_STARTED and n_catch_trial == 1.0:
                    # keep track of start time/frame for later
                    target_catch_trial.frameNStart = frameN  # exact frame index
                    target_catch_trial.tStart = t  # local t and not account for scr refresh
                    target_catch_trial.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(target_catch_trial, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_catch_trial.started')
                    target_catch_trial.setAutoDraw(True)
                if target_catch_trial.status == STARTED:
                    if bool(t >= concentration_period):
                        # keep track of stop time/frame for later
                        target_catch_trial.tStop = t  # not accounting for scr refresh
                        target_catch_trial.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'target_catch_trial.stopped')
                        target_catch_trial.setAutoDraw(False)
                
                # *target_catch_trial_go* updates
                if target_catch_trial_go.status == NOT_STARTED and (n_catch_trial == 1.0) and (t > concentration_period):
                    # keep track of start time/frame for later
                    target_catch_trial_go.frameNStart = frameN  # exact frame index
                    target_catch_trial_go.tStart = t  # local t and not account for scr refresh
                    target_catch_trial_go.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(target_catch_trial_go, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_catch_trial_go.started')
                    target_catch_trial_go.setAutoDraw(True)
                if target_catch_trial_go.status == STARTED:
                    if bool(t >= reaction_period + catch_offset):
                        # keep track of stop time/frame for later
                        target_catch_trial_go.tStop = t  # not accounting for scr refresh
                        target_catch_trial_go.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'target_catch_trial_go.stopped')
                        target_catch_trial_go.setAutoDraw(False)
                
                # *key_resp_catch* updates
                waitOnFlip = False
                if key_resp_catch.status == NOT_STARTED and t > reaction_period + catch_offset:
                    # keep track of start time/frame for later
                    key_resp_catch.frameNStart = frameN  # exact frame index
                    key_resp_catch.tStart = t  # local t and not account for scr refresh
                    key_resp_catch.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(key_resp_catch, 'tStartRefresh')  # time at next scr refresh
                    key_resp_catch.status = STARTED
                    # AllowedKeys looks like a variable named `valid_keys`
                    if not type(valid_keys) in [list, tuple, np.ndarray]:
                        if not isinstance(valid_keys, str):
                            logging.error('AllowedKeys variable `valid_keys` is not string- or list-like.')
                            core.quit()
                        elif not ',' in valid_keys:
                            valid_keys = (valid_keys,)
                        else:
                            valid_keys = eval(valid_keys)
                    # keyboard checking is just starting
                    waitOnFlip = True
                    win.callOnFlip(key_resp_catch.clock.reset)  # t=0 on next screen flip
                    win.callOnFlip(key_resp_catch.clearEvents, eventType='keyboard')  # clear events on next screen flip
                if key_resp_catch.status == STARTED and not waitOnFlip:
                    theseKeys = key_resp_catch.getKeys(keyList=list(valid_keys), waitRelease=False)
                    _key_resp_catch_allKeys.extend(theseKeys)
                    if len(_key_resp_catch_allKeys):
                        key_resp_catch.keys = _key_resp_catch_allKeys[-1].name  # just the last key pressed
                        key_resp_catch.rt = _key_resp_catch_allKeys[-1].rt
                # Run 'Each Frame' code from update_trial
                #-----#
                if t <= reaction_period + catch_offset:
                    
                    # Update cursor position.
                    mouse_x, mouse_y = mouse_adj_trial.getPos()
                    cursor_x_tr, cursor_y_tr, cursor_x_angle = transform_mouse(
                        mouse_x,
                        mouse_y,
                        system,
                        room,
                        wrist_distance,
                    )
                
                    # The trial is aborted if the mouse is moved.
                    if  (cursor_x_angle < cursor_catch_deg - mouse_conf) or\
                        (cursor_x_angle > cursor_catch_deg + mouse_conf):
                        
                        # Trigger "DO NOT MOVE" message in "ActionTrialAdj" Routine.
                        do_not_move = True
                        
                        continueRoutine = False
                        if debug_mode == True:
                            print("Stop reason: MOVED DURING CATCH")
                
                #-----#
                # Monitor key presses only after the
                # appearance of the CATCH arc.
                elif t > reaction_period + catch_offset:
                    
                    # Cursor appears at corresponding CATCH target
                    # position.
                    cursor_catch_trial_bp.setPos(targets_catch_coords[catch_keys[key_count]])
                    cursor_catch_trial_bp.setOri(targets_catch_degrees[catch_keys[key_count]])
                    
                    # Monitor key presses.
                    keys = event.getKeys(valid_keys)
                
                    if len(keys) != 0:
                        key_not_pressed = np.append(key_not_pressed, 0)
                        
                        first_key_press = True
                        
                        # Save time of last key press.
                        #last_key_press.append(t)
                        
                        if debug_mode == True:
                            print("KEY PRESS")
                    
                    elif len(keys) == 0:
                        key_not_pressed = np.append(key_not_pressed, 1)
                
                    key_not_pressed_count_t = np.sum(np.flip(key_not_pressed)[:not_pressed_id])
                
                    # End routine when no keypress is detected
                    # for a set amount of time.
                    if (t >= catch_inertia - (reaction_period + catch_offset)) and\
                        (key_not_pressed_count_t >= not_pressed_id) and\
                        first_key_press == True:
                       
                        catch_eval = False
                        continueRoutine = False
                        
                        if debug_mode == True:
                            print("Time: {} s".format(t))
                            print("No keypresses for {}s.".format(catch_threshold))
                
                    # Else update key count when a key is
                    # pressed.
                    else:
                        
                        if key_not_pressed[-1] == 0:
                            if np.sign(targets_degrees[target_position]) > 0:
                                # Clockwise increments.
                                key_count += 1
                            elif np.sign(targets_degrees[target_position]) < 0:
                                # Counter-clockwise increments.
                                key_count -= 1
                            
                            if debug_mode == True:
                                print("UPDATED KEY COUNT: {}".format(key_count))
                
                    # Wrap cursor around the arc.
                    if np.sign(targets_degrees[target_position]) > 0:
                        if key_count > 10:
                            key_count = 0
                    elif np.sign(targets_degrees[target_position]) < 0:
                        if key_count < 0:
                            key_count = 10
                
                # check for quit (typically the Esc key)
                if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                    core.quit()
                
                # check if all components have finished
                if not continueRoutine:  # a component has requested a forced-end of Routine
                    routineForceEnded = True
                    break
                continueRoutine = False  # will revert to True if at least one component still running
                for thisComponent in CatchTrialComponents:
                    if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                        continueRoutine = True
                        break  # at least one component has not yet finished
                
                # refresh the screen
                if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                    win.flip()
            
            # --- Ending Routine "CatchTrial" ---
            for thisComponent in CatchTrialComponents:
                if hasattr(thisComponent, "setAutoDraw"):
                    thisComponent.setAutoDraw(False)
            # check responses
            if key_resp_catch.keys in ['', [], None]:  # No response was made
                key_resp_catch.keys = None
            CATCHTRIAL.addData('key_resp_catch.keys',key_resp_catch.keys)
            if key_resp_catch.keys != None:  # we had a response
                CATCHTRIAL.addData('key_resp_catch.rt', key_resp_catch.rt)
            # Run 'End Routine' code from update_trial
            # ----- #
            # Estimate if the indicated CATCH final target
            # position is too far from the starting
            # position and the target, given the provided
            # `catch_offset`.
            
            # Retrieve cursor position and available time
            # for current trial.
            catch_cursor_pos = catch_keys[key_count]
            
            cursor_distance = targets_catch_degrees[catch_cursor_pos]
            target_distance = targets_degrees[target_position]
            
            if debug_mode == True:
                print("Catch cursor degrees: {}".format(str(cursor_distance)))
                print("Catch target degrees: {}".format(str(target_distance)))
            
            #-----#
            # Retrieve sign of movement direction (towards
            # or away from indicated target).
            catch_sign = np.sign(cursor_distance * target_distance)
            
            #-----#
            # Retrieve expected movement speed and
            # compute reported movement speed.
            time_given = (reaction_period - concentration_period) + catch_offset
            
            move_speed_exp = np.sort(
                np.abs(action_task_metrics_target["move_percs"])
            )
            move_speed_rep = cursor_distance / time_given
            
            move_distance_exp = move_speed_exp * time_given
            
            if debug_mode == True:
                print("Time given in CATCH trial: {}s".format(time_given))
                print("Expected cursor movement: {} degrees".format(move_distance_exp))
                print("Expected cursor speed: {} degrees/s".format(move_speed_exp))
                print("Estimated cursor speed: {} degrees/s".format(move_speed_rep))
            
            # The indicated movement will be considered
            # valid only if the computed movement speed
            # falls within [25th, 75th] percentile of all
            # ACTION movement speeds for the corresponding
            # target and the distance covered isn't too
            # small or too long respectively.
            if (
                np.abs(cursor_distance) >= move_distance_exp[0]
                and np.abs(cursor_distance) <= move_distance_exp[1]
                and do_not_move == False
            ):
                # Assessment indicates moved within limits
                # expected based on ACTION trials.
                too_far = 0
                not_far_enough = 0
                far_enough = 1
            
            elif (
                np.abs(cursor_distance) < move_distance_exp[0]
                and do_not_move == False
            ):
                if np.abs(cursor_distance) == np.abs(target_distance):
                    # Accept case reaching the target because
                    # there was ample time.
                    too_far = 0
                    not_far_enough = 0
                    far_enough = 1
                elif np.abs(cursor_distance) > np.abs(target_distance):
                    # Assessment indicates moved further than
                    # what was asked.
                    too_far = 1
                    not_far_enough = 0
                    far_enough = 0
                else:
                    # Assessment indicates not moved as far as
                    # expected based on ACTION trials.
                    too_far = 0
                    not_far_enough = 1
                    far_enough = 0
            
            elif (
                np.abs(cursor_distance) > move_distance_exp[1]
                and do_not_move == False
            ):
                # Assessment indicates moved further than
                # expected based on ACTION trials.
                too_far = 1
                not_far_enough = 0
                far_enough = 0
            
            elif do_not_move == True:
                # "Hack" allows to go into ImageryRebound
                # and show "DO NOT MOVE" message.
                too_far = 0
                not_far_enough = 0
                far_enough = 1
            
            if debug_mode == True:
                print(too_far, not_far_enough, far_enough)
            # the Routine "CatchTrial" was not non-slip safe, so reset the non-slip timer
            routineTimer.reset()
            
            # set up handler to look after randomisation of conditions etc
            im_reb = data.TrialHandler(nReps=far_enough, method='random', 
                extraInfo=expInfo, originPath=-1,
                trialList=[None],
                seed=None, name='im_reb')
            thisExp.addLoop(im_reb)  # add the loop to the experiment
            thisIm_reb = im_reb.trialList[0]  # so we can initialise stimuli with some values
            # abbreviate parameter names if possible (e.g. rgb = thisIm_reb.rgb)
            if thisIm_reb != None:
                for paramName in thisIm_reb:
                    exec('{} = thisIm_reb[paramName]'.format(paramName))
            
            for thisIm_reb in im_reb:
                currentLoop = im_reb
                # abbreviate parameter names if possible (e.g. rgb = thisIm_reb.rgb)
                if thisIm_reb != None:
                    for paramName in thisIm_reb:
                        exec('{} = thisIm_reb[paramName]'.format(paramName))
                
                # --- Prepare to start Routine "ImageryRebound" ---
                continueRoutine = True
                routineForceEnded = False
                # update component parameters for each repeat
                # Run 'Begin Routine' code from imagery_reb_code
                #-----#
                # Set mouse visibility.
                if show_mouse == True:
                    win.mouseVisible = True
                elif show_mouse == False:
                    win.mouseVisible = False
                
                #-----#
                # Show message to not move the hand during
                # imagery trials.
                if do_not_move == True:
                    if locale == "en":
                        msg = "Please do not move your hand!"
                    elif locale == "fr":
                        msg = "Ne bougez pas votre main !"
                    
                    moved_hand_in_imag.text = msg
                # keep track of which components have finished
                ImageryReboundComponents = [moved_hand_in_imag, fixation_cross_imagery_rebound, arc_imagery_rebound]
                for thisComponent in ImageryReboundComponents:
                    thisComponent.tStart = None
                    thisComponent.tStop = None
                    thisComponent.tStartRefresh = None
                    thisComponent.tStopRefresh = None
                    if hasattr(thisComponent, 'status'):
                        thisComponent.status = NOT_STARTED
                # reset timers
                t = 0
                _timeToFirstFrame = win.getFutureFlipTime(clock="now")
                frameN = -1
                
                # --- Run Routine "ImageryRebound" ---
                while continueRoutine:
                    # get current time
                    t = routineTimer.getTime()
                    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
                    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                    # update/draw components on each frame
                    
                    # *moved_hand_in_imag* updates
                    if moved_hand_in_imag.status == NOT_STARTED and (do_not_move == True):
                        # keep track of start time/frame for later
                        moved_hand_in_imag.frameNStart = frameN  # exact frame index
                        moved_hand_in_imag.tStart = t  # local t and not account for scr refresh
                        moved_hand_in_imag.tStartRefresh = tThisFlipGlobal  # on global time
                        win.timeOnFlip(moved_hand_in_imag, 'tStartRefresh')  # time at next scr refresh
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'moved_hand_in_imag.started')
                        moved_hand_in_imag.setAutoDraw(True)
                    if moved_hand_in_imag.status == STARTED:
                        # is it time to stop? (based on global clock, using actual start)
                        if tThisFlipGlobal > moved_hand_in_imag.tStartRefresh + too_fast_period-frameTolerance:
                            # keep track of stop time/frame for later
                            moved_hand_in_imag.tStop = t  # not accounting for scr refresh
                            moved_hand_in_imag.frameNStop = frameN  # exact frame index
                            # add timestamp to datafile
                            thisExp.timestampOnFlip(win, 'moved_hand_in_imag.stopped')
                            moved_hand_in_imag.setAutoDraw(False)
                    
                    # *fixation_cross_imagery_rebound* updates
                    if fixation_cross_imagery_rebound.status == NOT_STARTED and (do_not_move == False):
                        # keep track of start time/frame for later
                        fixation_cross_imagery_rebound.frameNStart = frameN  # exact frame index
                        fixation_cross_imagery_rebound.tStart = t  # local t and not account for scr refresh
                        fixation_cross_imagery_rebound.tStartRefresh = tThisFlipGlobal  # on global time
                        win.timeOnFlip(fixation_cross_imagery_rebound, 'tStartRefresh')  # time at next scr refresh
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'fixation_cross_imagery_rebound.started')
                        fixation_cross_imagery_rebound.setAutoDraw(True)
                    if fixation_cross_imagery_rebound.status == STARTED:
                        # is it time to stop? (based on global clock, using actual start)
                        if tThisFlipGlobal > fixation_cross_imagery_rebound.tStartRefresh + (rebound_period + avoid_flicker)-frameTolerance:
                            # keep track of stop time/frame for later
                            fixation_cross_imagery_rebound.tStop = t  # not accounting for scr refresh
                            fixation_cross_imagery_rebound.frameNStop = frameN  # exact frame index
                            # add timestamp to datafile
                            thisExp.timestampOnFlip(win, 'fixation_cross_imagery_rebound.stopped')
                            fixation_cross_imagery_rebound.setAutoDraw(False)
                    
                    # *arc_imagery_rebound* updates
                    if arc_imagery_rebound.status == NOT_STARTED and (do_not_move == False):
                        # keep track of start time/frame for later
                        arc_imagery_rebound.frameNStart = frameN  # exact frame index
                        arc_imagery_rebound.tStart = t  # local t and not account for scr refresh
                        arc_imagery_rebound.tStartRefresh = tThisFlipGlobal  # on global time
                        win.timeOnFlip(arc_imagery_rebound, 'tStartRefresh')  # time at next scr refresh
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'arc_imagery_rebound.started')
                        arc_imagery_rebound.setAutoDraw(True)
                    if arc_imagery_rebound.status == STARTED:
                        # is it time to stop? (based on global clock, using actual start)
                        if tThisFlipGlobal > arc_imagery_rebound.tStartRefresh + (rebound_period + avoid_flicker)-frameTolerance:
                            # keep track of stop time/frame for later
                            arc_imagery_rebound.tStop = t  # not accounting for scr refresh
                            arc_imagery_rebound.frameNStop = frameN  # exact frame index
                            # add timestamp to datafile
                            thisExp.timestampOnFlip(win, 'arc_imagery_rebound.stopped')
                            arc_imagery_rebound.setAutoDraw(False)
                    # Run 'Each Frame' code from imagery_reb_code
                    #-----#
                    # End routine in appropriate timing.
                    if (
                        (do_not_move == True and t > too_fast_period)
                        or (do_not_move == False and t >= rebound_period + avoid_flicker)
                    ):
                    
                        continueRoutine = False
                    
                    # check for quit (typically the Esc key)
                    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                        core.quit()
                    
                    # check if all components have finished
                    if not continueRoutine:  # a component has requested a forced-end of Routine
                        routineForceEnded = True
                        break
                    continueRoutine = False  # will revert to True if at least one component still running
                    for thisComponent in ImageryReboundComponents:
                        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                            continueRoutine = True
                            break  # at least one component has not yet finished
                    
                    # refresh the screen
                    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                        win.flip()
                
                # --- Ending Routine "ImageryRebound" ---
                for thisComponent in ImageryReboundComponents:
                    if hasattr(thisComponent, "setAutoDraw"):
                        thisComponent.setAutoDraw(False)
                # the Routine "ImageryRebound" was not non-slip safe, so reset the non-slip timer
                routineTimer.reset()
                thisExp.nextEntry()
                
            # completed far_enough repeats of 'im_reb'
            
            
            # set up handler to look after randomisation of conditions etc
            catch_reb = data.TrialHandler(nReps=too_far + not_far_enough, method='random', 
                extraInfo=expInfo, originPath=-1,
                trialList=[None],
                seed=None, name='catch_reb')
            thisExp.addLoop(catch_reb)  # add the loop to the experiment
            thisCatch_reb = catch_reb.trialList[0]  # so we can initialise stimuli with some values
            # abbreviate parameter names if possible (e.g. rgb = thisCatch_reb.rgb)
            if thisCatch_reb != None:
                for paramName in thisCatch_reb:
                    exec('{} = thisCatch_reb[paramName]'.format(paramName))
            
            for thisCatch_reb in catch_reb:
                currentLoop = catch_reb
                # abbreviate parameter names if possible (e.g. rgb = thisCatch_reb.rgb)
                if thisCatch_reb != None:
                    for paramName in thisCatch_reb:
                        exec('{} = thisCatch_reb[paramName]'.format(paramName))
                
                # --- Prepare to start Routine "CatchRebound" ---
                continueRoutine = True
                routineForceEnded = False
                # update component parameters for each repeat
                key_resp_reb.keys = []
                key_resp_reb.rt = []
                _key_resp_reb_allKeys = []
                # Run 'Begin Routine' code from imagery_catch_code
                #-----#
                # Set mouse visibility.
                if show_mouse == True:
                    win.mouseVisible = True
                elif show_mouse == False:
                    win.mouseVisible = False
                
                #-----#
                if debug_mode == True:
                    print("Into Catch Rebound.")
                
                #-----#
                # Inform type of block.
                if locale == "en":
                    if catch_sign < 0:
                        msg_c1 = "You have indicated that you imagined your hand moving in "
                        msg_c2 = "a direction opposite to the target!"
                        msg_c3 = "\n\nPlease try to aim for the target!"
                    elif too_far == 1 and not_far_enough == 0:
                        msg_c1 = "The time you were given in this trial was not enough for your "
                        msg_c2 = "hand to have moved that much!"
                        msg_c3 = "\n\nPlease try to adjust the speed of your imagined movements to that of real movements."
                    elif too_far == 0 and not_far_enough == 1:
                        msg_c1 = "The time you were given in this trial was enough for your "
                        msg_c2 = "hand to have moved closer to the target!"
                        msg_c3 = "\n\nPlease try to adjust the speed of your imagined movements to that of real movements."
                    msg_c4 = "\n\nPress any button to continue."
                
                elif locale == "fr":
                    if catch_sign < 0:
                        msg_c1 = "Vous avez indiqué que vous imaginiez votre main se déplaçant "
                        msg_c2 = "dans une direction opposée à la cible !"
                        msg_c3 = "\n\nEssayez de viser la cible !"
                    elif too_far == 1 and not_far_enough == 0:
                        msg_c1 = "Le temps qui vous a été accordé dans ce procès n'était pas suffisant "
                        msg_c2 = "pour que votre main se soit rapprochée de la cible !"
                        msg_c3 = "\n\nEssayez d'adapter la vitesse de vos mouvements imaginaires à celle des mouvements réels."
                    elif too_far == 0 and not_far_enough == 1:
                        msg_c1 = "le temps qui vous a été accordé dans ce procès était suffisant "
                        msg_c2 = "pour que votre main ait pu bouger autant !"
                        msg_c3 = "\n\nEssayez d'adapter la vitesse de vos mouvements imaginaires à celle des mouvements réels."
                    msg_c4 = "\n\nAppuyez sur n'importe quel bouton pour continuer."
                
                msg_call = msg_c1 + msg_c2 + msg_c3 + msg_c4
                text_catch_rebound.text = msg_call
                # keep track of which components have finished
                CatchReboundComponents = [text_catch_rebound, key_resp_reb]
                for thisComponent in CatchReboundComponents:
                    thisComponent.tStart = None
                    thisComponent.tStop = None
                    thisComponent.tStartRefresh = None
                    thisComponent.tStopRefresh = None
                    if hasattr(thisComponent, 'status'):
                        thisComponent.status = NOT_STARTED
                # reset timers
                t = 0
                _timeToFirstFrame = win.getFutureFlipTime(clock="now")
                frameN = -1
                
                # --- Run Routine "CatchRebound" ---
                while continueRoutine:
                    # get current time
                    t = routineTimer.getTime()
                    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
                    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                    # update/draw components on each frame
                    
                    # *text_catch_rebound* updates
                    if text_catch_rebound.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                        # keep track of start time/frame for later
                        text_catch_rebound.frameNStart = frameN  # exact frame index
                        text_catch_rebound.tStart = t  # local t and not account for scr refresh
                        text_catch_rebound.tStartRefresh = tThisFlipGlobal  # on global time
                        win.timeOnFlip(text_catch_rebound, 'tStartRefresh')  # time at next scr refresh
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'text_catch_rebound.started')
                        text_catch_rebound.setAutoDraw(True)
                    
                    # *key_resp_reb* updates
                    waitOnFlip = False
                    if key_resp_reb.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                        # keep track of start time/frame for later
                        key_resp_reb.frameNStart = frameN  # exact frame index
                        key_resp_reb.tStart = t  # local t and not account for scr refresh
                        key_resp_reb.tStartRefresh = tThisFlipGlobal  # on global time
                        win.timeOnFlip(key_resp_reb, 'tStartRefresh')  # time at next scr refresh
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'key_resp_reb.started')
                        key_resp_reb.status = STARTED
                        # AllowedKeys looks like a variable named `valid_keys`
                        if not type(valid_keys) in [list, tuple, np.ndarray]:
                            if not isinstance(valid_keys, str):
                                logging.error('AllowedKeys variable `valid_keys` is not string- or list-like.')
                                core.quit()
                            elif not ',' in valid_keys:
                                valid_keys = (valid_keys,)
                            else:
                                valid_keys = eval(valid_keys)
                        # keyboard checking is just starting
                        waitOnFlip = True
                        win.callOnFlip(key_resp_reb.clock.reset)  # t=0 on next screen flip
                        win.callOnFlip(key_resp_reb.clearEvents, eventType='keyboard')  # clear events on next screen flip
                    if key_resp_reb.status == STARTED and not waitOnFlip:
                        theseKeys = key_resp_reb.getKeys(keyList=list(valid_keys), waitRelease=False)
                        _key_resp_reb_allKeys.extend(theseKeys)
                        if len(_key_resp_reb_allKeys):
                            key_resp_reb.keys = _key_resp_reb_allKeys[-1].name  # just the last key pressed
                            key_resp_reb.rt = _key_resp_reb_allKeys[-1].rt
                            # a response ends the routine
                            continueRoutine = False
                    
                    # check for quit (typically the Esc key)
                    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                        core.quit()
                    
                    # check if all components have finished
                    if not continueRoutine:  # a component has requested a forced-end of Routine
                        routineForceEnded = True
                        break
                    continueRoutine = False  # will revert to True if at least one component still running
                    for thisComponent in CatchReboundComponents:
                        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                            continueRoutine = True
                            break  # at least one component has not yet finished
                    
                    # refresh the screen
                    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                        win.flip()
                
                # --- Ending Routine "CatchRebound" ---
                for thisComponent in CatchReboundComponents:
                    if hasattr(thisComponent, "setAutoDraw"):
                        thisComponent.setAutoDraw(False)
                # check responses
                if key_resp_reb.keys in ['', [], None]:  # No response was made
                    key_resp_reb.keys = None
                catch_reb.addData('key_resp_reb.keys',key_resp_reb.keys)
                if key_resp_reb.keys != None:  # we had a response
                    catch_reb.addData('key_resp_reb.rt', key_resp_reb.rt)
                # the Routine "CatchRebound" was not non-slip safe, so reset the non-slip timer
                routineTimer.reset()
                thisExp.nextEntry()
                
            # completed too_far + not_far_enough repeats of 'catch_reb'
            
            
            # --- Prepare to start Routine "GenericRebound" ---
            continueRoutine = True
            routineForceEnded = False
            # update component parameters for each repeat
            # Run 'Begin Routine' code from rebound_gen
            #-----#
            # Set mouse visibility.
            if show_mouse == True:
                win.mouseVisible = True
            elif show_mouse == False:
                win.mouseVisible = False
            
            #-----#
            if debug_mode == True:
                print("Into Generic Rebound.")
            
            #-----#
            # Only happen following a valid ACTION trial.
            #if (too_slow == False or too_fast == False):
            #    
            #    continueRoutine = False
            # keep track of which components have finished
            GenericReboundComponents = [arc_gen_rebound]
            for thisComponent in GenericReboundComponents:
                thisComponent.tStart = None
                thisComponent.tStop = None
                thisComponent.tStartRefresh = None
                thisComponent.tStopRefresh = None
                if hasattr(thisComponent, 'status'):
                    thisComponent.status = NOT_STARTED
            # reset timers
            t = 0
            _timeToFirstFrame = win.getFutureFlipTime(clock="now")
            frameN = -1
            
            # --- Run Routine "GenericRebound" ---
            while continueRoutine:
                # get current time
                t = routineTimer.getTime()
                tThisFlip = win.getFutureFlipTime(clock=routineTimer)
                tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                # update/draw components on each frame
                
                # *arc_gen_rebound* updates
                if arc_gen_rebound.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                    # keep track of start time/frame for later
                    arc_gen_rebound.frameNStart = frameN  # exact frame index
                    arc_gen_rebound.tStart = t  # local t and not account for scr refresh
                    arc_gen_rebound.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(arc_gen_rebound, 'tStartRefresh')  # time at next scr refresh
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'arc_gen_rebound.started')
                    arc_gen_rebound.setAutoDraw(True)
                if arc_gen_rebound.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > arc_gen_rebound.tStartRefresh + (rebound_period + avoid_flicker)-frameTolerance:
                        # keep track of stop time/frame for later
                        arc_gen_rebound.tStop = t  # not accounting for scr refresh
                        arc_gen_rebound.frameNStop = frameN  # exact frame index
                        # add timestamp to datafile
                        thisExp.timestampOnFlip(win, 'arc_gen_rebound.stopped')
                        arc_gen_rebound.setAutoDraw(False)
                
                # check for quit (typically the Esc key)
                if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                    core.quit()
                
                # check if all components have finished
                if not continueRoutine:  # a component has requested a forced-end of Routine
                    routineForceEnded = True
                    break
                continueRoutine = False  # will revert to True if at least one component still running
                for thisComponent in GenericReboundComponents:
                    if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                        continueRoutine = True
                        break  # at least one component has not yet finished
                
                # refresh the screen
                if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                    win.flip()
            
            # --- Ending Routine "GenericRebound" ---
            for thisComponent in GenericReboundComponents:
                if hasattr(thisComponent, "setAutoDraw"):
                    thisComponent.setAutoDraw(False)
            # the Routine "GenericRebound" was not non-slip safe, so reset the non-slip timer
            routineTimer.reset()
            thisExp.nextEntry()
            
        # completed n_catch_trial repeats of 'CATCHTRIAL'
        
        
        # --- Prepare to start Routine "EndTrial" ---
        continueRoutine = True
        routineForceEnded = False
        # update component parameters for each repeat
        # Run 'Begin Routine' code from finalize_trial
        #-----#
        # Set mouse visibility.
        if show_mouse == True:
            win.mouseVisible = True
        elif show_mouse == False:
            win.mouseVisible = False
        
        #-----#   
        # Debug message.
        if debug_mode == True:
            print("End of trial {}.\n".format(trial_id+1))
        
        #-----#
        # Starting position of next trial corresponds to
        # zero. 
        cursor_position = "0"
        
        if this_block_type == "imagery":
            cursor_imag_trial.setPos(targets_coords[cursor_position])
            cursor_catch_trial.setPos(targets_coords[cursor_position])
        elif this_block_type == "action":
            cursor_action_trial.setPos(targets_coords[cursor_position])
        
        # Update count of trials.
        if trial_id < (n_trials - 1):
        
            trial_id += 1
            target_position =  str(this_block_target_seq[trial_id])
        
            # Update target position.
            if this_block_type == "imagery":
            
                if cursor_target_sync == False:
                    target_imag_trial.setPos(targets_coords[target_position])
                elif cursor_target_sync == True:
                    target_imag_trial_sync.setPos(targets_coords[target_position])
                    target_imag_trial_go.setPos(targets_coords[target_position])
                
                target_catch_trial.setPos(targets_coords[target_position])
                target_catch_trial_go.setPos(targets_coords[target_position])
                
            elif this_block_type == "action":
        
                if cursor_target_sync == False:
                    target_action_trial.setPos(targets_coords[target_position])
                elif cursor_target_sync == True:
                    target_action_trial_sync.setPos(targets_coords[target_position])
                    target_action_trial_go.setPos(targets_coords[target_position])
                    target_rebound.setPos(targets_coords[target_position])
                
        #-----#
        # Terminate without allowing the arc and the fixation cross to appear
        # following a CATCH trial, or after the last trial of block.
        if (n_catch_trial == 1) or\
            (trial_id == n_trials - 1):
            
            continueRoutine = False
        # keep track of which components have finished
        EndTrialComponents = [fixation_cross_end, arc_itp_end]
        for thisComponent in EndTrialComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "EndTrial" ---
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *fixation_cross_end* updates
            if fixation_cross_end.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                # keep track of start time/frame for later
                fixation_cross_end.frameNStart = frameN  # exact frame index
                fixation_cross_end.tStart = t  # local t and not account for scr refresh
                fixation_cross_end.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(fixation_cross_end, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'fixation_cross_end.started')
                fixation_cross_end.setAutoDraw(True)
            if fixation_cross_end.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > fixation_cross_end.tStartRefresh + 0.1 + avoid_flicker-frameTolerance:
                    # keep track of stop time/frame for later
                    fixation_cross_end.tStop = t  # not accounting for scr refresh
                    fixation_cross_end.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'fixation_cross_end.stopped')
                    fixation_cross_end.setAutoDraw(False)
            
            # *arc_itp_end* updates
            if arc_itp_end.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                # keep track of start time/frame for later
                arc_itp_end.frameNStart = frameN  # exact frame index
                arc_itp_end.tStart = t  # local t and not account for scr refresh
                arc_itp_end.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(arc_itp_end, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'arc_itp_end.started')
                arc_itp_end.setAutoDraw(True)
            if arc_itp_end.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > arc_itp_end.tStartRefresh + 0.1 + avoid_flicker-frameTolerance:
                    # keep track of stop time/frame for later
                    arc_itp_end.tStop = t  # not accounting for scr refresh
                    arc_itp_end.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'arc_itp_end.stopped')
                    arc_itp_end.setAutoDraw(False)
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in EndTrialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "EndTrial" ---
        for thisComponent in EndTrialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        # the Routine "EndTrial" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        thisExp.nextEntry()
        
    # completed n_trials repeats of 'TRIAL'
    
    
    # --- Prepare to start Routine "InterBlockInterval" ---
    continueRoutine = True
    routineForceEnded = False
    # update component parameters for each repeat
    key_resp_5.keys = []
    key_resp_5.rt = []
    _key_resp_5_allKeys = []
    # Run 'Begin Routine' code from update_session
    #-----#
    # Set mouse visibility.
    if show_mouse == True:
        win.mouseVisible = True
    elif show_mouse == False:
        win.mouseVisible = False
    
    #-----#
    if locale == "en":
        msg_ibi = "End of block."
    elif locale == "fr":
        msg_ibi = "Fin du bloc."
    
    IBI.text = msg_ibi
    
    #-----#
    # Optionally provide some feedback to keep
    # participants motivated.
    if gamification == True:
        if locale == "en":
            score_msg = "Your score in this block was {} %.\nPress any button to proceed to the next block".format(block_score)
        elif locale == "fr":
            score_msg = "Votre score dans ce bloc était de {} %.\nAppuyez sur n'importe quel bouton pour passer au bloc suivant.".format(block_score)
        
        IBI_gamification.text = score_msg
    
    elif gamification == False:
        if locale == "en":
            msg_ibi_2_1 = "Next block starting in "
            msg_ibi_2_3 = "\n\nPress any button to proceed to the next block now."
        elif locale == "fr":
            msg_ibi_2_1 = "Le prochain bloc commence dans "
            msg_ibi_2_3 = "\n\nAppuyez sur n'importe quel bouton pour passer au bloc suivant maintenant."
    
    #-----#
    # Option to move on to the next block is
    # controlled by the epxerimenter.
    move_to_next_block = False
    countdown_time = 120
    # keep track of which components have finished
    InterBlockIntervalComponents = [IBI, IBI_2, IBI_gamification, key_resp_5]
    for thisComponent in InterBlockIntervalComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "InterBlockInterval" ---
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *IBI* updates
        if IBI.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            IBI.frameNStart = frameN  # exact frame index
            IBI.tStart = t  # local t and not account for scr refresh
            IBI.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(IBI, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'IBI.started')
            IBI.setAutoDraw(True)
        if IBI.status == STARTED:
            if bool(move_to_next_block == True):
                # keep track of stop time/frame for later
                IBI.tStop = t  # not accounting for scr refresh
                IBI.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'IBI.stopped')
                IBI.setAutoDraw(False)
        
        # *IBI_2* updates
        if IBI_2.status == NOT_STARTED and (gamification == False) and (move_to_next_block == True) and (block_id < n_blocks - 1):
            # keep track of start time/frame for later
            IBI_2.frameNStart = frameN  # exact frame index
            IBI_2.tStart = t  # local t and not account for scr refresh
            IBI_2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(IBI_2, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'IBI_2.started')
            IBI_2.setAutoDraw(True)
        if IBI_2.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > IBI_2.tStartRefresh + 120-frameTolerance:
                # keep track of stop time/frame for later
                IBI_2.tStop = t  # not accounting for scr refresh
                IBI_2.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'IBI_2.stopped')
                IBI_2.setAutoDraw(False)
        
        # *IBI_gamification* updates
        if IBI_gamification.status == NOT_STARTED and (gamification == True) and (move_to_next_block == True) and (block_id < n_blocks - 1):
            # keep track of start time/frame for later
            IBI_gamification.frameNStart = frameN  # exact frame index
            IBI_gamification.tStart = t  # local t and not account for scr refresh
            IBI_gamification.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(IBI_gamification, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'IBI_gamification.started')
            IBI_gamification.setAutoDraw(True)
        if IBI_gamification.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > IBI_gamification.tStartRefresh + 120-frameTolerance:
                # keep track of stop time/frame for later
                IBI_gamification.tStop = t  # not accounting for scr refresh
                IBI_gamification.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'IBI_gamification.stopped')
                IBI_gamification.setAutoDraw(False)
        
        # *key_resp_5* updates
        waitOnFlip = False
        if key_resp_5.status == NOT_STARTED and (t > 20.0) and (block_id < n_blocks - 1):
            # keep track of start time/frame for later
            key_resp_5.frameNStart = frameN  # exact frame index
            key_resp_5.tStart = t  # local t and not account for scr refresh
            key_resp_5.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(key_resp_5, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'key_resp_5.started')
            key_resp_5.status = STARTED
            # AllowedKeys looks like a variable named `valid_keys`
            if not type(valid_keys) in [list, tuple, np.ndarray]:
                if not isinstance(valid_keys, str):
                    logging.error('AllowedKeys variable `valid_keys` is not string- or list-like.')
                    core.quit()
                elif not ',' in valid_keys:
                    valid_keys = (valid_keys,)
                else:
                    valid_keys = eval(valid_keys)
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(key_resp_5.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(key_resp_5.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if key_resp_5.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > key_resp_5.tStartRefresh + 120-frameTolerance:
                # keep track of stop time/frame for later
                key_resp_5.tStop = t  # not accounting for scr refresh
                key_resp_5.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'key_resp_5.stopped')
                key_resp_5.status = FINISHED
        if key_resp_5.status == STARTED and not waitOnFlip:
            theseKeys = key_resp_5.getKeys(keyList=list(valid_keys), waitRelease=False)
            _key_resp_5_allKeys.extend(theseKeys)
            if len(_key_resp_5_allKeys):
                key_resp_5.keys = _key_resp_5_allKeys[-1].name  # just the last key pressed
                key_resp_5.rt = _key_resp_5_allKeys[-1].rt
                # a response ends the routine
                continueRoutine = False
        # Run 'Each Frame' code from update_session
        #-----#
        # Monitor key presses.
            keys = event.getKeys(valid_keys, timeStamped=True)
        
            if keys[-1][0] == "c":
                move_to_next_block = True
                button_press_time = keys[-1][-1]
        
        #-----#
        # End routine after last block without
        # showing 'move to next block' message.
        if block_id == n_blocks - 1:
        
            if t >= 10:
                continueRoutine = False
                
                if debug_mode == True:
                    print("Finished last block.")
        
        elif move_to_next_block == True:
            # Provide a countdown to the participant.
            seconds = int(
                np.around(
                    countdown_time + button_press_time - t,
                    decimals=0
                )
            )
            minutes = int(seconds // 60)
            
            if seconds >= countdown_time:
                msg_ibi_2_2 = "{}:0{}.".format(minutes, seconds - 120)
            if seconds >= 60 and seconds < countdown_time:
                if np.abs(seconds - 60) < 10:
                    msg_ibi_2_2 = "{}:0{}.".format(minutes, seconds - 60)
                else:
                    msg_ibi_2_2 = "{}:{}.".format(minutes, seconds - 60)
            elif seconds < 60 and seconds >= 10:
                msg_ibi_2_2 = "{}:{}.".format(minutes, seconds)
            elif seconds < 10:
                msg_ibi_2_2 = "{}:0{}.".format(minutes, seconds)
            
            msg_ibi_2_all = msg_ibi_2_1 + msg_ibi_2_2 + msg_ibi_2_3
            IBI_2.text = msg_ibi_2_all
            
            # Automatically end routine after 2 minutes,
            # following 20s of inactivity period.
            if t >= countdown_time:
                continueRoutine = False
                
                if debug_mode == True:
                    print("Exceeded max time.")
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in InterBlockIntervalComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "InterBlockInterval" ---
    for thisComponent in InterBlockIntervalComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # check responses
    if key_resp_5.keys in ['', [], None]:  # No response was made
        key_resp_5.keys = None
    BLOCK.addData('key_resp_5.keys',key_resp_5.keys)
    if key_resp_5.keys != None:  # we had a response
        BLOCK.addData('key_resp_5.rt', key_resp_5.rt)
    # Run 'End Routine' code from update_session
    #-----#
    # Save current state of experiment (block) as a
    # separate file (in a case a mid-experiment
    # restart is required).
    blockfilename = "sub-{}-{}_{}-ses-{}{}_block-{}{}_{}.csv".format(
        expInfo['participant'],
        expInfo['initials'],
        session_type_msg,
        ses_num,
        run_num,
        action_block_counter,
        block_id,
        expInfo['date'],
    )
    thisExp.saveAsWideText(join(_thisDir, subject_dir, room, blockfilename))
    
    #-----#
    # Save time needed to perform ACTION trials.
    if this_block_type == "action":
        
        action_metrics_fn = "sub-{}-{}_{}-ses-{}{}_block-{}{}_{}.npy".format(
            expInfo['participant'],
            expInfo['initials'],
            session_type_msg,
            ses_num,
            run_num,
            action_block_counter,
            block_id,
           "action_metrics",
        )
        
        np.save(
            join(_thisDir, subject_dir, room, action_metrics_fn),
            np.stack((valid_move_trials, valid_move_timings))
        )
        
        action_degrees_fn = "sub-{}-{}_{}-ses-{}{}_block-{}{}_{}.pickle".format(
            expInfo['participant'],
            expInfo['initials'],
            session_type_msg,
            ses_num,
            run_num,
            action_block_counter,
            block_id,
           "action_degrees",
        )
        
        with open(join(_thisDir, subject_dir, room, action_degrees_fn), "wb") as outfile:
            pickle.dump(
                [all_mouse_degrees, all_frame_times],
                outfile,
                protocol=pickle.HIGHEST_PROTOCOL,
            )
    
    #-----#
    # Update block count.
    if action_block_counter >= 2:
        block_id += 1
    action_block_counter += 1
    
    if debug_mode == True:
        print("Block id: {}".format(block_id))
        print("Action block counter: {}".format(action_block_counter))
    # the Routine "InterBlockInterval" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    thisExp.nextEntry()
    
# completed n_blocks + 2 repeats of 'BLOCK'


# --- Prepare to start Routine "ByeByeScreen" ---
continueRoutine = True
routineForceEnded = False
# update component parameters for each repeat
key_resp_6.keys = []
key_resp_6.rt = []
_key_resp_6_allKeys = []
# keep track of which components have finished
ByeByeScreenComponents = [bye_bye, key_resp_6]
for thisComponent in ByeByeScreenComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
frameN = -1

# --- Run Routine "ByeByeScreen" ---
while continueRoutine and routineTimer.getTime() < 15.0:
    # get current time
    t = routineTimer.getTime()
    tThisFlip = win.getFutureFlipTime(clock=routineTimer)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *bye_bye* updates
    if bye_bye.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        bye_bye.frameNStart = frameN  # exact frame index
        bye_bye.tStart = t  # local t and not account for scr refresh
        bye_bye.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(bye_bye, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'bye_bye.started')
        bye_bye.setAutoDraw(True)
    if bye_bye.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > bye_bye.tStartRefresh + 15.0-frameTolerance:
            # keep track of stop time/frame for later
            bye_bye.tStop = t  # not accounting for scr refresh
            bye_bye.frameNStop = frameN  # exact frame index
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'bye_bye.stopped')
            bye_bye.setAutoDraw(False)
    
    # *key_resp_6* updates
    waitOnFlip = False
    if key_resp_6.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_6.frameNStart = frameN  # exact frame index
        key_resp_6.tStart = t  # local t and not account for scr refresh
        key_resp_6.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_6, 'tStartRefresh')  # time at next scr refresh
        # add timestamp to datafile
        thisExp.timestampOnFlip(win, 'key_resp_6.started')
        key_resp_6.status = STARTED
        # AllowedKeys looks like a variable named `valid_keys`
        if not type(valid_keys) in [list, tuple, np.ndarray]:
            if not isinstance(valid_keys, str):
                logging.error('AllowedKeys variable `valid_keys` is not string- or list-like.')
                core.quit()
            elif not ',' in valid_keys:
                valid_keys = (valid_keys,)
            else:
                valid_keys = eval(valid_keys)
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_6.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_6.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_6.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > key_resp_6.tStartRefresh + 15.0-frameTolerance:
            # keep track of stop time/frame for later
            key_resp_6.tStop = t  # not accounting for scr refresh
            key_resp_6.frameNStop = frameN  # exact frame index
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'key_resp_6.stopped')
            key_resp_6.status = FINISHED
    if key_resp_6.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_6.getKeys(keyList=list(valid_keys), waitRelease=False)
        _key_resp_6_allKeys.extend(theseKeys)
        if len(_key_resp_6_allKeys):
            key_resp_6.keys = _key_resp_6_allKeys[-1].name  # just the last key pressed
            key_resp_6.rt = _key_resp_6_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        routineForceEnded = True
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in ByeByeScreenComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# --- Ending Routine "ByeByeScreen" ---
for thisComponent in ByeByeScreenComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_6.keys in ['', [], None]:  # No response was made
    key_resp_6.keys = None
thisExp.addData('key_resp_6.keys',key_resp_6.keys)
if key_resp_6.keys != None:  # we had a response
    thisExp.addData('key_resp_6.rt', key_resp_6.rt)
thisExp.nextEntry()
# using non-slip timing so subtract the expected duration of this Routine (unless ended on request)
if routineForceEnded:
    routineTimer.reset()
else:
    routineTimer.addTime(-15.000000)

# --- End experiment ---
# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv', delim='auto')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
if eyetracker:
    eyetracker.setConnectionState(False)
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
