"""
Experimental design randomization and visualization.
"""

import pickle
import numpy as np
import matplotlib.pyplot as plt


def split_in_half(number):
    """
    Counterbalance an experimental level by splitting it in
    half.

    Parameters
    ----------
    number: int
            Number of experimental conditions to be
            counterbalanced.

    Returns
    -------
    half: list
          List of random order [floor(number/2), ceil(number/2)].
    """

    # Split in half.
    if number % 2 == 0:
        half = [number / 2, number / 2]
    else:
        half = [np.floor(number / 2), np.ceil(number / 2)]
    np.random.shuffle(half)

    return half


def duplicate_with_inverse(ordered_list):
    """
    Compute the inverse of the provided list and return a
    list that stacks the original and the inverse.

    Parameters
    ----------
    ordered_list: list or numpy array
                  List of two elements repeated in any order.

    Returns
    -------
    duplicate_list: numpy array
                    Array stacking the 'ordered_list' and its
                    inverse.
    """

    # Unique elements of list.
    elements = np.unique(ordered_list)

    # Indices of elements.
    temp_0 = np.where(ordered_list == elements[0])[0]
    temp_1 = np.where(ordered_list == elements[1])[0]

    # Inverse entries.
    ordered_list_temp = np.copy(ordered_list)
    ordered_list_temp[temp_0] = elements[1]
    ordered_list_temp[temp_1] = elements[0]

    # Merge.
    duplicate_list = np.vstack((ordered_list, ordered_list_temp))

    return duplicate_list


def counterbalance_conditions(
    n_subs,
    parent_conditions,
    child_conditions,
    grandchild_conditions
):
    """
    Counterbalance the experimental conditions on the children level
    for each experimental condition on the parent level.

    Parameters
    ----------
    n_subs: int
            Number of subjects.
    parent_conditions: 2D list
                       List of parent-level conditions.
    child_conditions: 2D list
                      List of child-level conditions.
    grandchild_conditions: 2D list
                           List of grandchild-level conditions.

    Returns
    -------
    subs_parents: counter-balanced split of parent conditions
                  per experimental subject.
    subs_children: counter-balanced split of children conditions
                   per experimental subject.
    """

    # Split each level in half.
    if n_subs == 1:
        half_parent = [1, 1]

        child_0 = [1, 0]
        child_1 = [0, 1]

        grandchild_0_0 = [0, 0]
        grandchild_0_1 = [1, 0]
        grandchild_1_0 = [0, 1]
        grandchild_1_1 = [0, 0]

    else:
        half_parent = split_in_half(n_subs)

        child_0 = split_in_half(half_parent[0])
        child_1 = split_in_half(half_parent[1])

        grandchild_0_0 = split_in_half(child_0[0])
        grandchild_0_1 = split_in_half(child_0[1])
        grandchild_1_0 = split_in_half(child_1[0])
        grandchild_1_1 = split_in_half(child_1[1])

    # Counter-balanced order of grandchildren.
    grandchildren_order = []
    grandchildren_order.append(grandchild_0_0)
    grandchildren_order.append(grandchild_0_1)
    if child_0[0] > child_0[1]:
        if (
            grandchild_0_0[0] > grandchild_0_0[1]
            and grandchild_1_1[1] >= grandchild_1_1[0]
        ) or (
            grandchild_0_0[0] < grandchild_0_0[1]
            and grandchild_1_1[1] <= grandchild_1_1[0]
        ):
            grandchildren_order.append(grandchild_1_1)
            grandchildren_order.append(grandchild_1_0)
        else:
            grandchildren_order.append(grandchild_1_0)
            grandchildren_order.append(grandchild_1_1)
    elif child_0[0] <= child_0[1]:
        if (
            grandchild_0_1[0] > grandchild_0_1[1]
            and grandchild_1_0[1] >= grandchild_1_0[0]
        ) or (
            grandchild_0_1[0] < grandchild_0_1[1]
            and grandchild_1_0[1] <= grandchild_1_0[0]
        ):
            grandchildren_order.append(grandchild_1_1)
            grandchildren_order.append(grandchild_1_0)
        else:
            grandchildren_order.append(grandchild_1_0)
            grandchildren_order.append(grandchild_1_1)

    print(grandchildren_order)

    # Order randomization.
    subs_children = []
    subs_grandchildren = []

    for i in range(len(child_conditions)):
        if i == 0:
            subs_parent_0 = np.repeat(parent_conditions[0], half_parent[0])
            subs_parent_1 = np.repeat(parent_conditions[1], half_parent[1])

            # Random parent order.
            subs_parents_orig = np.hstack((subs_parent_0, subs_parent_1))
            shuffled = np.arange(0, len(subs_parents_orig), 1)
            np.random.shuffle(shuffled)

            subs_parents = np.copy(subs_parents_orig)[shuffled]
            subs_parents = duplicate_with_inverse(subs_parents)

            # Random "child_0" and "child_1" hand in 1st parent.
            subs_parent_0_child_0 = np.repeat(child_conditions[0], child_0[0])
            subs_parent_0_child_1 = np.repeat(child_conditions[1], child_0[1])
            subs_parent_0_children = np.hstack(
                (subs_parent_0_child_0, subs_parent_0_child_1)
            )

            subs_parent_1_child_0 = np.repeat(child_conditions[0], child_1[0])
            subs_parent_1_child_1 = np.repeat(child_conditions[1], child_1[1])
            subs_parent_1_children = np.hstack(
                (subs_parent_1_child_0, subs_parent_1_child_1)
            )

            # Random "grandchild_0_0/1" and "grandchild_1_1/0" condition in 1st parent.
            subs_parent_0_child_0_grand_0 = np.repeat(
                grandchild_conditions[0], grandchildren_order[0][0]
            )
            subs_parent_0_child_0_grand_1 = np.repeat(
                grandchild_conditions[1], grandchildren_order[0][1]
            )

            subs_parent_0_child_1_grand_0 = np.repeat(
                grandchild_conditions[0], grandchildren_order[1][0]
            )
            subs_parent_0_child_1_grand_1 = np.repeat(
                grandchild_conditions[1], grandchildren_order[1][1]
            )

            subs_parent_0_grandchildren = np.hstack(
                (
                    subs_parent_0_child_0_grand_0,
                    subs_parent_0_child_0_grand_1,
                    subs_parent_0_child_1_grand_0,
                    subs_parent_0_child_1_grand_1,
                )
            )

            subs_parent_1_child_0_grand_0 = np.repeat(
                grandchild_conditions[0], grandchildren_order[2][0]
            )
            subs_parent_1_child_0_grand_1 = np.repeat(
                grandchild_conditions[1], grandchildren_order[2][1]
            )

            subs_parent_1_child_1_grand_0 = np.repeat(
                grandchild_conditions[0], grandchildren_order[3][0]
            )
            subs_parent_1_child_1_grand_1 = np.repeat(
                grandchild_conditions[1], grandchildren_order[3][1]
            )

            subs_parent_1_grandchildren = np.hstack(
                (
                    subs_parent_1_child_0_grand_0,
                    subs_parent_1_child_0_grand_1,
                    subs_parent_1_child_1_grand_0,
                    subs_parent_1_child_1_grand_1,
                )
            )

        elif i == 1:
            # Counter-balanced parent order.
            subs_parent_0 = np.repeat(parent_conditions[0], half_parent[1])
            subs_parent_1 = np.repeat(parent_conditions[1], half_parent[0])

            # Counter-balanced "child_0" or "child_1" hand in 2nd parent.
            subs_parent_0_child_0 = np.repeat(child_conditions[0], child_0[1])
            subs_parent_0_child_1 = np.repeat(child_conditions[1], child_0[0])
            subs_parent_0_children = np.hstack(
                (subs_parent_0_child_0, subs_parent_0_child_1)
            )

            subs_parent_1_child_0 = np.repeat(child_conditions[0], child_1[1])
            subs_parent_1_child_1 = np.repeat(child_conditions[1], child_1[0])
            subs_parent_1_children = np.hstack(
                (subs_parent_1_child_0, subs_parent_1_child_1)
            )

            # Counter-balanced "grandchild_1_0" or "grandchild_1_0" condition in 2nd parent.
            subs_parent_0_child_0_grand_0 = np.repeat(
                grandchild_conditions[0], grandchildren_order[3][1]
            )
            subs_parent_0_child_0_grand_1 = np.repeat(
                grandchild_conditions[1], grandchildren_order[3][0]
            )

            subs_parent_0_child_1_grand_0 = np.repeat(
                grandchild_conditions[0], grandchildren_order[2][1]
            )
            subs_parent_0_child_1_grand_1 = np.repeat(
                grandchild_conditions[1], grandchildren_order[2][0]
            )

            subs_parent_0_grandchildren = np.hstack(
                (
                    subs_parent_0_child_0_grand_0,
                    subs_parent_0_child_0_grand_1,
                    subs_parent_0_child_1_grand_0,
                    subs_parent_0_child_1_grand_1,
                )
            )

            subs_parent_1_child_0_grand_0 = np.repeat(
                grandchild_conditions[0], grandchildren_order[1][1]
            )
            subs_parent_1_child_0_grand_1 = np.repeat(
                grandchild_conditions[1], grandchildren_order[1][0]
            )

            subs_parent_1_child_1_grand_0 = np.repeat(
                grandchild_conditions[0], grandchildren_order[0][1]
            )
            subs_parent_1_child_1_grand_1 = np.repeat(
                grandchild_conditions[1], grandchildren_order[0][0]
            )

            subs_parent_1_grandchildren = np.hstack(
                (
                    subs_parent_1_child_0_grand_0,
                    subs_parent_1_child_0_grand_1,
                    subs_parent_1_child_1_grand_0,
                    subs_parent_1_child_1_grand_1,
                )
            )

        subs_children_orig = np.hstack((subs_parent_0_children, subs_parent_1_children))
        subs_children_copy = np.copy(subs_children_orig)[shuffled]
        subs_children_copy = duplicate_with_inverse(subs_children_copy)
        subs_children.append(subs_children_copy)

        subs_grandchildren_copy_0 = np.copy(subs_parent_0_grandchildren)
        subs_grandchildren_copy_0 = duplicate_with_inverse(subs_grandchildren_copy_0)
        subs_grandchildren_copy_1 = np.copy(subs_parent_1_grandchildren)
        subs_grandchildren_copy_1 = duplicate_with_inverse(subs_grandchildren_copy_1)

        subs_grandchildren_copy = np.hstack(
            (subs_grandchildren_copy_0[1], subs_grandchildren_copy_1[1])
        )[shuffled]
        subs_grandchildren_copy = duplicate_with_inverse(subs_grandchildren_copy)
        subs_grandchildren.append(subs_grandchildren_copy)

    return subs_parents, subs_children, subs_grandchildren


def subs_generator(
    n_subs=25,
    n_blocks=10,
    n_trials=30,
    perc_catch=0.1,
    shuffle_order=True,
    mode="zero",
    calibration=False,
    savepath="/home/sotpapad/Codes/experiment/",
):
    """
    Produce a pseudorandom order of recording modality
    and hand order per subjects.

    Parameters
    ----------
    n_subs: int, optional
            Total number of participants in the experiment.
            Defaults to 25.
    n_blocks: int, optional
              Total number of blocks for a run.
              Defaults to 10.
    n_trials: int, optional
              Total number of trials in the sequence.
              Defaults to 30.
    perc_catch: float, optional
              Percentage of trials to be added as catch trials
              to an imagery block of trials.
              Defaults to 0.1.
    shuffle_order: bool
                   If "True", shuffle the order of blocks except for
                   The first one.
                   Defaults to "True".
    mode: str, optional, {"zero", "lelouch"}
          Control the experimental paradigm. If set to "zero",
          each trial will begin from the neutral hand position
          ("0"). In this case 90 degrees movement amplitudes
          are excluded. If set to "lelouch" each trial starts
          at the previous trial's target.
          Defaults to "zero".
    calibration: bool, optional
                 Control the experimental paradigm. If "True"
                 generate extra amplitudes (not used in the
                 experiment).
                 Defaults to "False".
    savepath: str, optional
              Directory to store results.
    """

    # Calibration data are only produced for trials starting
    # from neutral position.
    if calibration == True and mode == "lelouch":
        raise ValueError("Can only produce calibration block with trials starting from neutral position.")

    # Recording modalities, hands, experimental conditions.
    recordings = ["eeg", "meg"]
    hands = ["left_hand", "right_hand"]
    conditions = ["action", "imagery"]

    if n_subs == 1 and calibration == False:
        # Assume need for training data.
        print("Generating sample data for training block...")
        subs_meeg = ["eeg"]
        subs_hands = ["right_hand"]
        subs_conditions = ["action", "imagery"]
    elif n_subs == 1 and calibration == True:
        # Assume need for calibration data.
        print("Generating sample data for calibration purposes...")
        subs_meeg = ["eeg"]
        subs_hands = ["right_hand"]
        subs_conditions = ["action"]
    elif n_subs >= 4:
        subs_meeg, subs_hands, subs_conditions = counterbalance_conditions(
            n_subs, recordings, hands, conditions
        )
    elif n_subs > 1 and n_subs < 4:
        msg1 = "In order to properly counter-balance all experimental conditions you need at least 4 subjects.\n"
        msg2 = "You can specify 'n_subs=1', if you are only interested in generating some sample data for testing purposes."
        raise ValueError(msg1 + msg2)

    subjects = {}
    for s in range(n_subs):
        # Add subject id.
        subject_id = "subject_{}".format(s + 1)
        subjects[subject_id] = {}

        # Add recording modality.
        if n_subs == 1 or calibration == True:
            subjects[subject_id]["rec_order"] = subs_meeg
        elif n_subs >= 4:
            subjects[subject_id]["rec_order"] = subs_meeg[:, s].tolist()

        # Add 1st hand and 1st condition per recording modality.

        subjects[subject_id]["hand_order"] = []
        subjects[subject_id]["condition_order"] = []

        for sb, sub_recording in enumerate(subjects[subject_id]["rec_order"]):
            if n_subs == 1:
                subjects[subject_id]["hand_order"].append(subs_hands)
                subjects[subject_id]["condition_order"].append(subs_conditions)
            elif n_subs >= 4:
                subjects[subject_id]["hand_order"].append(subs_hands[sb][:, s].tolist())
                subjects[subject_id]["condition_order"].append(
                    subs_conditions[sb][:, s].tolist()
                )

            for sub_hand, sub_condition in zip(
                subjects[subject_id]["hand_order"][sb],
                subjects[subject_id]["condition_order"][sb],
            ):
                rec_hand_cond = "{}_{}_{}".format(
                    sub_recording, sub_hand, sub_condition
                )
                subjects[subject_id][rec_hand_cond] = blocks_generator(
                    sub_condition,
                    n_subs=n_subs,
                    n_blocks=n_blocks,
                    n_trials=n_trials,
                    perc_catch=perc_catch,
                    shuffle_order=shuffle_order,
                    mode=mode,
                    calibration=calibration,
                )

    # Save experimental design.
    if mode == "zero":
        mode_str = "zero"
    elif mode == "lelouch":
        mode_str = "continue"

    if n_subs == 1 and calibration == False:
        exp_des_name = "exp_design_{}_training.pickle".format(mode_str)
    elif n_subs == 1 and calibration == True:
        exp_des_name = "exp_design_{}_calibration.pickle".format(mode_str)
    elif n_subs >= 4:
        exp_des_name = "exp_design_{}.pickle".format(mode_str)

    with open(savepath + exp_des_name, "wb") as file:
        pickle.dump(subjects, file)


def blocks_generator(
    condition,
    n_subs=25,
    n_blocks=10,
    n_trials=30,
    perc_catch=0.1,
    shuffle_order=True,
    mode="zero",
    calibration=False,
):
    """
    Produce an alternating or random sequence of blocks,
    ensuring that both experimental conditions have the
    same number of blocks.

    Parameters
    ----------
    condition: str, {"action", "imagery"}
               Condition corresponding to the 1st block.
               The rest of the sequence is based on that.
    n_subs: int, optional
            Total number of participants in the experiment.
            Defaults to 25.
    n_blocks: int, optional
              Total number of blocks for a run.
              Defaults to 10.
    n_trials: int, optional
              Total number of trials in the sequence.
              Defaults to 30.
    perc_catch: float, optional
              Percentage of trials to be added as catch trials
              to an imagery block of trials.
              Defaults to 0.1.
    shuffle_order: bool
                   If "True", shuffle the order of blocks except for
                   The first one.
                   Defaults to "True".
    mode: str, optional, {"zero", "lelouch"}
          Control the experimental paradigm. If set to "zero",
          each trial will begin from the neutral hand position
          ("0"). In this case 90 degrees movement amplitudes
          are excluded. If set to "lelouch" each trial starts
          at the previous trial's target.
          Defaults to "zero".
    calibration: bool, optional
                 Control the experimental paradigm. If "True"
                 generate extra amplitudes (not used in the
                 experiment).
                 Defaults to "False".

    Returns
    -------
    blocks: dict
            Dictionary of blocks of trials.
    """

    # Experimental conditions.
    if condition == "action":
        conditions = ["action", "imagery"]
    elif condition == "imagery":
        conditions = ["imagery", "action"]
    n_conditions = len(conditions)

    # Conditioned blocks.
    if calibration == False:
        n_blocks_cond = int(n_blocks / n_conditions)
        blocks_order = np.tile(conditions, n_blocks_cond)
        if shuffle_order == True:
            np.random.shuffle(blocks_order[1:])
    elif calibration == True:
        n_blocks_cond = 1
        blocks_order = ["action"]

    # Generate blocks.
    blocks = {}
    for b, block in enumerate(blocks_order):
        block_id = "block_{}".format(b + 1)
        blocks[block_id] = {}
        blocks[block_id]["condition"] = block

        # Only keep full trial sequences.
        blocks[block_id]["n_rej_seq"] = 0
        while True:
            (
                cursor_sequence,
                target_sequence,
                amplitude_sequence,
                catch_sequence,
            ) = trial_seq_randomizer(
                block,
                n_subs=n_subs,
                n_trials=n_trials,
                perc_catch=perc_catch,
                mode=mode,
                calibration=calibration,
            )
            if cursor_sequence is None:
                # Keep track of "stats".
                blocks[block_id]["n_rej_seq"] += 1
            else:
                break
        blocks[block_id]["cursor_seq"] = cursor_sequence
        blocks[block_id]["target_seq"] = target_sequence
        blocks[block_id]["ampl_seq"] = amplitude_sequence
        blocks[block_id]["catch_seq"] = catch_sequence

    return blocks


def trial_seq_randomizer(
    block,
    n_subs=25,
    n_trials=30,
    perc_catch=0.1,
    mode="zero",
    calibration=False,    
):
    """
    Produce a pseudorandom sequence (aka block) of trials,
    ensuring that all targets are visited equally.

    Parameters
    ----------
    block: str, {"action", "imagery"}
           Condition corresponding to the current block.
    n_subs: int, optional
            Total number of participants in the experiment.
            Defaults to 25.
    n_trials: int, optional
              Total number of trials in the sequence.
              Defaults to 30.
    perc_catch: float, optional
              Percentage of trials to be added as catch trials
              to an imagery block of trials.
              Defaults to 0.1.
    mode: str, optional, {"zero", "lelouch"}
          Control the experimental paradigm. If set to "zero",
          each trial will begin from the neutral hand position
          ("0"). In this case 90 degrees movement amplitudes
          are excluded. If set to "lelouch" each trial starts
          at the previous trial's target.
          Defaults to "zero".
    calibration: bool, optional
                 Control the experimental paradigm. If "True"
                 generate extra amplitudes (not used in the
                 experiment).
                 Defaults to "False".

    Returns
    -------
    cursor_sequence: list
                     List of consecutive cursor positions,
                     always starting from 0.
    target_sequence: list
                     List of consecutive target positions.
    amplitude_sequence: list
                        List of consecutive movement amplitudes
                        as a difference between 'cursor' and
                        'target'.
    """

    # All possible positions and valid next amplitudes and directions.
    if calibration == False:
        positions = np.array([-60, -30, 0, 30, 60])
        positions_names = np.array(["minus_60", "minus_30", "zero", "plus_30", "plus_60"])

        if mode == "zero":
            amplitudes = np.array([-60, -30, 30, 60])
            valid_amplitudes = {"zero": (-60, -30, 30, 60)}

        elif mode == "lelouch":
            amplitudes = np.array([-90, -60, -30, 30, 60, 90])
            valid_amplitudes = {
                "minus_60": (30, 60, 90),
                "minus_30": (-30, 30, 60, 90),
                "zero": (-60, -30, 30, 60),
                "plus_30": (-90, -60, -30, 30),
                "plus_60": (-90, -60, -30),
            }
    
    elif calibration == True:
        positions = np.array([-60, -45, -30, -20, 0, 20, 30, 45, 60])
        positions_names = np.array(
            ["minus_60", "minus_45", "minus_30",
            "minus_20", "zero", "plus_20",
            "plus_30", "plus_45", "plus_60"]
        )

        amplitudes = np.array([-60, -45, -30, -20, 20, 30, 45, 60])
        valid_amplitudes = {"zero": (-60, -45, -30, -20, 20, 30, 45, 60)}

    n_amplitudes = len(amplitudes)

    # Ensure that the number of trials is a multiple of
    # the movement amplitudes.
    if not n_trials % n_amplitudes == 0:
        msg = "The number of trials should be a mutliple of {}".format(n_amplitudes)
        raise ValueError(msg)

    # Add extra "catch" trials ~ 10% of all, for "imagery" condition.
    if block == "imagery" and n_subs >= 1:
        if mode == "zero":
            n_trials += 1 * len(amplitudes)
        elif mode == "lelouch":
            n_trials += perc_catch * n_trials
        n_trials = int(n_trials)

    # Number of visits per amplitude.
    n_visits = np.repeat(n_trials / n_amplitudes, n_amplitudes)

    # Random amplitudes for "catch" trials.
    if block == "imagery" and n_subs >= 1:
        if mode == "zero":
            catch_amplitudes = amplitudes
            n_visits = np.ceil(n_visits)
        elif mode == "lelouch":
            add_catch = np.tile([0, 1], int(n_amplitudes / 2))
            np.random.shuffle(add_catch)
            catch_amplitudes = amplitudes[add_catch == 1]
            n_visits[add_catch == 0] = np.floor(n_visits[add_catch == 0])
            n_visits[add_catch == 1] = np.ceil(n_visits[add_catch == 1])

    # Initial amplitude probabilities.
    amplitudes_probs = n_visits / np.sum(n_visits)

    # Lists that store the sequences of amplitudes, cursor and target positions.
    cursor_sequence = []
    target_sequence = []
    amplitude_sequence = []

    # Initial cursor position.
    cursor_position = 0
    cursor_position_name = positions_names[
        int(np.where(positions == cursor_position)[0])
    ]

    # Set up random number generator.
    rng = np.random.default_rng()

    for n in range(n_trials):
        while True:
            # Stricter target selection when a small number of trials is
            # available.
            if n_trials <= 2 * n_amplitudes:
                while True:
                    # Select target amplitude.
                    target_ampl_id = rng.choice(n_amplitudes, p=amplitudes_probs)
                    target_ampl = amplitudes[target_ampl_id]

                    # Do not allow to perform the same amplitude towards
                    # the opposite direction immediately.
                    if n == 0:
                        break
                    else:
                        if target_ampl != -amplitude_sequence[-1]:
                            break
            else:
                target_ampl_id = rng.choice(n_amplitudes, p=amplitudes_probs)
                target_ampl = amplitudes[target_ampl_id]

            # Verify amplitude validity.
            if not target_ampl in valid_amplitudes[cursor_position_name]:
                if n == 0:
                    remaining_amps = amplitudes[np.where(amplitudes_probs != 0)[0]]
                else:
                    remaining_amps = amplitudes[
                        np.where(
                            (amplitudes_probs != 0)
                            & (amplitudes != -amplitude_sequence[-1])
                        )[0]
                    ]

                if (
                    len(
                        set(valid_amplitudes[cursor_position_name]).intersection(
                            remaining_amps
                        )
                    )
                    == 0
                ):
                    return None, None, None, None
                else:
                    continue

            # Update amplitude probabilities.
            n_visits[target_ampl_id] -= [1 if n_visits[target_ampl_id] > 0 else 0]
            if n + 1 < n_trials:
                amplitudes_probs = n_visits / (n_trials - (n + 1))

            break

        # Update cursor and target positions.
        amplitude_sequence.append(target_ampl)

        target = cursor_position + target_ampl
        target_sequence.append(target)

        cursor_sequence.append(cursor_position)
        if mode == "zero":
            cursor_position = 0
        elif mode == "lelouch":
            cursor_position = target

        cursor_position_name = positions_names[
            int(np.where(positions == cursor_position)[0])
        ]

    # Randomly pick some trials to be "catch" among the whole sequence.
    catch_sequence = np.zeros(n_trials)
    if block == "imagery" and n_subs >= 1:
        catch_ids = []
        for c, catch_ampl in enumerate(catch_amplitudes):
            catch_candidates = np.where(amplitude_sequence == catch_ampl)[0]

            # No catch trial in the first 5.
            catch_candidates = catch_candidates[np.where(catch_candidates >= 5)[0]]

            # No consecutive catch trials.
            flag = True
            while flag == True:
                catch_id = np.random.choice(catch_candidates, 1)

                if c == 0:
                    flag = False
                if c >= 1:
                    catch_diffs = np.abs(np.copy(catch_ids) - catch_id)
                    if (catch_diffs != 1).any():
                        flag = False

            catch_sequence[catch_id] = 1
            catch_ids.append(catch_id)

    catch_sequence = catch_sequence.tolist()

    return cursor_sequence, target_sequence, amplitude_sequence, catch_sequence


def plot_design(
    n_subs,
    mode="zero",
    screen_res=[1920, 972],
    dpi=300,
    savepath="/home/sotpapad/Codes/experiment/",
):
    """
    Produce a plot that summarizes the experimental design.

    Parameters
    ----------
    n_subs: int
            Total number of participants in the experiment.
    mode: str, optional, {"zero", "lelouch"}
          Control the experimental paradigm. If set to "zero",
          each trial will begin from the neutral hand position
          ("0"). In this case 90 degrees movement amplitudes
          are excluded. If set to "lelouch" each trial starts
          at the previous trial's target.
          Defaults to "zero".
    screen_res: two-element list, optional
                Number of pixels for specifying the figure size in
                conjunction with "dpi".
                Defaults to [1920, 972].
    dpi: int, optional
         Number of dots per inch for specifying the figure size in
         conjunction with "screen_res".
         Defaults to 300.
    savepath: str, optional
              Directory tp store results.
    """

    # ----- #
    # Load design.
    if mode == "zero":
        mode_str = "zero"
    elif mode == "lelouch":
        mode_str = "continue"

    if n_subs == 1:
        exp_des_name = "exp_design_{}_training.pickle".format(mode_str)
    elif n_subs >= 4:
        exp_des_name = "exp_design_{}.pickle".format(mode_str)

    with open(savepath + exp_des_name, "rb") as file:
        subjects = pickle.load(file)

    # ----- #
    # Varibles initialization.
    rec_hand = [
        "eeg_left_hand_action",
        "eeg_left_hand_imagery",
        "eeg_right_hand_action",
        "eeg_right_hand_imagery",
        "meg_left_hand_action",
        "meg_left_hand_imagery",
        "meg_right_hand_action",
        "meg_right_hand_imagery",
    ]

    starts_with_eeg = 0
    starts_with_eeg_lh_action = 0
    starts_with_eeg_rh_action = 0
    starts_with_eeg_lh_imagery = 0
    starts_with_eeg_rh_imagery = 0

    starts_with_meg = 0
    starts_with_meg_lh_action = 0
    starts_with_meg_rh_action = 0
    starts_with_meg_lh_imagery = 0
    starts_with_meg_rh_imagery = 0

    n_rej_seq = 0
    n_acc_seq = 0

    all_cursor_sequences = []
    all_target_sequences = []
    all_ampl_sequences = []
    all_catch_sequences = []

    # Parse all subjects.
    n_subjects = len(subjects.keys())

    for s in range(n_subjects):
        subject_id = "subject_{}".format(s + 1)

        # Find first recording modality and hand side.
        if subjects[subject_id]["rec_order"][0] == "eeg":
            starts_with_eeg += 1

            # EEG
            if subjects[subject_id]["hand_order"][0][0] == "left_hand":
                if subjects[subject_id]["condition_order"][0][0] == "action":
                    starts_with_eeg_lh_action += 1
                else:
                    starts_with_eeg_lh_imagery += 1

            elif subjects[subject_id]["hand_order"][0][0] == "right_hand":
                if subjects[subject_id]["condition_order"][0][0] == "action":
                    starts_with_eeg_rh_action += 1
                else:
                    starts_with_eeg_rh_imagery += 1

            # MEG
            if n_subs > 1:
                if subjects[subject_id]["hand_order"][1][0] == "left_hand":
                    if subjects[subject_id]["condition_order"][1][0] == "action":
                        starts_with_meg_lh_action += 1
                    else:
                        starts_with_meg_lh_imagery += 1

                elif subjects[subject_id]["hand_order"][1][0] == "right_hand":
                    if subjects[subject_id]["condition_order"][0][0] == "action":
                        starts_with_meg_rh_action += 1
                    else:
                        starts_with_meg_rh_imagery += 1

        elif subjects[subject_id]["rec_order"][0] == "meg":
            starts_with_meg += 1

            # MEG
            if subjects[subject_id]["hand_order"][0][0] == "left_hand":
                if subjects[subject_id]["condition_order"][0][0] == "action":
                    starts_with_meg_lh_action += 1
                else:
                    starts_with_meg_lh_imagery += 1

            elif subjects[subject_id]["hand_order"][0][0] == "right_hand":
                if subjects[subject_id]["condition_order"][0][0] == "action":
                    starts_with_meg_rh_action += 1
                else:
                    starts_with_meg_rh_imagery += 1

            # EEG
            if subjects[subject_id]["hand_order"][1][0] == "left_hand":
                if subjects[subject_id]["condition_order"][1][0] == "action":
                    starts_with_eeg_lh_action += 1
                else:
                    starts_with_eeg_lh_imagery += 1

            elif subjects[subject_id]["hand_order"][1][0] == "right_hand":
                if subjects[subject_id]["condition_order"][0][0] == "action":
                    starts_with_eeg_rh_action += 1
                else:
                    starts_with_eeg_rh_imagery += 1

        # Parse all blocks.
        valid_blocks = []
        for rh in rec_hand:
            if rh in subjects[subject_id].keys():
                valid_blocks.append(rh)

        for rh in valid_blocks:
            n_blocks = len(valid_blocks)

            for b in range(n_blocks):
                block_id = "block_{}".format(b + 1)

                # Aggregate number of rejected sequences.
                n_rej_seq += subjects[subject_id][rh][block_id]["n_rej_seq"]
                n_acc_seq += 1

                # Aggregate trial sequences.
                all_cursor_sequences.append(
                    subjects[subject_id][rh][block_id]["cursor_seq"]
                )
                all_target_sequences.append(
                    subjects[subject_id][rh][block_id]["target_seq"]
                )
                all_ampl_sequences.append(
                    subjects[subject_id][rh][block_id]["ampl_seq"]
                )
                all_catch_sequences.append(
                    subjects[subject_id][rh][block_id]["catch_seq"]
                )

    # ----- #
    # Figure initialization.
    fig = plt.figure(
        constrained_layout=False,
        figsize=(screen_res[0] / dpi, screen_res[1] / dpi),
        dpi=dpi,
    )
    gs = fig.add_gridspec(
        nrows=2,
        ncols=1,
        hspace=0.50,
        right=0.93,
        left=0.08,
        top=0.95,
        bottom=0.10,
        height_ratios=[0.3, 0.7],
    )
    gs0 = gs[0].subgridspec(1, 4, width_ratios=[0.15, 0.20, 0.40, 0.25], wspace=0.40)
    gs1 = gs[1].subgridspec(1, 2, width_ratios=[0.85, 0.15], wspace=0.20)

    ax00 = fig.add_subplot(gs0[0])
    ax01 = fig.add_subplot(gs0[1])
    ax02 = fig.add_subplot(gs0[2])
    ax03 = fig.add_subplot(gs0[3])
    ax10 = fig.add_subplot(gs1[0])
    ax11 = fig.add_subplot(gs1[1])

    label_size = 6
    ticks_size = 6

    # ----- #
    # Plots.

    print(starts_with_eeg_lh_action)
    print(starts_with_eeg_lh_imagery)
    print(starts_with_eeg_rh_action)
    print(starts_with_eeg_rh_imagery)

    print(starts_with_meg_lh_action)
    print(starts_with_meg_lh_imagery)
    print(starts_with_meg_rh_action)
    print(starts_with_meg_rh_imagery)

    # Subplot 00: 1st recording modality balance.
    ax00.bar(["eeg", "meg"], [starts_with_eeg, n_subjects - starts_with_eeg])
    ax00.set_xlabel("1st recording modality", fontsize=label_size)
    ax00.set_ylabel("# subjects", fontsize=label_size)
    ax00.tick_params(axis="both", labelsize=ticks_size)
    ax00.spines[["top", "right"]].set_visible(False)

    # Subplot 01: 1st hand modality balance.
    ax01.bar(
        ["eeg\nleft", "eeg\nright", "meg\nleft", "meg\nright"],
        [
            starts_with_eeg_lh_action + starts_with_eeg_lh_imagery,
            starts_with_eeg_rh_action + starts_with_eeg_rh_imagery,
            starts_with_meg_lh_action + starts_with_meg_lh_imagery,
            starts_with_meg_rh_action + starts_with_meg_rh_imagery,
        ],
    )
    ax01.set_xlabel("1st hand per modality", fontsize=label_size)
    ax01.set_ylabel("# blocks", fontsize=label_size)
    ax01.tick_params(axis="both", labelsize=ticks_size)
    ax01.spines[["top", "right"]].set_visible(False)

    # Subplot 02: 1st condition balance.
    llabels = ["action", "imagery"]
    xlabels = ["eeg\nleft", "eeg\nright", "meg\nleft", "meg\nright"]
    xpos = np.arange(len(xlabels))
    width = 0.4
    ax02.bar(
        xpos - width / 2,
        [
            starts_with_eeg_lh_action,
            starts_with_eeg_rh_action,
            starts_with_meg_lh_action,
            starts_with_meg_rh_action,
        ],
        width,
        label=llabels[0],
    )
    ax02.bar(
        xpos + width / 2,
        [
            starts_with_eeg_lh_imagery,
            starts_with_eeg_rh_imagery,
            starts_with_meg_lh_imagery,
            starts_with_meg_rh_imagery,
        ],
        width,
        label=llabels[1],
    )
    ax02.set_xlabel("1st condition per modality and hand", fontsize=label_size)
    ax02.set_ylabel("# blocks", fontsize=label_size)
    ax02.set_xticks(xpos, xlabels)
    ax02.tick_params(axis="both", labelsize=ticks_size)
    ax02.spines[["top", "right"]].set_visible(False)
    ax02.legend(frameon=False, fontsize=4, loc="best")

    # Subplot 03: targets balance.
    bag_of_ampls = np.hstack(np.copy(all_ampl_sequences))
    unique_ampls = np.unique(bag_of_ampls)
    ampls_count = []
    ampls_names = []
    for unique_ampl in unique_ampls:
        ampls_count.append(len(np.where(bag_of_ampls == unique_ampl)[0]))
        ampls_names.append("{}".format(unique_ampl))

    ax03.bar(ampls_names, ampls_count)
    ax03.set_xlabel("Amplitude", fontsize=label_size)
    ax03.set_ylabel("# trials", fontsize=label_size)
    ax03.tick_params(axis="both", labelsize=ticks_size)
    ax03.spines[["top", "right"]].set_visible(False)

    # Subplot 10: sequences.
    if n_subs > 1:
        rng = np.random.default_rng()
        sample = rng.integers(
            1, len(all_target_sequences) + 1, size=int(0.02 * len(all_target_sequences))
        )
        sample_sequences = np.array(all_target_sequences)[sample]
    elif n_subs == 1:
        sample_sequences = np.array(all_target_sequences)

    target_pos = []
    target_names = []
    for unique_target in np.unique(all_target_sequences[0]):
        target_pos.append(unique_target)
        target_names.append("{}".format(unique_target))

    for target_seq in sample_sequences:
        ax10.plot(np.arange(1, len(target_seq) + 1, 1), target_seq)
    ax10.set_yticks(target_pos)
    ax10.set_yticklabels(target_pos)
    ax10.set_xlabel("# trial", fontsize=label_size)
    ax10.set_ylabel("Target position", fontsize=label_size)
    ax10.tick_params(axis="both", labelsize=ticks_size)
    ax10.spines[["top", "right"]].set_visible(False)

    # Subplot 11: rejected and accepted sequences balance.
    ax11.pie(
        [n_rej_seq, n_acc_seq],
        explode=[0, 0.1],
        labels=["Rejected\ntrial seq.", "Accepted\ntrial seq."],
        colors=["r", "m"],
        textprops={"fontsize": ticks_size},
        autopct="%1.1f%%",
        startangle=-90,
    )
    ax11.axis("equal")

    plt.show()


# ----- #
# RUN
mode = "zero"       # "zero", "lelouch"
training = False    # True, False
calibration = False # True, False

if training == True or calibration == True:
    n_subs = 1
elif training == False and calibration == False:
    n_subs = 24

if training == False and calibration == True:
    n_blocks = 1
    n_trials = 32
elif training == True and calibration == False:
    n_blocks = 2
    n_trials = 12
    perc_catch = 0.2
elif training == False and calibration == False:
    n_blocks = 6
    n_trials = 20
    perc_catch = 0.1

#"""
subs_generator(
    n_subs=n_subs,
    n_blocks=n_blocks,
    n_trials=n_trials,
    perc_catch=perc_catch,
    shuffle_order=True,
    mode=mode,
    calibration=calibration,
)
#"""

#plot_design(n_subs)
