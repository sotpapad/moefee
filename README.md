# Moefee

Project's name meaning according to El Psy Kongroo:
> It doesn't mean anything.

Some believe the name to be some kind of code.

## Description

Selection of repositories relevant to my PhD thesis. The aim is to systematically organize various scripts developed during different stages of the process: from initial test codes to the final code structure used for the analysis of the data collected during the experiment.

## Requirements

Generally, all included scripts should run using python >= 3.9 and/or a recent ipython kernel.

Libraries that are essential for running these scripts include up-to-date versions of:
- [numpy](https:numpy.org)
- [matplotlib](https:matplotlib.org)
- [mne](https://mne.tools/stable/index.html#)
- [scipy](https://scipy.org/)

Other libraries include:
- [fooof](https://fooof-tools.github.io/fooof/)
- [autoreject](https://autoreject.github.io/stable/index.html)
- [scikit-learn](https://scikit-learn.org/stable/index.html)
- [scikit-image](https://scikit-image.org/)
- [MOABB](https://moabb.neurotechx.com/docs/index.html)
- [pyRiemann](https://pyriemann.readthedocs.io/en/latest/index.html)
- [neurodsp](https://neurodsp-tools.github.io/neurodsp/)
- [psychopy](https://www.psychopy.org/index.html) - NOTE: it requires python
  v==3.18. 

## Roadmap
1. Add and populate relevant subdirectories and/or submodules.

## License
All scripts licensed under GPLv3 or later.

Publications related to this work:

 - [S. Papadopoulos, et al., Improved motor imagery decoding
   with spatiotemporal filtering based on beta burst kernels.
   Proceedings of the 9th Graz Brain-Computer Interface Conference 2024,
   10.3217/978-3-99161-014-4-042](https://www.tugraz.at/fileadmin/user_upload/Institute/INE/Proceedings/Proceedings_GBCIC2024.pdf)
 - [S. Papadopoulos, et al., Surfing beta bursts to improve
   motor imagery-based BCI. bioRxiv, 2024, 2024.07.18.604064](https://www.biorxiv.org/content/10.1101/2024.07.18.604064v1).
 - [S. Papadopoulos, et al., Beta bursts question the ruling power for
   brain-computer interfaces. J. Neural Eng., 2024, 21 (1), pp.016010](https://iopscience.iop.org/article/10.1088/1741-2552/ad19ea).
 - [S. Papadopoulos, J. Bonaiuto, J. Mattout, An Impending Paradigm Shift
   in Motor Imagery Based Brain-Computer Interfaces. Front. Neurosci. 15 (2022)](https://www.frontiersin.org/articles/10.3389/fnins.2021.824759/full).
